# Deltax
## How to do local setup?
This is a docker based setup for local development. We have 4 docker services.

### `admin`: UI to access MySQL DB.
This is an installaton of `phpMyAdmin` instance. You can access the UI through http://localhost:9201/

| Username | Password |
| -- | -- |
| root | root |
| deltax | deltax@2022 |

### `db`: MySQL DB Instance
This is the MySQL Server instance.
| Username | Password |
| -- | -- |
| root | root |
| deltax | deltax@2022 |

### `api`: Nest.js based APIs
Nest.js codebase that exposes APIs for front-end application.

### `ui`: Angular based UI Code.
UI Code using Angular. This will have the Angular code that will provide the UI for the application.

---
## Useful Commands
```bash
# To start all the docur services in the background as daemon.
$ docker-compose up -d
```

```bash
# To start specific service
# docker-compose up <service>
$ docker-compose up api
```

```bash
# To stop all services
$ docker-compose stop
```

```bash
# To stop specific services
# docker-compose stop <service>
$ docker-compose stop api
```

### SSH To Service Termial
```bash
# docker exec -it <service> bash
$ docker exec -it api bash
```

### Run UI Code
```bash
$ docker exec -it ui bash
$ npm install
$ npm start
```
> You can access the UI at http://localhost:4201/

### Run API Code
```bash
$ docker exec -it api bash
$ npm install
$ npm start
```
> You can access the service at http://localhost:9001/
