import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/features/auth/login.service';
import { environment } from 'src/environments/environment';
import { AppStore } from 'src/app/services/appstore.service';
import { CommonService } from 'src/app/services/common.service';
import { SessionData } from 'src/app/services/sessiondata';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private commonService: CommonService,
    private appStore: AppStore) { }

  public userInfo: any = null
  public docurl = '';
  public companyId = 0;
  public userRole: string = '';
  public isShowDropdown: boolean = false;

  ngOnInit(): void {
    this.docurl = environment.docurl;
    this.userInfo = SessionData.userInfo;
    this.userRole = this.commonService.getUserRoleForDisplay();
    // this.customService.changeTheme(this.userInfo.theme || 'default');
  }

  toggleDropdown(): void {
    this.isShowDropdown = !this.isShowDropdown;
  }

  handleLogout = () => {
    this.loginService.logout();
    // this.customService.changeTheme('default');
  }
}
