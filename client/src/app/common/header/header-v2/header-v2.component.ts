import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/features/auth/login.service';
import { UserService } from 'src/app/features/user/user.service';
import { CustomService } from 'src/app/services/custom.service';
import { SessionData } from 'src/app/services/sessiondata';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-header-v2',
  templateUrl: './header-v2.component.html',
  styleUrls: ['./header-v2.component.scss']
})
export class HeaderV2Component implements OnInit {

  constructor(private loginService: LoginService, private _UserService: UserService, private _CustomService: CustomService) { }
  authData: any = '';
  userName: string = '';
  userRole: string = '';
  userInfo: any = null
  docurl = '';
  companyId = 0;
  ngOnInit(): void {
    this.docurl = environment.docurl;

  }
  handleLogout = () => {
    this.loginService.logout();
  }
}
