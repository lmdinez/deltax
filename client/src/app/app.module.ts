import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import * as interceptor from './_interceptor';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { CustomService } from './services/custom.service';
import { NgPrimeModule } from './_factory/prime.module';
import { DialogService } from 'primeng/dynamicdialog';
import { CustomModule } from './_factory/module/custom.module';
import { Store } from './_store/store';
import { HttpErrorInterceptor } from './_interceptor';
import { CommonLayoutModule } from './common/common-layout.module';
import { AppStore } from './services/appstore.service';
import { CommonService } from './services/common.service';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgPrimeModule,
    CustomModule,
    CommonLayoutModule
  ],
  exports: [

  ],
  providers: [
    Store,
    CustomService,
    DialogService,
    AppStore,
    CommonService,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: interceptor.HeaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
