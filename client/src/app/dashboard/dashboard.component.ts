import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionData } from '../services/sessiondata';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public userRole: number = 0;
  public userSettingsEnabled: boolean = false;
  public isShowMsg: boolean = true;

  constructor(
    private dashboardService: DashboardService,
    private router: Router) { }

  ngOnInit(): void {
    // this.getcompleteUser();
    this.userRole = SessionData.userInfo.userRole;
    this.userSettingsEnabled = SessionData.userInfo.accountModule === 1 ? true : false;
  }
  openUserSettings(): void {
    if (this.userSettingsEnabled) {
      console.log('User Settings Enabled');
      this.isShowMsg = false;
      this.router.navigate(['/user/list'])
    } else {
      console.log('User Settings Disabled');
    }
  }


}
