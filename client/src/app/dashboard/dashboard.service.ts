import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpService } from '../services/http.service/http.service';
import { apiurl } from '../_factory/api';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpService) { }

}
