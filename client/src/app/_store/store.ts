import { Injectable } from '@angular/core';
import { action, observable } from 'mobx';
@Injectable()
export class Store {
    @observable activeAccountTab: number | undefined = 0;
    @observable themeName: string | undefined = 'default';
    @observable companyDetail: any | undefined = {};
    @observable companySubscription: any | undefined = null;
    constructor() { }

    @action actionAccountTab(index: number) {
        this.activeAccountTab = index
    }
    @action actionHandleTheme(value: string) {
        this.themeName = value
    }
    @action actionCompanyDetail(value: any) {
        this.companyDetail = value
    }
    @action actionCompanySubscriptionDetail(value: any) {
        this.companySubscription = value
    }
}
