import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorComponent } from './common/error/error.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from './services/auth-guard.service';
const routes: Routes = [
    { path: '', redirectTo: 'auth', pathMatch: 'full' },
    { path: 'dashboard', component: DashboardComponent, data: { header: true }, canActivate: [AuthGuardService] },
    {
        path: 'auth',
        loadChildren: () => import('./features/auth/auth.module').then(mod => mod.AuthModule)
    },
    {
        path: 'account',
        loadChildren: () => import('./features/account/account.module').then(mod => mod.AccountModule)
    },
    {
        path: 'user',
        loadChildren: () => import('./features/user/user.module').then(mod => mod.UserModule)
    },
    {
        path: 'reports',
        loadChildren: () => import('./features/reports/reports.module').then(mod => mod.ReportsModule)
    },
    { path: '**', component: ErrorComponent, pathMatch: 'full' },
];
@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false, onSameUrlNavigation: 'reload' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
