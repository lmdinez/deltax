import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DigitOnlyDirective } from '../digit-only.directive';
import { InputRestrictionDirective } from '../app-input-restrictions';

@NgModule({
  declarations: [DigitOnlyDirective, InputRestrictionDirective],
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule, DigitOnlyDirective, InputRestrictionDirective
  ]
})
export class CustomModule { }
