export const MasterEnum = {
    countryCodeListMaster: [{
        "id": '',
        "code": 'Select',
        "countryName": "",
        "countryId": ''
    }],
    countryListMaster: [{
        "id": '',
        'name': 'Select'
    }],
    stateListMaster: [{
        "id": '',
        "countryId": '',
        "countryName": '',
        'name': 'Select'
    }],
    timeZoneMaster: [{
        "id": '',
        "name": "Select",
        "description": "",
        "relativeToGmt": ""
    }],
    industryTypeListMaster: [{
        "id": '',
        "name": "Select"
    }],
    userTypeList: [{
        'name': 'Select',
        'id': ''
    }],
    userRoleList: [{
        'name': 'Select',
        'id': ''
    }],
    statusList: [{
        'id': '',
        'name': 'Select'
    }],
    locationTypeMaster: [{
        "locationTypeId": '',
        "locationTypeName": "Select"
    }]
}
