export const apiurl = {
    UserManagementApi: {
        UserManagement: `/UserManagement`,
        GET_COMPLETE_USER: '/usermanagement/completeinfo',
        GET_USER_COMPANIES: '/userManagement/companies',
        USER_COMPLETE_INFO: '/UserManagement/Companies',
        SIGNUP: `/UserManagement/Signup`,
        UserManagement_Search: `/UserManagement/Search`
    },
    StatusApi: {
        status: '/status'
    },
    CountryApi: {
        countryCode: '/countryCode',
        GET_COUNTRIES: '/country',
    },
    STATE_API: {
        Get_STATES_BY_COUNTRY_ID: '/state/getbycountry',
    },
    INDUSTRYTYPE_API: {
        GET_INDUSTRY_TYPES: '/industrytype',
    },
    USERMGMT_API: {
        GET_USERS: '/company/{0}/users',
    },
    ACCOUNT_API: {
        LOGIN: '/auth/login',
        REGISTER: '/auth/register',
        GENERATE_OTP: '/auth/generate-otp',
        AUTH_WITH_OTP: '/auth/validate-otp',
        LOGIN_STEP_2: '/account/loginstep2',
        GET_LOGIN_TYPES: '/account/logintypes',
        GET_ROLES_BY_LOGIN_TYPE: '/account/roles',
        FORGOT_PASSWORD: `account/forgotPassword`,
        SET_PASSWORD: `account/setPassword`
    },
    UTILITY_API: {
        GET_COUNTRIES: '/Utility/country',
        GET_STATES: '/Utility/state/{0}',
        GET_INDUSTRY_TYPES: '/Utility/industryType',
        GET_CITIES: '/Utility/city/{0}',
        GET_TIMEZONE: '/Utility/timezone',
        GET_LANGUAGE: '/Utility/language',
        GET_USER_ROLES: '/Utility/userroles'
    },
    COMPANY_API: {
        // UPDATE_COMPANY: '/company/companyDetails',
        // SAVE_COMPANY_ADDRESSES: '/company/CompanyOtherAddress',
        // COMPANY_OTHER_ADDRESS_LIST: '/Company/OtherAddresses',
        // COMPANY_OTHER_ADDRESS: '/Company/companyOtherAddress',
        // COMPANY_OTHER_ADDRESS_UPDATE: '/Company/CompanyOtherAddress/{companyId}',
        // CREATE_COMPANY: '/Company/CompanyDetails',
        // COMPANY_DETAILS: '/Company/CompanyDetails',
        // GET: '/company',
        // ACCOUNT_PREFERENCE: `/Company/AccountPreference`,
        // THEME: `/Company/Theme/`,
        // COMPANY_SUBSCRIPTION: '/CompanySubscription',
        // COMPANY_PROFILE: `/Company/companyProfile`,
        LOCATION_TYPES: `/Company/LocationTypes/{companyId}`,
        // COMPANY_BILLING_ADDRESS: `/Company/companyBillingAddress`
        UPDATE_COMPANY: '/company/{0}',
        UPDATE_LOCATION: '/company/{0}/locations/{1}',
        GET_ADDRESS_TYPE: '/company/address/type',
        GET_LOCATIONS: 'company/:company_id/locations'
    },
    DOCUMENT_API: {
        UPLOAD_DOC: '/document/upload'
    },
    GET_USERS: '/addcard',
    Timezone: '/Timezone',
    Language: '/Language',
    THEME: '/Theme',
    LICENSE: `/License/Valid`
}
