import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule } from 'primeng/fileupload';
import { PaginatorModule } from 'primeng/paginator';
import { ProgressBarModule } from 'primeng/progressbar';
import { SidebarModule } from 'primeng/sidebar';
@NgModule({
    declarations: [],
    imports: [
        CommonModule, DialogModule, DynamicDialogModule, ProgressSpinnerModule, ToastModule,
        MessagesModule, MessageModule, FileUploadModule, PaginatorModule, ProgressBarModule, SidebarModule
    ],
    exports: [
        CommonModule, DialogModule, DynamicDialogModule, ProgressSpinnerModule, MessagesModule, MessageModule,
        ToastModule, FileUploadModule, PaginatorModule, ProgressBarModule, SidebarModule
    ]
})
export class NgPrimeModule { }
