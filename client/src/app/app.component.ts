import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';
import { CustomService } from './services/custom.service';
import { SessionData } from './services/sessiondata';
import { Store } from './_store/store';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public store: Store,
    private _CustomService: CustomService,
    private commonService: CommonService
  ) { }

  visibility = true;
  theme: string = '';
  companyId = 0;
  ngOnInit() {
    this.commonService.setTheme();
  }
  getTitle() {
    return "EsgToolKit"
  }

}
