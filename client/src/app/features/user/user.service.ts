import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service/http.service';
import { apiurl } from 'src/app/_factory/api';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpService: HttpService) { }
  getUserList(params: any, headers: any) {
    let url = apiurl.UserManagementApi.UserManagement;
    return this.httpService.Get(url, params, headers);
  }
  getUserCompanyUserList(companyId: number) {
    let url = apiurl.USERMGMT_API.GET_USERS.replace('{0}', `${companyId}`);
    return this.httpService.Get(url, null, null);
  }
  getCountryCode(params: any, headers: any) {
    let url = apiurl.CountryApi.countryCode;
    return this.httpService.Get(url, params, headers);
  }
  getCountryCodeById(params: any, headers: any, countryId: any) {
    let url = `${apiurl.CountryApi.countryCode}/Country/${countryId}`;
    return this.httpService.Get(url, params, headers);
  }
  getStatusMaster(params: any, headers: any) {
    let url = apiurl.StatusApi.status;
    return this.httpService.Get(url, params, headers);
  }
  locationTypeMaster(companyId: any) {
    let url = apiurl.COMPANY_API.LOCATION_TYPES.replace('{companyId}', companyId);
    return this.httpService.Get(url, null, null);
  }
  getUserDetail(id: number, params: any, headers: any) {
    return this.httpService.Get(apiurl.UserManagementApi.UserManagement + '/' + id, params, headers);
  }
  addUser(postData: any, actionType: number) {
    let url = apiurl.UserManagementApi.UserManagement;
    if (actionType == 0) {
      return this.httpService.Post(url, postData, null, null);
    } else {
      url = `${url}/${postData.id}`;
      return this.httpService.Put(url, postData, null, null);
    }
  }

  getUserTypeMaster(params: any, headers: any) {
    return this.httpService.Get(apiurl.ACCOUNT_API.GET_LOGIN_TYPES, params, headers);
  }
  getRolesByLoginType(id: number, params: any, headers: any) {
    return this.httpService.Get(apiurl.ACCOUNT_API.GET_ROLES_BY_LOGIN_TYPE + '/' + id, params, headers);
  }

  getUserCompanies() {
    return this.httpService.Get(apiurl.UserManagementApi.GET_USER_COMPANIES, null, null);
  }
  getUserCompleteInfo = () => {
    let url = apiurl.UserManagementApi.USER_COMPLETE_INFO;
    return this.httpService.Get(url, null, null);
  }
  searchUserList = (postDto: any, headers: any) => {
    let url = apiurl.UserManagementApi.UserManagement_Search;
    return this.httpService.Post(url, postDto, null, headers);
  }
}
