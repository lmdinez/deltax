import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/features/user/user.service';
import { NgForm } from '@angular/forms';
import { SessionData } from 'src/app/services/sessiondata';
import { MessageService } from 'primeng/api';
import { MasterEnum } from 'src/app/_factory/masterEnum';
import { CustomService } from 'src/app/services/custom.service';
const initialValue: any = {
  "userName": "",
  "password": "",
  "designation": "",
  "designationId": '',
  "id": 0,
  "firstName": "",
  "lastName": "",
  "phoneNumber": "",
  "email": "",
  "confirmPassword": "",
  "image": "",
  "role": "",
  "companyId": 0,
  "statusId": '',
  'countryCodeId': '',
  'locationTypeId': '',
  'locationTypeName': '',
  'isPrimaryContact': false
}
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['../../user.module.scss'],
  providers: [MessageService]
})
export class UserFormComponent implements OnInit {
  public hide: boolean = false;
  toastPosition = 'top-center';
  dialogConfigData: any;
  submitted: boolean = false;
  desitionId = 0;
  userFormAttribute: any = {
    ...initialValue
  };
  passwordType = 'password';
  master: any = {
    userTypeList: MasterEnum?.userTypeList,
    userRoleList: [],
    statusList: MasterEnum?.statusList,
    countryCodeListMaster: MasterEnum?.countryCodeListMaster,
    locationTypeMaster: MasterEnum?.locationTypeMaster
  }
  constructor(private _UserService: UserService, private messageService: MessageService, private customeService: CustomService) { }

  ngOnInit(): void {
    // this.dialogConfigData = this.config.data.res;
    // if (this.dialogConfigData?.actionType == 1) {
    //   this.createForm(() => {
    //     this.loadPageData();
    //   })
    // } else {
    //   this.loadPageData();
    // }

  }
  // createForm = (callback: any) => {
  //   this.userFormAttribute = this.dialogConfigData.initialValue;
  //   Object.keys(initialValue).forEach((key) => {
  //     this.userFormAttribute[key] = this.dialogConfigData.initialValue[key]
  //   })
  //   if (callback) {
  //     callback()
  //   }
  // }
  // toastMsg(severity: any, summary: any, detail: any, position: any) {
  //   this.toastPosition = position
  //   this.messageService.add({ key: 'addEditUser', severity: severity, summary: summary, detail: detail });
  // }
  // loadPageData = () => {
  //   this.getCountryCode();
  //   this.getStatusMaster();
  //   this.getLocationTypeMaster();
  //   this.handleUserTypeMaster();
  //   if (this.userFormAttribute.designationId) {
  //     this.desitionId = parseInt(this.userFormAttribute.designationId);
  //     this.loadUserRoles()
  //   }
  // }
  // handleUserTypeMaster = () => {
  //   this._UserService.getUserTypeMaster(null, null).subscribe((res) => {
  //     this.master.userTypeList = [...MasterEnum?.userTypeList, ...res?.result];
  //   });
  // }
  // getCountryCode = () => {
  //   const params = {
  //     'page': '0',
  //     'pageLimit': '2000'
  //   }
  //   this._UserService.getCountryCode(params, params).subscribe((res) => {
  //     this.master.countryCodeListMaster = [...MasterEnum?.countryCodeListMaster, ...res?.result.data]
  //   });
  // }
  // getStatusMaster = () => {
  //   const params = {
  //     'page': '0',
  //     'pageLimit': '2000'
  //   }
  //   this._UserService.getStatusMaster(params, params).subscribe((res) => {
  //     this.master.statusList = res?.result?.data
  //   });
  // }
  // getLocationTypeMaster = () => {
  //   this._UserService.locationTypeMaster(this.dialogConfigData.companyId).subscribe((res) => {
  //     this.master.locationTypeMaster = [...MasterEnum?.locationTypeMaster, ...res?.result]
  //   });
  // }
  // loadUserRoles = () => {
  //   this._UserService.getRolesByLoginType(this.desitionId, null, null).subscribe((res) => {
  //     this.master.userRoleList = [...res.result];
  //   });
  // }
  // handlePasswordType = () => {
  //   if (this.passwordType == 'text') {
  //     this.passwordType = 'password'
  //   } else {
  //     this.passwordType = 'text'
  //   }
  // }
  // handleLocationChange = (event: any) => {
  //   const index = event.currentTarget.selectedIndex;
  //   const name = this.master.userTypeList[index].name;
  //   this.userFormAttribute.locationTypeName = name;
  // }
  // handleDesinationChange = (event: any) => {
  //   const index = event.currentTarget.selectedIndex;
  //   this.desitionId = event.target.value ? parseInt(this.master.userTypeList[index].id) : 0;
  //   const designation = this.master.userTypeList[index].name;
  //   this.userFormAttribute.designation = designation;
  //   this.userFormAttribute.role = '';
  //   this.master.userRoleList = MasterEnum?.userRoleList;
  //   this.loadUserRoles()
  // }

  // handleFormSubmit = (myForm: NgForm) => {
  //   this.submitted = true;
  //   if (myForm.form.status !== 'INVALID') {
  //     this.submitted = false;
  //     const element = this.userFormAttribute
  //     const postData = {
  //       "password": element.password,
  //       "designation": element.designation,
  //       "designationId": element.designationId,
  //       "id": element.id,
  //       "firstName": element.firstName,
  //       "lastName": element.lastName,
  //       "phoneNumber": element.phoneNumber,
  //       "email": element.email,
  //       "confirmPassword": element.confirmPassword,
  //       "image": element.image,
  //       "role": element.role,
  //       "companyId": element.companyId,
  //       "statusId": element.statusId,
  //       'countryCodeId': element.countryCodeId,
  //       'locationTypeId': element.locationTypeId,
  //       'locationTypeName': element.locationTypeName,
  //       'isPrimaryContact': element.isPrimaryContact,
  //     }
  //     this._UserService.addUser(postData, this.dialogConfigData?.actionType).subscribe((response) => {
  //       let summary = `${this.dialogConfigData?.actionType == 0 ? 'Add' : 'Update'} User`;
  //       if (response && response.responseCode == 200) {
  //         let detail = `User ${this.dialogConfigData?.actionType == 0 ? ' added ' : ' updated '} successfully`;
  //         this.toastMsg('success', summary, detail, this.toastPosition)
  //       } else {
  //         let detail = `Error while ${this.dialogConfigData?.actionType == 0 ? ' adding ' : ' updating '} user`;
  //         this.toastMsg('error', summary, detail, this.toastPosition)
  //       }
  //       setTimeout(() => {
  //         this.handleFormCancel(true)
  //       }, 3000);
  //     }, (err) => {
  //       this.toastMsg('error', 'Form Validation Error', this.customeService.getFormatedError(err), this.toastPosition);
  //     });
  //   } else {
  //     this.toastMsg('error', 'Form Validation Error', 'Please fill all required fields', this.toastPosition)
  //   }
  // }
  // clearForm(callback: any) {
  //   (<HTMLFormElement>document.getElementById("userForm")).reset();
  //   if (callback) {
  //     callback()
  //   }
  // }
  // resetForm = (callback: any) => {
  //   this.clearForm(() => {
  //     this.submitted = false;
  //     if (callback) {
  //       callback()
  //     }
  //   })
  // }
  // handleFormCancel = (value: any) => {
  //   this.resetForm(() => {
  //     this.ref.close(value);
  //   });
  // }
  // handleFormClose = (myForm: NgForm) => {
  //   this.resetForm(() => {
  //     this.ref.close();
  //   });
  // }
}
