import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../user.service';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { FormGroup, NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { SessionData } from 'src/app/services/sessiondata';
import { Store } from 'src/app/_store/store';
import { CustomService } from '../../../../services/custom.service';
import { UserFormComponent } from '../user-form/user-form.component';
import { filter } from 'lodash';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  providers: [MessageService, PrimeNGConfig]
})

export class UserListComponent implements OnInit {
  constructor(private _UserService: UserService, private messageService: MessageService, public store: Store, private customeService: CustomService) { }
  formInput: FormGroup | undefined;
  selectedTabUrl: number = 2;
  userDialog: boolean = false;
  toastPosition = 'top-center';
  position = 'right';
  page = 0;
  companyId = 0;
  pageLimit = environment.PAGE_LIMIT;
  searchParam: any = {

  }
  filtered: any = [];
  usersList: any = [];
  master = {
    userTypeList: [{
      name: '',
      id: ''
    }],
    userRoleList: [{
      name: '',
      id: ''
    }],
    statusList: [{
      id: '',
      name: ''
    }],
    locationTypeMaster: [
      {
        "locationTypeId": '',
        "locationTypeName": ""
      }
    ]
  }
  userListMsg: boolean = true;
  submitted: boolean = false;
  actionType: number = 0;
  desitionId = 0;
  status = 0;
  displayFilterDialog = false;
  filterDto = {
    "userName": "",
    "roleId": '',
    "designationId": '',
    "statusId": '',
    "locationTypeId": '',
    "email": "",
    "phoneNumber": ""
  }
  dialogConfig: any = {}
  ngOnInit(): void {
    // this.companyId = (SessionData.userCompany == null || SessionData.userCompany === '') ? 0 : +SessionData.userCompany;
    // this.getAuthInfo();
    // this.getUserList(() => {
    //   this.handleStartNumber();
    //   this.handleEndNumber();
    // });
    // this.getStatusMaster();
    // this.getLocationTypeMaster();
    // this.handleUserTypeMaster(() => { })

    this.getUserList();

  }
  // getAuthInfo = () => {
  //   this.authData = SessionData.userInfo;
  // }
  // getLocationTypeMaster = () => {
  //   this._UserService.locationTypeMaster(this.companyId).subscribe((res) => {
  //     this.master.locationTypeMaster = res?.result
  //   });
  // }
  // getStatusMaster = () => {
  //   const params = {
  //     'page': '0',
  //     'pageLimit': '2000'
  //   }
  //   this._UserService.getStatusMaster(params, params).subscribe((res) => {
  //     this.master.statusList = res?.result?.data
  //   });
  // }
  // toastMsg(severity: any, summary: any, detail: any, position: any) {
  //   this.toastPosition = position
  //   this.messageService.add({ key: 'addEditCustomer', severity: severity, summary: summary, detail: detail });
  // }
  public handleTab(status: number): void {
    this.selectedTabUrl = status;

    switch (status) {
      case 0:
        this.filtered = filter(this.usersList, user => user.userStatus === 0);
        break;
      case 1:
        this.filtered = filter(this.usersList, user => user.userStatus === 1);
        break;
      default:
        this.filtered = this.usersList;
        break;
    }
    // this.filtered = {
    //   data: [],
    //   totalCount: -1
    // };
    // this.page = 0;
    // this.status = status;
    // this.getUserList(() => {
    //   this.handleStartNumber();
    //   this.handleEndNumber();
    // })
  }
  // handleStartNumber = () => {
  //   let temp = 0;
  //   temp = (this.page * this.pageLimit) + 1
  //   return temp;
  // }
  // handleEndNumber = () => {
  //   let temp = 0;
  //   temp = (this.pageLimit * (this.page + 1))
  //   if (this.filtered?.totalCount < temp) {
  //     temp = this.filtered?.totalCount
  //   }
  //   return temp;
  // }

  public addUser(): void {
    this.userDialog = true;
  }
  public getUserList(): void {
    this.companyId = SessionData.userInfo.companyId;
    this._UserService.getUserCompanyUserList(this.companyId).subscribe((res) => {
      if (res.status === "SUCCESS") {
        this.usersList = res.users;
        this.filtered = res.users;
      } else {
        this.filtered = [];
      }
    }, (err) => {
      this.filtered = [];
    });
  }

  // handleUserTypeMaster = (callback: any) => {
  //   this._UserService.getUserTypeMaster(null, null).subscribe((res) => {
  //     this.master.userTypeList = res?.result;
  //     if (callback) {
  //       callback()
  //     }
  //   });
  // }
  // loadUserRoles = (callback: any) => {
  //   this._UserService.getRolesByLoginType(this.desitionId, null, null).subscribe((res) => {
  //     this.master.userRoleList = res.result;
  //     if (callback) {
  //       callback()
  //     }
  //   });
  // }
  handleUserMsgBox = () => {
    this.userListMsg = false
  }
  // addEditDialog(type: number, item: any) {
  //   this.actionType = type
  //   this.dialogConfig = {
  //     header: type == 0 ? 'Add User' : 'Edit User',
  //     actionType: type,
  //     companyId: this.companyId,
  //     companySubscription: this.store.companySubscription,
  //     width: '796px'
  //   }
  //   if (type == 1) {
  //     this.dialogConfig.initialValue = {
  //       ...item
  //     }
  //   }
  //   this.customeService.dialogComponentConfig(this.dialogConfig, UserFormComponent).subscribe(res => {
  //     if (res) {
  //       this.resetFilter()
  //     }
  //   })
  // }
  /* evnet handler */
  handleDesinationChange = (event: any, actionType: any) => {
    // const index = event.currentTarget.selectedIndex - 1;
    // this.desitionId = parseInt(this.master.userTypeList[index].id);
    // if (actionType == 1) {

    // } else {
    //   this.filterDto.roleId = '';
    // }
    // this.loadUserRoles(() => { })
  }
  // handleLocationChange = (event: any) => {
  //   const index = event.currentTarget.selectedIndex - 1;
  //   const name = this.master.userTypeList[index].name;
  // }
  // loadUserList = (() => {
  //   this.displayFilterDialog = false;
  //   this.getUserList(() => {
  //     this.handleStartNumber();
  //     this.handleEndNumber();
  //   })
  // })
  // searchUserList = (() => {
  //   this.applyFilter(() => {
  //     this.displayFilterDialog = false;
  //     this.handleStartNumber();
  //     this.handleEndNumber();
  //   })
  // })
  // paginate(event: any) {
  //   if (this.page !== event.page) {
  //     this.page = event.page;
  //     if (this.displayFilterDialog) {
  //       this.searchUserList();
  //     } else {
  //       this.loadUserList()
  //     }
  //   }
  // }
  openFilterdialog = () => {
    this.displayFilterDialog = true;
    // if (this.filterDto.designationId) {
    //   this.desitionId = parseInt(this.filterDto.designationId);
    //   this.loadUserRoles(() => {
    //     this.displayFilterDialog = true;
    //   })
    // } else {
    //   this.desitionId = 0;
    //   this.master.userRoleList = [{
    //     name: '',
    //     id: ''
    //   }]
    //   this.displayFilterDialog = true;
    // }

  }
  // handleApplyFilter = () => {
  //   this.page = 0;
  //   this.searchUserList();
  // }
  // resetFilter = () => {
  //   this.filterDto = {
  //     "userName": "",
  //     "roleId": '',
  //     "designationId": '',
  //     "statusId": '',
  //     "locationTypeId": '',
  //     "email": "",
  //     "phoneNumber": ""
  //   }
  //   this.page = 0;
  //   this.loadUserList()
  // }
  // applyFilter = (callback: any) => {
  //   const headers = {
  //     'page': this.page,
  //     'pageLimit': this.pageLimit.toString()
  //   }
  //   const postDto = {
  //     "userName": this.filterDto.userName,
  //     "roleId": this.filterDto.roleId && parseInt(this.filterDto.roleId) || 0,
  //     "designationId": this.filterDto.designationId && parseInt(this.filterDto.designationId) || 0,
  //     "statusId": this.filterDto.statusId && parseInt(this.filterDto.statusId) || 1,
  //     "locationTypeId": this.filterDto.locationTypeId && parseInt(this.filterDto.locationTypeId) || 0,
  //     "email": this.filterDto.email,
  //     "phoneNumber": this.filterDto.phoneNumber,
  //   }
  //   this._UserService.searchUserList(postDto, headers).subscribe((res) => {
  //     this.filtered = res?.result;
  //     if (callback) {
  //       callback()
  //     }
  //   })
  // }
}
