import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { UserListComponent } from './component/user-list/user-list.component';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
const userRoutes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: UserListComponent, data: { header: true }, canActivate: [AuthGuardService] },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
