import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportsRoutingModule } from './reports-routing.module';

import { MainComponent } from './main/main.component';

import { HeaderV2Component } from '../../common/header/header-v2/header-v2.component';

@NgModule({
  declarations: [
    HeaderV2Component,
    MainComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ReportsRoutingModule
  ]
})
export class ReportsModule { }
