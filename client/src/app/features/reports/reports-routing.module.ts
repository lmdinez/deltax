import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { MainComponent } from './main/main.component';
import { DataTemplatesComponent } from './components/data-templates/data-templates.component';
const reportsRoutes = [
    {
        path: '',
        component: MainComponent,
        children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'datatemplates', component: DataTemplatesComponent, data: { header: true } },

        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(reportsRoutes)],
    exports: [RouterModule]
})
export class ReportsRoutingModule { }
