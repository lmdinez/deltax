import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MFAModel } from 'src/app/features/models/loginmodel';
import { LoginService } from '../../../login.service';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signup-by-otp',
  templateUrl: './signup-by-otp.component.html',
  styleUrls: ['./signup-by-otp.component.css'],
  providers: [MessageService]
})

export class SignupByOTPComponent implements OnInit {
  @Input() userInfo: any = {};
  public signupDetail: MFAModel = {
    userName: '',
    otpCode: ''
  };
  config: any = {
    allowNumbersOnly: true,
    length: 4,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      'margin-right': '12px'
    }
  };
  public countDown: Subscription;
  public counter = 900;
  public tick = 1000;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig
  ) {
    this.countDown = new Subscription();
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.sendOTP();
  }

  onOtpChange = (event: any) => {
    this.signupDetail.otpCode = event;
    //console.log('OTP', event);
  }

  sendOTP() {
    if (this.userInfo.UserName !== '') {
      this.loginService.generateOTP(this.userInfo.UserName).subscribe(res => {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'OTP sent to your mail' });
        this.counter = 900;
        this.countDown = this.loginService.getCounter(this.tick).subscribe(() => this.counter--);
      }, err => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something went wrong, please try again later' });
      });
    } else {
      this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Something went wrong, Please try again later' });
    }
  }

  submitOTP() {
    if (this.signupDetail.otpCode === '' || this.signupDetail.otpCode.length !== 4) {
      return;
    }
    this.signupDetail.userName = this.userInfo.UserName;
    this.loginService.loginWithOtp(this.signupDetail).subscribe((loginResult) => {
      if (loginResult.status === 'SUCCESS') {
        this.loginService.loginSuccess(loginResult.result);
        this.router.navigate(['/dashboard'], {});
      }
      else {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid Username/ Password' });
      }
    }, (err) => {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid Username/ Password' });
    });

  }

}

