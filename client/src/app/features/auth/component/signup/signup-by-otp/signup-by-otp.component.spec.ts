import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupByOTPComponent } from './signup-by-otp.component';

describe('SignupByOTPComponent', () => {
  let component: SignupByOTPComponent;
  let fixture: ComponentFixture<SignupByOTPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignupByOTPComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupByOTPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
