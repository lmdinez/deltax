import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../login.service';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { FormBuilder, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/_factory/must-match.validator';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})

export class SignupComponent implements OnInit {
  public welcomeMsg: string = 'Welcome to ESG Futuretech';
  signupSteps = {
    activeStep: 1
  }
  constructor() { }

  ngOnInit(): void {
    this.setWelcomeMsg();
  }
  eventFromChild(data: any) {
    console.log(data);
    this.signupSteps = { ...data };
  }

  setWelcomeMsg(): void {
    switch (this.signupSteps.activeStep) {
      case 2:
        this.welcomeMsg = 'We have sent an OTP to your email address';
        break;
      default:
      case 1:
        this.welcomeMsg = 'Welcome to ESG Futuretech';
        break;
    }
  }

}

