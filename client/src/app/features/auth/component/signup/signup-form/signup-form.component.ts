import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../../login.service';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/_factory/must-match.validator';
import { environment } from 'src/environments/environment';
import { SignupModel } from '../../../../models/loginmodel';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss'],
  providers: [MessageService]
})

export class SignupFormComponent implements OnInit {
  @Input() inputUser: string = '';
  @Output() sendDataToParent = new EventEmitter<string>();

  public hide = true;
  public isShowExpires = false;
  public siteKey = environment.CAPTCHA_KEY;
  public validateCaptcha = false;
  public signupForm: FormGroup = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    phoneno: ['', [Validators.required]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPassword: ['', Validators.required],
    acceptTerms: [false, Validators.requiredTrue]
  }, {
    validator: MustMatch('password', 'confirmPassword')
  });

  constructor(
    private loginService: LoginService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

  signupByOTP(email: string) {
    let data: any = {
      activeStep: 2,
      UserName: email
    }
    this.sendDataToParent.emit(data);
  }

  showResponse(event: any) {
    if (event.response) {
      this.validateCaptcha = true
    }
  }

  resetCaptchaResponse() {
    this.validateCaptcha = false;
  }

  submit(signupFormDirective: FormGroupDirective) {
    // console.log(this.signupForm.value);
    if (this.signupForm.invalid || !this.validateCaptcha) {
      return;
    }
    const signupObj: SignupModel = {
      firstName: this.signupForm.value.firstName,
      lastName: this.signupForm.value.lastName,
      email: this.signupForm.value.email,
      password: this.signupForm.value.password,
      phoneNumber: this.signupForm.value.phoneno,
      role_id: 1
    };
    this.loginService.signup(signupObj).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.signupByOTP(signupObj.email);
      } else {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Please contact ESG Team for further assistance.' });
      }
      this.signupForm.reset();
      signupFormDirective.resetForm();
    }, error => {
      this.messageService.add({ sticky: true, severity: 'error', summary: 'Error', detail: 'Sorry, Something went Wrong. Try Again later.' });
    });
  }

}

