import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MFAModel } from 'src/app/features/models/loginmodel';
import { LoginService } from '../../../login.service';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login-with-otp',
  templateUrl: './login-with-otp.component.html',
  styleUrls: ['./login-with-otp.component.css'],
  providers: [MessageService]
})

export class LoginWithOTPComponent implements OnInit {
  @Input() inputUser: string = '';
  @Output() sendDataToParent = new EventEmitter<string>();

  public hide = true;
  public isShowExpires = false;
  public siteKey = environment.CAPTCHA_KEY;
  public validateCaptcha = false;
  public frmUserName = new FormControl('', [Validators.required, Validators.email]);
  public frmPassword = new FormControl('', [Validators.required]);
  public loginDetail: MFAModel = {
    userName: '',
    otpCode: ''
  };
  public countDown: Subscription;
  public counter = 900;
  public tick = 1000;

  constructor(
    private loginService: LoginService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private router: Router
  ) {
    this.countDown = new Subscription();
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.loginDetail.userName = this.inputUser;
  }

  sendOTP() {
    if (this.loginDetail.userName !== '') {
      this.loginService.generateOTP(this.loginDetail.userName).subscribe(res => {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'OTP sent to your mail' });
        this.isShowExpires = true;
        this.counter = 900;
        this.countDown = this.loginService.getCounter(this.tick).subscribe(() => this.counter--);
      }, err => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something went wrong, please try again later' });
      });
    } else {
      this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Please enter your mail.' });
    }
  }

  gotoSignup() {
    this.router.navigate(['/auth/signup'], {});
  }

  showResponse(event: any) {
    if (event.response) {
      this.validateCaptcha = true
    }
  }

  resetCaptchaResponse() {
    this.validateCaptcha = false;
  }

  _sendDataToParent() {
    if (this.loginDetail.userName === '' || this.loginDetail.otpCode === '' || !this.validateCaptcha) {
      return;
    }
    this.loginService.loginWithOtp(this.loginDetail).subscribe(loginResult => {
      console.log('Login Result', loginResult);
      if (loginResult.status === 'SUCCESS' || loginResult.result.accessToken !== '') {
        this.loginService.loginSuccess(loginResult.result);
        this.router.navigate(['/dashboard'], {});
      }
      else {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid userName/ Password' });
      }
    }, error => {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid userName/ Password' });
    });
  }

}

