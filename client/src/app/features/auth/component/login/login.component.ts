import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }
  loginSteps = {
    activeStep: 2,
    title: '',
    userName: '',
    userLogin: {
      designation: '',
      password: '',
      username: '',
    }
  }
  ngOnInit(): void {
  }
  eventFromChild(data: any) {
    console.log(data);

    this.loginSteps = { ...data };
  }

}
