import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { LoginModel } from 'src/app/features/models/loginmodel';
import { environment } from 'src/environments/environment';
import { LoginService } from '../../../login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
  providers: [MessageService]
})
export class LoginFormComponent implements OnInit {
  @Output() sendDataToParent = new EventEmitter<string>();

  public hide = true;
  public siteKey = environment.CAPTCHA_KEY;
  public validateCaptcha = false;
  public frmUserName = new FormControl('', [Validators.required, Validators.email]);
  public frmPassword = new FormControl('', [Validators.required]);
  public loginDetail: LoginModel = {
    userName: '',
    password: ''
  };

  constructor(
    private loginService: LoginService,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

  showResponse(event: any) {
    if (event.response) {
      this.validateCaptcha = true
    }
  }

  resetCaptchaResponse() {
    this.validateCaptcha = false;
  }

  loginWithOTP() {
    let data: any = {
      activeStep: 4,
      title: '',
      userName: this.loginDetail.userName
    }
    this.sendDataToParent.emit(data);
  }

  _sendDataToParent() {
    if (this.loginDetail.userName === '' || this.loginDetail.password === '' || !this.validateCaptcha) {
      return;
    }
    this.loginService.login(this.loginDetail).subscribe(loginResult => {
      console.log('Login Result', loginResult);
      if (loginResult.status === 'SUCCESS' || loginResult.result.accessToken !== '') {
        this.loginService.loginSuccess(loginResult.result);
        this.router.navigate(['/dashboard'], {});
      }
      else {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid UserName/ Password' });
      }
    }, error => {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid UserName/ Password' });
    });
  }

}
