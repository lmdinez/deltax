import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgPrimeModule } from 'src/app/_factory/prime.module';
import { NgOtpInputModule } from 'ng-otp-input';
import { CaptchaModule } from 'primeng/captcha';

import { AuthRoutingModule } from './auth-routing.module';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { MainComponent } from './main/main.component';
import { LoginComponent } from './component/login/login.component';
import { LoginFormComponent } from './component/login/login-form/login-form.component';
import { LoginWithOTPComponent } from './component/login/login-with-otp/login-with-otp.component';
import { SignupComponent } from './component/signup/signup.component';
import { SignupFormComponent } from './component/signup/signup-form/signup-form.component';
import { SignupByOTPComponent } from './component/signup/signup-by-otp/signup-by-otp.component';
import { FormatTimePipe } from 'src/app/_factory/pipe/formatTime.pipe';

@NgModule({
  declarations: [
    MainComponent,
    LoginComponent,
    LoginFormComponent,
    LoginWithOTPComponent,
    SignupComponent,
    SignupFormComponent,
    SignupByOTPComponent,
    FormatTimePipe,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    NgPrimeModule,
    NgOtpInputModule,
    ReactiveFormsModule,
    CaptchaModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCheckboxModule
  ]
})
export class AuthModule { }
