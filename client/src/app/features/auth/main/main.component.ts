import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionData } from 'src/app/services/sessiondata';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.checkIfLoggedIn()
  }
  checkIfLoggedIn = () => {
    if (SessionData.AuthToken) {
      this.router.navigate(['/dashboard'])
    }
  }
}
