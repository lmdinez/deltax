import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import { LoginModel, MFAModel, SignupModel } from 'src/app/features/models/loginmodel';
import { HttpService } from 'src/app/services/http.service/http.service';
import { Router } from '@angular/router';
import { SessionData } from 'src/app/services/sessiondata';
import { CommonService } from 'src/app/services/common.service';
import { apiurl } from 'src/app/_factory/api';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  @Output() fireIsLoggedIn: EventEmitter<any> = new EventEmitter<any>();
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  loginResult = false;
  constructor(
    private httpService: HttpService,
    private _Router: Router,
    private commonService: CommonService
  ) {
    let tempUserInfo: any;
    tempUserInfo = new BehaviorSubject<any>(SessionData?.userInfo)
    this.currentUserSubject = tempUserInfo;
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  getCounter(tick: number) {
    return timer(0, tick);
  }

  loginSuccess(res: any): boolean {
    SessionData.AuthToken = res.accessToken;
    delete res.accessToken;
    // res.userRole = 'superuser';
    // res.authModule = 0;
    // res.userStatus = 1;
    // res.theme = 'green-theme';
    SessionData.userInfo = JSON.stringify(res);
    this.commonService.setTheme(res.theme);
    setTimeout(() => {
      this.currentUserSubject.next(res);
      this.fireIsLoggedIn.emit();
    }, 300)

    return true;
  }

  getAuthEmitter() {
    return this.fireIsLoggedIn;
  }

  logout() {
    SessionData.userCompany = null;
    SessionData.AuthToken = null;
    SessionData.userInfo = null;
    localStorage.removeItem('esgForwardUrl');
    // this.currentUserSubject.next(null);
    // this.fireIsLoggedIn.emit()
    this._Router.navigate(['/auth/login']);
  }

  login(login: LoginModel) {
    return this.httpService.Post(apiurl.ACCOUNT_API.LOGIN, login, null, null);
  }

  generateOTP(userName: string) {
    return this.httpService.Post(apiurl.ACCOUNT_API.GENERATE_OTP, { userName }, null, null)
  }

  loginWithOtp(mfaDetail: MFAModel) {
    return this.httpService.Post(apiurl.ACCOUNT_API.AUTH_WITH_OTP, mfaDetail, null, null);
  }

  signup(signup: SignupModel) {
    return this.httpService.Post(apiurl.ACCOUNT_API.REGISTER, signup, null, null);
  }

  login2(twoFactorAuthenticationModel: any) {
    return this.httpService.Post(apiurl.ACCOUNT_API.LOGIN_STEP_2, twoFactorAuthenticationModel, null, null);
  }

  getLoginTypes() {
    return this.httpService.Get(apiurl.ACCOUNT_API.GET_LOGIN_TYPES, null, null);
  }

  getRolesByLoginType(id: number) {
    return this.httpService.Get(apiurl.ACCOUNT_API.GET_ROLES_BY_LOGIN_TYPE + '/' + id, null, null);
  }

  userSignUp(postData: any) {
    let url = apiurl.UserManagementApi.SIGNUP;
    return this.httpService.Post(url, postData, null, null);
  }

  checkSubscriptionValid(postData: any) {
    let url = apiurl.LICENSE;
    return this.httpService.Post(url, postData, null, null);
  }

  forgotPasswordEmail(param: any) {
    let url = apiurl.ACCOUNT_API.FORGOT_PASSWORD;
    return this.httpService.Get(url, param, null);
  }
  setPassword(postDto: any) {
    let url = apiurl.ACCOUNT_API.SET_PASSWORD;
    return this.httpService.Post(url, postDto, null, null);
  }
}
