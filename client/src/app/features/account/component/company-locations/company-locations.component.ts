import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { SessionData } from 'src/app/services/sessiondata';
import { get } from 'lodash';

import { AccountService } from '../../services/account.service';

@Component({
  selector: 'app-acc-company-locations',
  templateUrl: './company-locations.component.html',
  styleUrls: ['./company-locations.component.scss'],
  providers: [MessageService]
})
export class CompanyLocationsComponent implements OnInit {
  public companyLocations: FormGroup;

  public countries: any = [];
  public states: any = [];
  public cities: any = [];
  public timezones: any = [];
  public addressTypes: any = [];
  public companyId: number = 0;
  public locationId: number = 0;
  public locations: any = [];
  public createdBy = '';

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private messageService: MessageService
  ) {
    this.companyLocations = new FormGroup({
      companyName: new FormControl(null, { validators: [Validators.required] }),
      locationName: new FormControl(null, { validators: [Validators.required] }),
      locationDescription: new FormControl(null, null),
      siteId: new FormControl('', { validators: [Validators.required] }),
      addressTypeId: new FormControl(null, { validators: [Validators.required] }),
      timeZoneID: new FormControl(null, { validators: [Validators.required] }),
      addressLine1: new FormControl(null, { validators: [Validators.required] }),
      addressLine2: new FormControl(null, []),
      countryId: new FormControl(null, { validators: [Validators.required] }),
      stateId: new FormControl(null, { validators: [Validators.required] }),
      cityId: new FormControl(null, { validators: [Validators.required] }),
      zipCode: new FormControl(null, { validators: [Validators.required] }),
      status: new FormControl(null, { validators: [Validators.required] }),
    });
  }

  ngOnInit(): void {
    // this.companyLocations.setValue({
    //   companyName: '',
    //   locationName: '',
    //   locationDescription: '',
    //   siteId: '12XXX-XXXX'
    // })
    const companyId = get(SessionData.userInfo, 'companyId', null);
    this.createdBy = get(SessionData.userInfo, 'email');
    this.loadCountries();
    this.loadAddressTypes();
    this.loadTimezones();
    this.loadLocations(1);
    this.companyId = 1;
    this.locationId = 0;

    if (companyId !== null) {
      this.loadCountries();
      this.loadAddressTypes();
      this.loadTimezones();
      this.loadLocations(companyId);
    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Add Company details to proceed locations.' });
    }

  }

  loadCountries() {
    this.accountService.getCountries().subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.countries = res.countries;
      }
    }, err => {
      console.log('Country is not loaded.')
    });
  }

  loadStates(countryId: number) {
    this.accountService.getStates(countryId).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.states = res.states;
      } else {
        this.states = [];
      }
    }, err => {
      this.states = [];
      console.log('States is not loaded.')
    });
  }

  loadCities(stateId: number) {
    this.accountService.getCities(stateId).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.cities = res.cities;
      } else {
        this.cities = [];
      }
    }, err => {
      this.cities = [];
      console.log('Cities is not loaded.')
    });
  }

  loadTimezones(): void {
    this.accountService.getTimezones().subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.timezones = res.timezones;
        // this.companyLocations.get('timeZone')?.setValue(1);
      }
    }, err => {
      console.log('Timezone is not loaded.')
    });
  }

  loadAddressTypes(): void {
    this.accountService.getAddressTypes().subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.addressTypes = res.data;
      }
    }, err => {
      console.log('Address is not loaded.')
    });
  }

  loadLocations(companyId: number) {
    this.accountService.getLocations(companyId).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.addressTypes = res.locations;
      }
    }, err => {
      console.log('Address is not loaded.')
    });
  }

  submit() {
    let companyObj = { ...this.companyLocations.value };
    companyObj.createdBy = 2; // this.createdBy
    console.log('submitted', this.companyLocations.value);
    this.accountService.postLocationDetails(this.companyId, this.locationId, companyObj).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Company details added/updated successfully.' });
      } else {
        this.messageService.add({ severity: 'error', summary: 'Success', detail: 'Sorry, Something went wrong.' });
      }
    }, error => {
      this.messageService.add({ sticky: true, severity: 'error', summary: 'Error', detail: 'Sorry, Something went Wrong. Try Again later.' });
    });
  }
}
