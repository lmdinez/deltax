import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { CommonService } from 'src/app/services/common.service';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'app-acc-company-preference',
  templateUrl: './company-preference.component.html',
  styleUrls: ['./company-preference.component.scss']
})
export class CompanyPreferenceComponent implements OnInit, OnDestroy {
  public companyPreference: FormGroup;
  public selectedTheme = '';
  public timezones: any = [];
  public languages: any = [];

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private accountService: AccountService) {
    this.companyPreference = new FormGroup({
      language: new FormControl(null, { validators: [Validators.required] }),
      timeZone: new FormControl(null, { validators: [Validators.required] }),
    });
  }

  ngOnInit(): void {
    this.loadLanguages();
    this.loadTimezones();
    this.selectedTheme = this.commonService.getCompanySelectedTheme();
  }

  loadTimezones(): void {
    this.accountService.getTimezones().subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.timezones = res.timezones;
        this.companyPreference.get('timeZone')?.setValue(1);
      }
    }, err => {
      console.log('Country is not loaded.')
    });
  }

  loadLanguages(): void {
    this.accountService.getLanguages().subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.languages = res.languages;
        this.companyPreference.get('language')?.setValue(1);
      }
    }, err => {
      console.log('Country is not loaded.')
    });
  }

  chooseTheme(theme: string): void {
    this.commonService.setThemeForView(this.selectedTheme, theme);
    this.selectedTheme = theme
  }

  submit() {
    console.log('submitted');
    this.commonService.setTheme(this.selectedTheme);
  }

  ngOnDestroy() {
    this.chooseTheme(this.commonService.getCompanySelectedTheme());
  }
}
