import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../services/account.service';
import { SessionData } from 'src/app/services/sessiondata';
import { filter, get } from 'lodash';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-acc-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss'],
  providers: [MessageService]
})
export class CompanyDetailsComponent implements OnInit {
  public companyDetails: FormGroup;
  public countries: any = [];
  public industryTypes: any = [];
  public states: any = [];
  public billingStates: any = [];
  public billingCities: any = [];
  public companyId: number = 0;
  public initialDepts: any = [{ id: 0, value: '' }, { id: 0, value: '' }];
  public deptartmentArray: any = [];
  public createdBy = '';

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private messageService: MessageService,
  ) {
    this.companyDetails = new FormGroup({
      companyName: new FormControl(null, { validators: [Validators.required] }),
      industryTypeId: new FormControl(null, { validators: [Validators.required] }),
      companyCountryId: new FormControl(null, { validators: [Validators.required] }),
      companyStateId: new FormControl(null, null),
      addressLine1: new FormControl(null, { validators: [Validators.required] }),
      addressLine2: new FormControl(null, []),
      billingCountryId: new FormControl(null, { validators: [Validators.required] }),
      billingStateId: new FormControl(null, { validators: [Validators.required] }),
      billingCityId: new FormControl(null, { validators: [Validators.required] }),
      zipCode: new FormControl(null, { validators: [Validators.required] }),
      primaryContactName: new FormControl(null, { validators: [Validators.required] }),
      primaryContactCCCodeId: new FormControl(null, null),
      primaryContactNumber: new FormControl(null, null),
      contactMail: new FormControl(null, { validators: [Validators.required, Validators.email] }),
      deptArray: this.formBuilder.array([])
    });
  }

  get deptArray() {
    return this.companyDetails.get("deptArray") as FormArray;
  }

  ngOnInit(): void {
    const companyId = get(SessionData.userInfo, 'companyId', null);
    this.createdBy = get(SessionData.userInfo, 'email');
    if (companyId !== null) {
      // Edit the page.
      this.companyId = companyId;

    } else {
      this.deptartmentArray = this.initialDepts;
      this.deptartmentArray.map((dept: any) => {
        this.deptArray.push(this.formBuilder.control('', { validators: [Validators.required] }));
      });
      this.loadIndustryTypes();
      this.loadCountries();
    }

  }
  addDeptControl() {
    const dept = { id: 0, value: '' };
    this.deptartmentArray.push(dept);
    this.deptArray.push(this.formBuilder.control('', { validators: [Validators.required] }));
  }

  loadIndustryTypes() {
    this.accountService.getIndustryTypes().subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.industryTypes = res.industryTypes;
      }
    }, err => {
      console.log('Industry Type is not loaded.')
    });
  }

  loadCountries() {
    this.accountService.getCountries().subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.countries = res.countries;
      }
    }, err => {
      console.log('Country is not loaded.')
    });
  }

  loadStates(countryId: number) {
    this.accountService.getStates(countryId).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.states = res.states;
      } else {
        this.states = [];
      }
    }, err => {
      this.states = [];
      console.log('States is not loaded.')
    });
  }

  loadBillingStates(countryId: number) {
    const selectedCountry = filter(this.countries, c => c.id === countryId);
    if (selectedCountry.length > 0) {
      this.companyDetails.get('primaryContactCCCodeId')?.setValue(selectedCountry[0].phonecode);
    }
    this.accountService.getStates(countryId).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.billingStates = res.states; // need to change as states
      } else {
        this.billingStates = [];
      }
    }, err => {
      this.billingStates = [];
      console.log('States is not loaded.')
    });
  }


  loadBillingCities(stateId: number) {
    this.accountService.getCities(stateId).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.billingCities = res.cities;
      } else {
        this.billingCities = [];
      }
    }, err => {
      this.billingCities = [];
      console.log('States is not loaded.')
    });
  }

  submit() {
    let companyObj = { ...this.companyDetails.value };
    companyObj.createdBy = 6; // this.createdBy
    companyObj.departments = [];
    companyObj.deptArray.forEach((value: any, index: any) => {
      this.deptartmentArray[index].value = value;
      companyObj.departments.push(this.deptartmentArray[index]);
    });
    delete companyObj.deptArray;
    console.log('submitted', this.companyDetails.value, companyObj);
    this.accountService.updateCompanyDetails(this.companyId, companyObj).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Company details added/updated successfully.' });
      } else {
        this.messageService.add({ severity: 'error', summary: 'Success', detail: 'Sorry, Something went wrong.' });
      }
    }, error => {
      this.messageService.add({ sticky: true, severity: 'error', summary: 'Error', detail: 'Sorry, Something went Wrong. Try Again later.' });
    });
  }
}
