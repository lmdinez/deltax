import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { USER_ROLES, CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  public selectedTabUrl: number = 1;
  constructor(
    private router: Router,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    if (this.commonService.getUserRole() !== USER_ROLES.SUPERUSER) {
      this.router.navigate(['/dashboard']);
    }
    console.log(this.router.url);
    this.setSelectedTabUrl(this.router.url);
  }

  goToDashboard() {
    this.router.navigate(['/dashboard']);
  }

  openPage(url: string): void {
    this.setSelectedTabUrl(url);
    this.router.navigate([url]);
  }

  setSelectedTabUrl(url: string): void {
    switch (url) {
      default:
      case '/account/company-profile':
        this.selectedTabUrl = 1;
        break;
      case '/account/company-details':
        this.selectedTabUrl = 2;
        break;
      case '/account/company-locations':
        this.selectedTabUrl = 3;
        break;
      case '/account/company-preference':
        this.selectedTabUrl = 4;
        break;
    }
  }

}
