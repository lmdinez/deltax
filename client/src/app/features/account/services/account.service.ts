import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service/http.service';
import { apiurl } from 'src/app/_factory/api';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpService: HttpService) { }

  getIndustryTypes() {
    return this.httpService.Get(apiurl.UTILITY_API.GET_INDUSTRY_TYPES, null, null);
  }

  getCountries() {
    return this.httpService.Get(apiurl.UTILITY_API.GET_COUNTRIES, null, null);
  }

  getStates(companyId: number) {
    let url = apiurl.UTILITY_API.GET_STATES.replace('{0}', `${companyId}`);
    return this.httpService.Get(url, null, null);
  }

  getCities(stateId: number) {
    let url = apiurl.UTILITY_API.GET_CITIES.replace('{0}', `${stateId}`);
    return this.httpService.Get(url, null, null);
  }

  getTimezones() {
    return this.httpService.Get(apiurl.UTILITY_API.GET_TIMEZONE, null, null);
  }

  getLanguages() {
    return this.httpService.Get(apiurl.UTILITY_API.GET_LANGUAGE, null, null);
  }

  getAddressTypes() {
    return this.httpService.Get(apiurl.COMPANY_API.GET_ADDRESS_TYPE, null, null);
  }

  getLocations(companyId: number) {
    let url = apiurl.COMPANY_API.GET_LOCATIONS.replace('{0}', `${companyId}`);
    return this.httpService.Get(url, null, null);
  }

  updateCompanyDetails(companyId: number, companyObj: any) {
    let url = apiurl.COMPANY_API.UPDATE_COMPANY.replace('{0}', `${companyId}`);
    return this.httpService.Post(url, companyObj, null, null);
  }

  postLocationDetails(companyId: number, locationId: number, locationObj: any) {
    let url = apiurl.COMPANY_API.UPDATE_LOCATION.replace('{0}', `${companyId}`).replace('{1}', `${locationId}`);
    return this.httpService.Post(url, locationObj, null, null);
  }
}
