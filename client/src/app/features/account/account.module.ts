import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDropzoneModule } from 'ngx-dropzone';

import { CustomModule } from 'src/app/_factory/module/custom.module';
import { CommonLayoutModule } from 'src/app/common/common-layout.module';
import { NgPrimeModule } from 'src/app/_factory/prime.module';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';

import { AccountRoutingModule } from './account-routing.module';

import { MainComponent } from './main/main.component';
import { CompanyProfileComponent } from './component/company-profile/company-profile.component';
import { CompanyDetailsComponent } from './component/company-details/company-details.component';
import { CompanyLocationsComponent } from './component/company-locations/company-locations.component';
import { CompanyPreferenceComponent } from './component/company-preference/company-preference.component';

@NgModule({
  declarations: [
    MainComponent,
    CompanyProfileComponent,
    CompanyDetailsComponent,
    CompanyLocationsComponent,
    CompanyPreferenceComponent
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgPrimeModule,
    CustomModule,
    NgxDropzoneModule,
    CommonLayoutModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule
  ]
})
export class AccountModule { }
