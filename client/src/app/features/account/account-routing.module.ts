import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { MainComponent } from './main/main.component';
import { CompanyProfileComponent } from './component/company-profile/company-profile.component';
import { CompanyDetailsComponent } from './component/company-details/company-details.component';
import { CompanyLocationsComponent } from './component/company-locations/company-locations.component';
import { CompanyPreferenceComponent } from './component/company-preference/company-preference.component';
const accountRoutes = [
    {
        path: '',
        component: MainComponent,
        children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'company-profile', component: CompanyProfileComponent, data: { header: true }, canActivate: [AuthGuardService] },
            { path: 'company-details', component: CompanyDetailsComponent, data: { header: true }, canActivate: [AuthGuardService] },
            { path: 'company-locations', component: CompanyLocationsComponent, data: { header: true }, canActivate: [AuthGuardService] },
            { path: 'company-preference', component: CompanyPreferenceComponent, data: { header: true }, canActivate: [AuthGuardService] },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(accountRoutes)],
    exports: [RouterModule]
})
export class AccountRoutingModule { }
