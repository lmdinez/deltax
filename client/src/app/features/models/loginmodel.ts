export interface LoginModel {
  password: string;
  userName: string;
}

export interface UserType {
  id: string;
  name: string;
}

export interface TwoFactorAuthenticationModel {
  username: string;
  twofactorcode: string;
}

export interface MFAModel {
  userName: string,
  otpCode: string
}

export interface SignupModel {
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  phoneNumber: string,
  role_id: 1
}
