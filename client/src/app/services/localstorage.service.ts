export class LocalStorageService {
  static set theme(value: string) {
    localStorage.setItem('theme', value === null ? '' : value);
  }

  static get theme(): string {
    return localStorage.getItem('theme') || 'green-theme';
  }

}
