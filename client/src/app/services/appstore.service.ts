import { Injectable, Injector } from '@angular/core';

@Injectable()
export class AppStore {
  private genericStore = new Map<string, any>();

  constructor(
    private injector: Injector
  ) { }

  public getStoreValue<T = any>(key: string): T {
    return this.genericStore.get(key);
  }

  public setStoreValue(key: string, value: any): void {
    this.genericStore.set(key, value);
  }

  public clearStoreValue(key: string): void {
    this.genericStore.delete(key);
  }

  public clearAllStoreValue(): void {
    this.genericStore.clear();
  }
}
