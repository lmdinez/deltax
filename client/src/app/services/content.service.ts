import { Injectable, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators';
import { isPlainObject } from 'lodash';

import { IKeyValuePair } from '../_factory/types/util.types';
import { HttpService } from 'src/app/services/http.service/http.service';


@Injectable()
export class ContentService {
  private cache: IKeyValuePair<Observable<IKeyValuePair<any> | null>> = {};

  constructor(
    private httpService: HttpService
  ) { }

  public fetchContents<T>(moduleName: string, pageName: string): Observable<any> {
    const cacheKey = `${moduleName}-${pageName}`;
    if (this.cache[cacheKey] === undefined) {
      const contentUrl = this.generateURL(moduleName, pageName);
      this.cache[cacheKey] = this.httpService.Get(contentUrl, '', '')
        .pipe(
          map(response => {
            if (response !== null && isPlainObject(response.body)) {
              return response.body;
            }
            throw new Error('Invalid Content');
          }),
          catchError(() => {
            console.log('Error in fetching content:', cacheKey);
            delete this.cache[cacheKey];
            return of(null);
          })
        ),
        shareReplay(1);
    }

    return this.cache[cacheKey];
  }

  public generateURL(moduleName: string, pageName: string) {
    return `contents/${moduleName}/${pageName}`;
  }
}
