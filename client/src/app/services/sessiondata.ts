export class SessionData {
  static set AuthToken(value) {
    sessionStorage.setItem('AuthToken', value === null ? '' : value);
  }

  static get AuthToken() {
    return sessionStorage.getItem('AuthToken');
  }

  static set userInfo(value: any) {
    sessionStorage.setItem('esgUserData', value === null ? '' : value);
  }

  static get userInfo() {
    let data = sessionStorage.getItem('esgUserData')
    return data && JSON.parse(data) || null
  }

  static set userCompany(value: any) {
    sessionStorage.setItem('esgUserCompany', value === null ? '' : value);
  }

  static get userCompany() {
    let data = sessionStorage.getItem('esgUserCompany')
    return data && JSON.parse(data) || null
  }

}
