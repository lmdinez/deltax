import { Injectable, NgZone } from '@angular/core';
import { DialogService } from 'primeng/dynamicdialog';
import { HttpService } from 'src/app/services/http.service/http.service';
import { Subject } from 'rxjs';
import { apiurl } from '../_factory/api';
import { Store } from '../_store/store';
import { environment } from 'src/environments/environment';
import { values } from 'mobx';
declare global {
  interface Window { dataLayer: any[]; }
}
@Injectable({
  providedIn: 'root'
})
export class CustomService {
  constructor(public dialogService: DialogService, public store: Store, private httpService: HttpService) { }
  activeLoader = new Subject<boolean>();
  show() {
    this.activeLoader.next(true);
  }
  hide() {
    this.activeLoader.next(false);
    localStorage.removeItem('isSarch');
  }
  isMobile() {
    if (sessionStorage.desktop)
      return false;
    else if (localStorage.mobile)
      return true;
    var mobile = ['iphone', 'ipad', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) return true;
    return false;
  };
  getFormattedDate = (date: any) => {
    var week = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    var day = week[date.getDay()];
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var returnDate = day + ' - ' + dd + '/' + mm + '/' + yyyy + ' ' + hours + ':' + minutes + ':' + ampm;
    return returnDate
  };
  openDialogComponent = (dialogConfig: any, dialogHeader: string, width: any, component: any) => {
    return this.dialogService.open(component, {
      data: {
        ...dialogConfig
      },
      header: dialogHeader,
      width: width
    });
  }
  dialogComponentConfig = (data: any, component: any) => {
    let dialogConfig = {
      res: data,
    };
    let dialogHeader = data.header;
    let width = data.width;
    component = component
    let ref = this.openDialogComponent(dialogConfig, dialogHeader, width, component);
    return ref.onClose;
  }
  // getCompanyTheme = (id: string) => {
  //   let url = apiurl.COMPANY_API.THEME + id;
  //   return this.httpService.Get(url, null, null);
  // }
  // getCompanySubscription = (id: string) => {
  //   let url = apiurl.COMPANY_API.COMPANY_SUBSCRIPTION + '/' + id;
  //   return this.httpService.Get(url, null, null);
  // }
  // setThemeColour = (...value: any) => {
  //   document.documentElement.style.setProperty('--themeColor', value[0]);
  //   document.documentElement.style.setProperty('--btnSuccessColor', value[1]);
  // }
  // changeTheme(value: string) {
  //   const theme = value.toLocaleLowerCase();
  //   if (theme == 'default') {
  //     this.setThemeColour('#455b2d', '#69793b');
  //   } else if (theme == 'darkslateblue' || theme == 'blue') {
  //     this.setThemeColour('#483D8B', '#483D8B');
  //   } else if (theme == 'teal') {
  //     this.setThemeColour('#008080', '#008080');
  //   } else if (theme == 'coral') {
  //     this.setThemeColour('#FF7F50', '#FF7F50');
  //   } else if (theme == 'black') {
  //     this.setThemeColour('#000', '#000');
  //   } else if (theme == 'darksgrey') {
  //     this.setThemeColour('#2f4f4f', '#2f4f4f');
  //   } else if (theme == 'white') {
  //     this.setThemeColour('#FFFFFF', '#FFFFFF');
  //   }
  // }
  setReturnUrl = (url: any) => {
    // const defaulUrl = url && environment.hostUrl+url || window.location.href;
    const defaulUrl = url || window.location.href;
    localStorage.setItem('esgForwardUrl', defaulUrl)
  }
  getRturnUrl = (url: any) => {
    let esgForwardUrl;
    // const defaulUrl = url && environment.hostUrl || environment.hostUrl+'/dashboard';
    const defaulUrl = url || '/dashboard';
    esgForwardUrl = localStorage.getItem('esgForwardUrl') || defaulUrl;
    localStorage.removeItem('esgForwardUrl');
    return esgForwardUrl
  }
  getFormatedError = (err: any) => {
    var result = err?.error?.result;
    if (result && typeof result === 'string') {
      return result;
    }
    else if (result) {
      const vals = Object.keys(result).map(key => result[key]);
      return vals.toString();
    }
    else {
      return "Error found from server. ";
    }
  }
}
