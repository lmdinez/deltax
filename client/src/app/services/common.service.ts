import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

import { SessionData } from './sessiondata';
import { LocalStorageService } from './localstorage.service';
import * as _ from 'lodash';

export const USER_ROLES = {
  SUPERUSER: 1,
  CONTRIBUTOR: 2
};

@Injectable()
export class CommonService {
  constructor(
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) { }

  getUserRoleForDisplay(): string {
    let displayRole = '';
    switch (SessionData.userInfo.userRole) {
      case USER_ROLES.CONTRIBUTOR:
        displayRole = 'Contributor';
        break;

      case USER_ROLES.SUPERUSER:
        displayRole = 'Super User';
        break;

      default:
        displayRole = '';
        break;
    }
    return displayRole;
  }

  getUserRole(): number {
    return SessionData.userInfo.userRole || 0;
  }

  setTheme(newTheme: string = ''): void {
    if (newTheme !== '') {
      LocalStorageService.theme = newTheme;
    }
    this.document.body.className = LocalStorageService.theme;
  }

  setThemeForView(oldTheme: string, newTheme: string): void {
    this.document.body.classList.replace(oldTheme, newTheme);
  }

  getCompanySelectedTheme(): string {
    return LocalStorageService.theme;
  }

}
