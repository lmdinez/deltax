// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const BASEURL = '/api';

export const environment = {
  PAGE_LIMIT: 10,
  BASE_URL: BASEURL,
  production: false,
  docurl: `${BASEURL}/Document/Download/`,
  hostUrl: `http://localhost:4200`,
  Google_Capcha_Key: `6LdBOTgeAAAAAFvUqoN5XRPfFa2nxcFcATBNfN_V`
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
