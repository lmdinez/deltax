export const BASEURL = 'http://esg.skdedu.in/api';

export const environment = {
  PAGE_LIMIT: 10,
  BASE_URL: BASEURL,
  production: false,
  docurl: `${BASEURL}/Document/Download/`,
  hostUrl: `http://localhost:4200`,
  Google_Capcha_Key: `6LdBOTgeAAAAAFvUqoN5XRPfFa2nxcFcATBNfN_V`
};
