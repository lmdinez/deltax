const apiurl = {
  UserManagementApi: {
    UserManagement: `/UserManagement`,
    GET_COMPLETE_USER: '/usermanagement/completeinfo',
    GET_USER_COMPANIES: '/userManagement/companies',
    USER_COMPLETE_INFO: '/UserManagement/Companies',
    SIGNUP: `/UserManagement/Signup`,
    UserManagement_Search: `/UserManagement/Search`
  },
  StatusApi: {
    status: '/status'
  },
  CountryApi: {
    countryCode: '/countryCode',
    GET_COUNTRIES: '/country',
  },
  STATE_API: {
    Get_STATES_BY_COUNTRY_ID: '/state/getbycountry',
  },
  INDUSTRYTYPE_API: {
    GET_INDUSTRY_TYPES: '/industrytype',
  },
  ACCOUNT_API: {
    LOGIN: '/account/login',
    LOGIN_STEP_2: '/account/loginstep2',
    GET_LOGIN_TYPES: '/account/logintypes',
    GET_ROLES_BY_LOGIN_TYPE: '/account/roles',
    FORGOT_PASSWORD: `account/forgotPassword`,
    SET_PASSWORD: `account/setPassword`,
  },
  COMPANY_API: {
    UPDATE_COMPANY: '/company/companyDetails',
    SAVE_COMPANY_ADDRESSES: '/company/CompanyOtherAddress',
    COMPANY_OTHER_ADDRESS_LIST: '/Company/OtherAddresses',
    COMPANY_OTHER_ADDRESS: '/Company/companyOtherAddress',
    COMPANY_OTHER_ADDRESS_UPDATE: '/Company/CompanyOtherAddress/{companyId}',
    CREATE_COMPANY: '/Company/CompanyDetails',
    COMPANY_DETAILS: '/Company/CompanyDetails',
    GET: '/company',
    ACCOUNT_PREFERENCE: `/Company/AccountPreference`,
    THEME: `/Company/Theme/`,
    COMPANY_SUBSCRIPTION: '/CompanySubscription',
    COMPANY_PROFILE: `/Company/companyProfile`,
    LOCATION_TYPES: `/Company/LocationTypes/{companyId}`,
    COMPANY_BILLING_ADDRESS: `/Company/companyBillingAddress`
  },
  DOCUMENT_API: {
    UPLOAD_DOC: '/document/upload'
  },
  GET_USERS: '/addcard',
  Timezone: '/Timezone',
  Language: '/Language',
  THEME: '/Theme',
  LICENSE: `/License/Valid`
};

function printApiList(source, parentNode = []) {
  if (typeof source === 'string') {
    console.log(`${parentNode.join('.')}:${source}`);
  } else if (typeof source === 'object') {
    Object.keys(source).forEach(s => {
      printApiList(source[s], [...parentNode, s]);
    });
  }
}

printApiList(apiurl);