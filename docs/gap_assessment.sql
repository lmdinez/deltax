-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema deltax
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema deltax
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `deltax` ;
USE `deltax` ;

-- -----------------------------------------------------
-- Table `deltax`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`user` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(100) NOT NULL,
  `enc_password` VARCHAR(100) NOT NULL,
  `company_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deltax`.`gap_assessment_framework`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`gap_assessment_framework` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `framework_name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `created_by` BIGINT UNSIGNED NOT NULL,
  `is_master` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `created_on` DATETIME NULL,
  `updated_on` TIMESTAMP NULL,
  `due_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_gap_assessment_user`
    FOREIGN KEY (`created_by`)
    REFERENCES `deltax`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_gap_assesment_user_idx` ON `deltax`.`gap_assessment_framework` (`created_by` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `deltax`.`gap_assessment_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`gap_assessment_question` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(100) NOT NULL,
  `sub_category` VARCHAR(100) NOT NULL,
  `type_of_question` VARCHAR(100) NOT NULL,
  `question` TEXT NOT NULL,
  `desired_response` TINYINT(1) NOT NULL,
  `comment` TEXT NULL,
  `created_on` DATETIME NULL,
  `updated_on` TIMESTAMP NULL,
  `created_by` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_gap_assessment_question_user`
    FOREIGN KEY (`created_by`)
    REFERENCES `deltax`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_gap_assesment_question_user_idx` ON `deltax`.`gap_assessment_question` (`created_by` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `deltax`.`gap_assessment_share`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`gap_assessment_share` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `assessment_id` BIGINT UNSIGNED NOT NULL,
  `shared_by` BIGINT UNSIGNED NOT NULL,
  `shared_with` BIGINT UNSIGNED NOT NULL,
  `share_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_gap_assessment_share_shared_by`
    FOREIGN KEY (`shared_by`)
    REFERENCES `deltax`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_gap_assessment_share_shared_with`
    FOREIGN KEY (`shared_with`)
    REFERENCES `deltax`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_gap_assessment_share_assesment_id`
    FOREIGN KEY (`assessment_id`)
    REFERENCES `deltax`.`gap_assessment_framework` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_gap_assesment_share_shared_by_idx` ON `deltax`.`gap_assessment_share` (`shared_by` ASC) VISIBLE;

CREATE INDEX `fk_gap_assesment_share_shared_with_idx` ON `deltax`.`gap_assessment_share` (`shared_with` ASC) VISIBLE;

CREATE INDEX `fk_gap_assesment_share_assesment_id_idx` ON `deltax`.`gap_assessment_share` (`assessment_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `deltax`.`gap_assessment_response`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`gap_assessment_response` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `question_id` BIGINT UNSIGNED NOT NULL,
  `response` TINYINT NOT NULL,
  `point` INT NOT NULL,
  `response_by` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_gap_assessment_response_question_id`
    FOREIGN KEY (`question_id`)
    REFERENCES `deltax`.`gap_assessment_question` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_gap_assessment_response_response_by`
    FOREIGN KEY (`response_by`)
    REFERENCES `deltax`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_gap_assesment_response_question_id_idx` ON `deltax`.`gap_assessment_response` (`question_id` ASC) VISIBLE;

CREATE INDEX `fk_gap_assesment_response_response_by_idx` ON `deltax`.`gap_assessment_response` (`response_by` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `deltax`.`gap_assessment_to_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`gap_assessment_to_question` (
  `assessment_id` BIGINT UNSIGNED NOT NULL,
  `question_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`assessment_id`, `question_id`),
  CONSTRAINT `fk_gap_assessment_to_question_assessment_id`
    FOREIGN KEY (`assessment_id`)
    REFERENCES `deltax`.`gap_assessment_framework` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_gap_assessment_to_question_question_id`
    FOREIGN KEY (`question_id`)
    REFERENCES `deltax`.`gap_assessment_question` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_gap_assessment_to_question_question_id_idx` ON `deltax`.`gap_assessment_to_question` (`question_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `deltax`.`gap_assessment_framework_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`gap_assessment_framework_category` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` VARCHAR(100) NOT NULL,
  `parent_id` BIGINT UNSIGNED NULL,
  `created_by` BIGINT UNSIGNED NOT NULL,
  `created_on` DATETIME NOT NULL,
  `updated_on` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_gap_assessment_framework_category_parent_id`
    FOREIGN KEY (`parent_id`)
    REFERENCES `deltax`.`gap_assessment_framework_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_gap_assessment_framework_category_parent_id_idx` ON `deltax`.`gap_assessment_framework_category` (`parent_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `deltax`.`gap_assessment_framework_category_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deltax`.`gap_assessment_framework_category_map` (
  `category_id` BIGINT UNSIGNED NOT NULL,
  `framework_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`category_id`, `framework_id`),
  CONSTRAINT `fk_gap_assessment_framework_category_map_category_id`
    FOREIGN KEY (`category_id`)
    REFERENCES `deltax`.`gap_assessment_framework_category` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_gap_assessment_framework_category_map_framework_id`
    FOREIGN KEY (`framework_id`)
    REFERENCES `deltax`.`gap_assessment_framework` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_gap_assessment_framework_category_map_framework_id_idx` ON `deltax`.`gap_assessment_framework_category_map` (`framework_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
