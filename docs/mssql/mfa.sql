CREATE TABLE `MfaDetails` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `UserName` text NOT NULL,
  `GeneartionTime` datetime DEFAULT NULL,
  `Token` varchar(10) DEFAULT NULL,
  `Secret` longtext,
  `VerifyTime` datetime DEFAULT NULL,
  `VerifyStatus` int DEFAULT NULL,
  `Attempt` int DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ;
