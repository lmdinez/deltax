/****** Object:  Schema [HangFire]    Script Date: 17-02-2022 19:33:05 ******/
CREATE DATABASE [deltax]
GO

USE [deltax]
GO

CREATE SCHEMA [HangFire]
GO

/****** Object:  Table [dbo].[DocumentMapping]    Script Date: 17-02-2022 19:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FileId] [nvarchar](100) NOT NULL,
	[GroupId] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[FileName] [nvarchar](4000) NOT NULL,
	[FilePath] [nvarchar](4000) NOT NULL,
	[Length] [bigint] NOT NULL,
	[Ext] [nvarchar](900) NULL,
	[SubPath] [nvarchar](500) NULL,
	[Provider] [nvarchar](100) NULL,
	[EntityId] [bigint] NULL,
	[CreateDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 17-02-2022 19:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 17-02-2022 19:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[ExpireAt] [datetime] NULL
) ON [PRIMARY]
GO