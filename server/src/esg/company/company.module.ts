import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Language } from "../common/entity/language.entity";
import { Theme } from "../common/entity/theme.entity";
import { Timezone } from "../common/entity/timezone.entity";
import { CompanyController } from "./company.controller";
import { CompanyService } from "./company.service";
import { CompanyAddress } from "./entity/company.address.entity";
import { CompanyDetails } from "./entity/company.details.entity";
import { UserCompany } from "./entity/user.company.entity";
//import { CountryCode } from "../common/entity/countryCode.entity";
import { State } from "../common/entity/state.entity";
import { Country } from "../common/entity/country.entity";
import { IndustryType } from "../common/entity/industryType.entity";
import { CompanyLocation } from "./entity/company.locations.entity";
import { Company } from "./entity/company.entity";
import { CompanyAddresss } from "./entity/company_address.entity";
import { CompanyAddressType } from "./entity/comapnyAddressType.entity";
import { UserRegister } from "../usermgmt/entity/registerUser.entity";
import { CompanyDepartment } from "./entity/company.department.entity";
import { User } from "../common/entity/user.entity";

@Module({
  imports: [TypeOrmModule.forFeature([CompanyDetails, Language, Theme, Timezone, CompanyAddress, UserCompany, State, Country, IndustryType, CompanyLocation, Company, CompanyAddresss, CompanyAddressType, UserRegister, CompanyDepartment, User])],
  providers: [CompanyService],
  controllers: [CompanyController],
})

export class CompanyModule { }
