import { Controller, Get, Post, Body, Param, Res, Headers, Put, UseGuards, Request, HttpStatus } from '@nestjs/common';
import { CompanyService } from './company.service';
import { AccountPreference } from '../model/account.preference.model';
import { Utility } from '../utils/util'
import { CompanyProfileViewModel, InsertCompanyBillingAddress, InsertCompanyDetailsViewModel, InsertCompanyOtherAddress, UpdateCompanyDetailsViewModel } from '../model/company.model';
import { UpdateThemeViewModel } from '../model/theme.view.model';
import { AuthGuard } from '@nestjs/passport';

@Controller('company')
export class CompanyController {
  constructor(private companyService: CompanyService) {
  }

  @Post('/AccountPreference/:companyId')
  public updateAccountPreference(@Res() response, @Param('companyId') companyId: number, @Body() accountPreference: AccountPreference) {
    let userId: number = Utility.getUserId();
    if (companyId != accountPreference.CompanyId) {
      return Utility.handleFailure(response, "id is not matched");
    }
    this.companyService.modifyAccountPreference(accountPreference, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "Company does not exist");
      }
      if (result == -2) {
        return Utility.handleFailure(response, "theme does not exist");
      }
      if (result == -3) {
        return Utility.handleFailure(response, "language does not exist");
      }
      if (result == -4) {
        return Utility.handleFailure(response, "timezone does not exist");
      }
      return Utility.handleSuccess(response, "Update Successfully");
    });
  }

  @Post('/companyBillingAddress/:companyId')
  public updateCompanyBillingAddress(@Res() response, @Param('companyId') companyId: number, @Body() insertCompanyBillingAddress: InsertCompanyBillingAddress) {
    let userId: number = Utility.getUserId();
    if (companyId != insertCompanyBillingAddress.CompanyId) {
      return Utility.handleFailure(response, "Company does not exist");
    }
    this.companyService.updateCompanyBillingAddress(insertCompanyBillingAddress, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "Company does not exist");
      }
      if (result == -1) {
        return Utility.handleFailure(response, "billing address for given company does not exist");
      }
      return Utility.handleSuccess(response, "Update Successfully");
    });
  }

  @Get('/id/:companyId')
  public getCompanyById(@Res() response, @Param('companyId') companyId: number) {
    this.companyService.getCompanyById(companyId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "No Data is present in database1");
      } else {
        return result != null ? Utility.handleSuccessWithResult(response, result) : Utility.handleFailureWithResult(response, result);
      }
    });
  }

  @Post('/CompanyDetails')
  public postCompanyDetails(@Res() response, @Body() insertCompanyDetailsViewModel: InsertCompanyDetailsViewModel) {
    let userId: number = Utility.getUserId();
    this.companyService.insertCompanyDetails(insertCompanyDetailsViewModel, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "Company already exist");
      } else {
        return result != 0 ? Utility.handleSuccessWithResult(response, result) : Utility.handleFailureWithResult(response, result);
      }
    });

  }

  @Post('/CompanyDetails/:companyId')
  public UpdateCompanyDetails(@Res() response, @Param('companyId') companyId: number, @Body() updateCompanyDetailsViewModel: UpdateCompanyDetailsViewModel) {

    var companyModel: UpdateCompanyDetailsViewModel = updateCompanyDetailsViewModel;
    companyModel.Id = companyId;
    console.log(`Company Details ${companyId} : ${updateCompanyDetailsViewModel}`);
    let userId: number = Utility.getUserId();
    this.companyService.updateCompanyDetails(companyModel, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "Company Id does not exist");
      } else {
        return result != 0 ? Utility.handleSuccessWithResult(response, result) : Utility.handleFailureWithResult(response, result);
      }
    });
  }

  @Post('/companyOtherAddress')
  public postCompanyOtherAddress(@Res() response, @Body() insertCompanyOtherAddress: InsertCompanyOtherAddress) {
    let userId: number = Utility.getUserId();
    console.log(`Other Company address ${insertCompanyOtherAddress}`);
    this.companyService.insertCompanyOtherAddress(insertCompanyOtherAddress, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "Company Id does not exist");
      } else {
        return result != 0 ? Utility.handleSuccessWithResult(response, result) : Utility.handleFailureWithResult(response, result);
      }
    });

  }

  @Get('/OtherAddresses')
  public getCompanyOtherAddresses(@Res() response, @Headers() headers) {
    console.log(`Header : ${headers}`);
    let page: number = headers.page;
    let pageLimit: number = headers.pagelimit; // Header always take lowercase key 
    let companyId: number = Utility.getUserCompanyId();
    console.log(`GetCompanyOtherAddress => page: ${page} -- PageLimit: ${pageLimit}`);
    this.companyService.getCompanyOtherAddressesByCompanyId(companyId, page, pageLimit).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "No Other address mapped for this company");
      } else {
        return result.length != 0 ? Utility.handleSuccessWithResult(response, result) : Utility.handleFailureWithResult(response, result);
      }
    });

  }


  @Put('/companyOtherAddress/:companyId')
  public updateCompanyOtherAddress(@Res() response, @Param('companyId') companyId: number, @Body() updateCompanyOtherAddress: InsertCompanyOtherAddress) {

    if (companyId != updateCompanyOtherAddress.CompanyId) {
      return Utility.handleFailure(response, "id is not matched");
    }

    let userId: number = Utility.getUserId();
    this.companyService.updateCompanyOtherAddress(companyId, updateCompanyOtherAddress, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "No Other address mapped for this company");
      } else {
        return result != 0 ? Utility.handleSuccessWithResult(response, result) : Utility.handleFailureWithResult(response, result);
      }
    });

  }


  @Post('/companyProfile')
  @UseGuards(AuthGuard('jwt'))
  public postCompanyProfile(@Request() request, @Res() response, @Body() companyProfileViewModel: CompanyProfileViewModel) {
    let userId: number = Utility.getUserById(request);
    this.companyService.insertCompanyProfile(companyProfileViewModel, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "Company id does not exist");
      } else {
        return result != 0 ? Utility.handleSuccessWithResult(response, result) : Utility.handleFailureWithResult(response, result);
      }
    });
  }

  @Get('/LocationTypes/:companyId?')
  @UseGuards(AuthGuard('jwt'))
  public getLocationTypeByCompanyId(@Request() request, @Res() response, @Param('companyId') companyId?: number) {
    //Convert.ToInt32(companyId)  -- Pending
    console.log("request.user.UsrId", Utility.getUserById(request));
    let compId = companyId == null ? Utility.getUserCompanyId() : companyId;
    this.companyService.getLocationTypeByCompanyId(compId).then(result => {
      return Utility.handleSuccessList(response, result);
    });
  }

  @Get('/Theme/:companyId')
  public getCompanyTheme(@Res() response, @Param('companyId') companyId: number) {
    this.companyService.getCompaniesTheme(companyId).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    });
  }

  @Put('/Theme/:companyId')
  public updateCompanyTheme(@Res() response, @Param('companyId') companyId: number, @Body() updateThemeViewModel: UpdateThemeViewModel) {
    if (companyId != updateThemeViewModel.CompanyId) {
      return Utility.handleFailure(response, "id is not matched");
    }
    let userId: number = Utility.getUserId();
    this.companyService.updateTheme(updateThemeViewModel, userId).then(result => {
      if (result == -1) {
        return Utility.handleFailure(response, "Company does not exist");
      }
      if (result == -2) {
        return Utility.handleFailure(response, "theme does not exist");
      }
      return Utility.handleSuccess(response, "Update Successfully");
    });
  }

  @Get('/:company_id/locations')
  public getlocations(@Res() response, @Param('company_id') companyId: number) {
    this.companyService.getlocations(companyId).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { "status": "SUCCESS", "siteId": "", "locations": result };

      return response.status(200).json(final_result);
    });
  }

  /**
   * get by company id
   * @param companyid 
   * @returns 
   */
  @Get('/:companyid')
  async getCompanyId(@Param('companyid') companyid: any): Promise<any> {
    return {
      status: "SUCCESS",
      statusCode: HttpStatus.OK,
      data: await this.companyService.getCompanyId(companyid)
    }
  }


  /**
   * 
   * @param data 
   * @returns 
   */
  @Post('/location')
  async createCompanyLocation(@Body() data: any): Promise<any> {
    return {
      status: "SUCCESS",
      statusCode: HttpStatus.CREATED,
      message: 'creacted successfully',
      data: await this.companyService.createCompanyLocation(data)
    }
  }

  @Get('/address/type')
  async getaddresstype(@Param('companyid') companyid: any): Promise<any> {
    return {
      status: "SUCCESS",
      statusCode: HttpStatus.OK,
      data: await this.companyService.getaddresstype()
    }
  }

  @Get('/:companyid/users')
  async getCompanyIduser(@Param('companyid') companyid: any): Promise<any> {
    return {
      status: "SUCCESS",
      statusCode: HttpStatus.OK,
      users: await this.companyService.getCompanyIduser(companyid)
    }
  }
  @Get('/:companyid/users/:user_id')
  async getCompanyIduserid(@Param('companyid') companyid: any, @Param('user_id') userid: any): Promise<any> {
    return {
      status: "SUCCESS",
      statusCode: HttpStatus.OK,
      users: await this.companyService.getCompanyIduserid(companyid, userid)
    }
  }
  @Get('/:companyid/department')
  async getCompanyIddepartment(@Param('companyid') companyid: any): Promise<any> {
    return {
      status: "SUCCESS",
      statusCode: HttpStatus.OK,
      users: await this.companyService.getCompanyIddepartment(companyid)
    }
  }

  /**
   * 
   * @param data 
   * @returns 
   */
  @Post('/:company_id')
  async createCompanyDetails(@Param('company_id') company_Id: number, @Body() data: any): Promise<any> {
    return {
      status: "SUCCESS",
      message: "Company Details Added",
      companyId: await this.companyService.createCompanyDetails(company_Id, data)
    }
  }


  @Post('/:company_id/locations/:location_id')
  async createLocation(@Param('company_id') company_Id: number, @Param('location_id') location_id: number, @Body() data: any): Promise<any> {
    return {
      status: "SUCCESS",
      message: "location Details Added",
      companyId: await this.companyService.createCompanyLocationDetails(company_Id, location_id, data)
    }
  }


  @Post('/:company_id/users/:user_id')
  async createCompanyUser(@Param('company_id') company_Id: number, @Param('user_id') user_id: number, @Body() data: any): Promise<any> {
    return {
      status: "SUCCESS",
      message: "location Details Added",
      userId: await this.companyService.createCompanyUser(company_Id, user_id, data)
    }
  }
}


