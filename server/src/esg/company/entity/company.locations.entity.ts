import { Country } from "../../common/entity/country.entity";
import { State } from "../../common/entity/state.entity";
import { BaseEntity, Column, Entity, Generated, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Timezone } from "src/esg/common/entity/timezone.entity";
import { City } from "src/esg/common/entity/city.entity";
import { Company } from "./company.entity";
import { CompanyAddressType } from "./comapnyAddressType.entity";
import { User } from "src/esg/common/entity/user.entity";

@Entity('company_location')
export class CompanyLocation extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 255,
    nullable: false
  })
  company_name: string

  @Column({
    type: 'varchar',
    length: 100,
    nullable: false
  })
  location_name: string

  @Column({
    type: 'varchar',
    length: 100,
    nullable: true
  })
  location_description: string


  @OneToOne(() => CompanyAddressType, address_type => address_type.companyLocation)
  @JoinColumn({ name: 'address_type_id' })
  address_type_id: CompanyAddressType;


  @OneToOne(() => Timezone, timezone => timezone.companyLocation)
  @JoinColumn({ name: 'timezone_id' })
  timezone_id: Timezone;


  @Column({
    type: 'varchar',
    length: 255,
    nullable: false
  })
  address_line_1: string


  @Column({
    type: 'varchar',
    length: 100,
    nullable: true
  })
  address_line_2: string


  @OneToOne(() => Country, country => country.companyLocation)
  @JoinColumn({ name: 'country_id' })
  country_id: Country;


  @OneToOne(() => State, state => state.companyLocation)
  @JoinColumn({ name: 'state_id' })
  state_id: State;


  @OneToOne(() => City, city => city.companyLocation)
  @JoinColumn({ name: 'city_id' })
  city_id: City;


  @Column({
    type: 'varchar',
    length: 50,
    nullable: true
  })
  zipcode: string

  @Column({
    type: 'tinyint',
    nullable: false
  })
  status: number


  @OneToOne(() => Company, company => company.companyLocation)
  @JoinColumn({ name: 'company_id' })
  company_id: Company;

  // @OneToOne(() => City, city => city.city_id)
  // @JoinColumn({ name: 'city_idcompany_id' })
  @Column({
    type: 'bigint',
    nullable: false
  })
  created_by: String;


  @Column({
    type: 'datetime',
    nullable: false
  })
  created_on: string


  @Column({
    type: 'timestamp',
    nullable: false
  })
  updated_on: string

  @OneToOne(() => Company, company => company.companyLocation)
  company: Company;


  @OneToOne(() => User, user => user.location_id)
  user: User;

}
