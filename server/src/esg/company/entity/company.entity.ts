import { Country } from "src/esg/common/entity/country.entity";
import { IndustryType } from "src/esg/common/entity/industry.type.entity";
import { State } from "src/esg/common/entity/state.entity";
import { User } from "src/esg/common/entity/user.entity";
import { BaseEntity, Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { CompanyDepartment } from "./company.department.entity";
import { CompanyLocation } from "./company.locations.entity";
import { CompanyAddresss } from "./company_address.entity";

@Entity('company')
export class Company extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 255,
    nullable: false
  })
  name: string

  @OneToOne(() => IndustryType, industryType => industryType.company)
  @JoinColumn({ name: 'industry_type_id' })
  industry_type_id: IndustryType;

  @OneToOne(() => Country, country => country.company)
  @JoinColumn({ name: 'country_id' })
  country_id: Country;

  @OneToOne(() => State, state => state.company)
  @JoinColumn({ name: 'state_id' })
  state_id: State;


  @Column({
    type: 'bigint',
    nullable: true
  })
  logo_image_upload_id: number


  @Column({
    type: 'bigint',
    nullable: true
  })
  bg_image_upload_id: number


  @Column({
    type: 'bigint',
    nullable: true
  })
  created_by: number


  // @Column({
  //   type: 'datetime',
  //   nullable: false
  // })
  @CreateDateColumn()
  created_on: string


  // @Column({
  //   type: 'timestamp',
  //   nullable: false
  // })
  @UpdateDateColumn()
  updated_on: string


  @Column({
    type: 'int',
    nullable: true
  })
  theme_id: number


  @Column({
    type: 'int',
    nullable: true
  })
  language_id: number


  @Column({
    type: 'int',
    nullable: true
  })
  timezone_id: number


  @OneToOne(() => CompanyLocation, companyLocation => companyLocation.company)
  companyLocation: CompanyLocation;

  @OneToOne(() => CompanyAddresss, companyAddress => companyAddress.company_id)
  company_address: CompanyAddresss;

  @OneToOne(() => CompanyDepartment, companyDepartment => companyDepartment.company_id)
  company_department: CompanyDepartment;

  @OneToOne(() => User, user => user.company_id)
  user: User;

}
