import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToOne } from "typeorm";
import { CompanyLocation } from "./company.locations.entity";

@Entity('company_address_type')
export class CompanyAddressType extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 100,
    nullable: false
  })
  type: string

  @Column({
    type: 'tinyint',
    nullable: false
  })
  is_active: number

  @OneToOne(() => CompanyLocation, companylocation => companylocation.address_type_id) // specify inverse side as a second parameter
  companyLocation: CompanyLocation;

}
