import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

import { Country } from "../../common/entity/country.entity"
import { State } from "../../common/entity/state.entity"
import { CountryCode } from "src/esg/common/entity/countryCode.entity";
import { Timezone } from "src/esg/common/entity/timezone.entity";
import { CompanyLocation } from "./company.locations.entity";

@Entity({ name: 'CompanyAddress' })
export class CompanyAddress {

  @PrimaryGeneratedColumn()
  Id: number;
  @Column({ name: 'CompanyId' })
  CompanyId: number;
  @Column({ name: 'StateId' })
  StateId: number; c
  @Column({ name: 'CountryId' })
  CountryId: number;
  @Column({ name: 'LocationType' })
  LocationType: string;
  @Column({ name: 'TimezoneId' })
  TimezoneId: number;
  @Column({ name: 'AddressTypeId' })
  AddressTypeId: number;
  @Column({ name: 'AddressLine1' })
  AddressLine1: string;
  @Column({ name: 'AddressLine2' })
  AddressLine2: string;
  @Column({ name: 'City' })
  City: string;
  @Column({ name: 'ZipCode' })
  ZipCode: string;
  @Column({ name: 'PointOfContactName' })
  PointOfContactName: string;
  @Column({ name: 'PointOfContactMobileNumber' })
  PointOfContactMobileNumber: string;
  @Column({ name: 'CountryCodeId' })
  CountryCodeId: number;
  @Column({ name: 'CorrespondanceEmail' })
  CorrespondanceEmail: string;
  @Column({ name: 'AlternativeEmail' })
  AlternativeEmail: string;
  @Column({ name: 'ModifiedDate' })
  ModifiedDate: Date;
  @Column({ name: 'ModifiedBy' })
  ModifiedBy: number;
  @Column({ name: 'CreatedDate' })
  CreatedDate: Date;
  @Column({ name: 'CreatedBy' })
  CreatedBy: number;

  @OneToOne(() => CountryCode)
  @JoinColumn({ name: 'CountryCodeId', referencedColumnName: 'Id' })
  countryCode: CountryCode;

  @OneToOne(() => State)
  @JoinColumn({ name: 'StateId', referencedColumnName: 'id' })
  state: State;

  @OneToOne(() => Country)
  @JoinColumn({ name: 'CountryId', referencedColumnName: 'id' })
  country: Country;

  @OneToOne(() => Timezone)
  @JoinColumn({ name: 'TimezoneId', referencedColumnName: 'id' })
  timeZone: Timezone;

}
