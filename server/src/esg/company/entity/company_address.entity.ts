import { City } from "src/esg/common/entity/city.entity";
import { Country } from "src/esg/common/entity/country.entity";
import { State } from "src/esg/common/entity/state.entity";
import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Company } from "./company.entity";


export enum Type {
  billing = 'BILLING'
}
@Entity('company_address')
export class CompanyAddresss extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 255,
    nullable: true
  })
  type: Type

  @Column({
    type: 'varchar',
    length: 255,
    nullable: false
  })
  address_line_1: string


  @Column({
    type: 'varchar',
    length: 255,
    nullable: true
  })
  address_line_2: string

  @OneToOne(() => Country, country => country.company_address)
  @JoinColumn({ name: 'country_id' })
  country_id: Country;

  @OneToOne(() => State, state => state.company_address)
  @JoinColumn({ name: 'state_id' })
  state_id: State;

  @OneToOne(() => City, city => city.company_address)
  @JoinColumn({ name: 'city_id' })
  city_id: City;


  @Column({
    type: 'varchar',
    length: 50,
    nullable: false
  })
  zipcode: string

  @Column({
    type: 'varchar',
    length: 255,
    nullable: false
  })
  primary_contact_name: string


  @Column({
    type: 'varchar',
    length: 10,
    nullable: true
  })
  primary_contact_mob_country_code: string


  @Column({
    type: 'varchar',
    length: 20,
    nullable: true
  })
  primary_contact_mob_number: string

  @Column({
    type: 'varchar',
    length: 255,
    nullable: false
  })
  correspondence_email: string


  @OneToOne(() => Company, company => company.company_address)
  @JoinColumn({ name: 'company_id' })
  company_id: Company;

}
