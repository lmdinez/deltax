import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToOne, UpdateDateColumn, CreateDateColumn, JoinColumn } from "typeorm";
import { Company } from "./company.entity";

@Entity('company_department')
export class CompanyDepartment extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 100,
    nullable: false
  })
  name: string


  @OneToOne(() => Company, company => company.company_department)
  @JoinColumn({ name: 'company_id' })
  company_id: Company;

  @Column({
    type: 'bigint',
    nullable: true
  })
  created_by: number

  @CreateDateColumn()
  created_on: string

  @UpdateDateColumn()
  updated_on: string
}
