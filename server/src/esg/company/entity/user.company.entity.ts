import { User } from "src/esg/usermgmt/entity/user.entity";
import { Column, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'UserCompany' })
export class UserCompany {
  @PrimaryGeneratedColumn()
  Id: number;
  @Column({ name: 'UserId' })
  UserId: number;
  @Column({ name: 'CompanyId' })
  CompanyId: number;
  @Column({ name: 'ModifiedDate' })
  ModifiedDate: Date;
  @Column({ name: 'ModifiedBy' })
  ModifiedBy: number;
  @Column({ name: 'CreatedDate' })
  CreatedDate: Date;
  @Column({ name: 'CreatedBy' })
  CreatedBy: number;

  @ManyToOne(() => User, user => user.userCompany)
  user: User;
}
