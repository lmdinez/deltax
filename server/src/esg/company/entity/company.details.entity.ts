import { Country } from "../../common/entity/country.entity";
import { IndustryType } from "../../common/entity/industryType.entity";
import { State } from "../../common/entity/state.entity";
import { Column, Entity, Generated, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Theme } from "../../common/entity/theme.entity";

@Entity({ name: 'CompanyDetails' })
export class CompanyDetails {

  @PrimaryGeneratedColumn()
  // @Generated('increment')
  Id: number;
  @Column({ name: 'CompanyName' })
  CompanyName: string;
  @Column({ name: 'IndustryTypeId' })
  IndustryTypeId: number;
  @Column({ name: 'CountryId' })
  CountryId: number;
  @Column({ name: 'StateId' })
  StateId: number;
  @Column({ name: 'ThemeId' })
  ThemeId: number;
  @Column({ name: 'LanguageId' })
  LanguageId: number;
  @Column({ name: 'TimezoneId' })
  TimezoneId: number;
  @Column({ name: 'ImageId' })
  ImageId: string;
  @Column({ name: 'BackgroundImageId' })
  BackgroundImageId: string;
  //  DateTime? ModifiedDate { get; set; }
  @Column({ name: 'ModifiedDate' })
  ModifiedDate: Date;
  @Column({ name: 'ModifiedBy' })
  ModifiedBy: number;
  //  DateTime? CreatedDate { get; set; }
  @Column({ name: 'CreatedDate' })
  CreatedDate: Date;
  //  int? CreatedBy : number;
  @Column({ name: 'CreatedBy' })
  CreatedBy: number;

  @OneToOne(() => State)
  @JoinColumn({ name: 'StateId', referencedColumnName: "id" })
  state: State;

  @OneToOne(() => IndustryType)
  @JoinColumn({ name: 'IndustryTypeId', referencedColumnName: "id" })
  industryType: IndustryType;

  @OneToOne(() => Country)
  @JoinColumn({ name: 'CountryId', referencedColumnName: "id" })
  country: Country;

  @OneToOne(() => Theme)
  @JoinColumn({ name: 'ThemeId', referencedColumnName: "id" })
  theme: Theme;

}
