import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { getConnection, getRepository, Repository } from "typeorm";
import { CompanyDetails } from "./entity/company.details.entity";
import { AccountPreference } from "../model/account.preference.model";
import { Theme } from "../common/entity/theme.entity";
import { Language } from "../common/entity/language.entity";
import { Timezone } from "../common/entity/timezone.entity";
import { CompanyProfileViewModel, InsertCompanyBillingAddress, InsertCompanyDetailsViewModel, InsertCompanyOtherAddress, UpdateCompanyDetailsViewModel } from "../model/company.model";
import { CompanyAddress } from "./entity/company.address.entity";
import { UserCompany } from "./entity/user.company.entity";
import { LocationTypeViewModel } from "../model/location.types.model";
import { ThemeViewModel, UpdateThemeViewModel } from "../model/theme.view.model";
import { CompanyLocation } from "./entity/company.locations.entity";
import { Company } from "./entity/company.entity";
import { CompanyAddresss } from "./entity/company_address.entity";
import { CompanyAddressType } from "./entity/comapnyAddressType.entity";
import { UserRegister } from "../usermgmt/entity/registerUser.entity";
import { CompanyDepartment } from "./entity/company.department.entity";
import { User } from "../common/entity/user.entity";
import { Role } from "../common/entity/role.entity";


@Injectable()
export class CompanyService {



  private className: string = 'CompanyService -->';
  constructor(@InjectRepository(CompanyDetails) private companyDetailsRepo: Repository<CompanyDetails>,
    @InjectRepository(Theme) private themeRepo: Repository<Theme>,
    @InjectRepository(Language) private langRepo: Repository<Language>,
    @InjectRepository(Timezone) private timezoneRepo: Repository<Timezone>,
    @InjectRepository(CompanyAddress) private companyAddressRepo: Repository<CompanyAddress>,
    @InjectRepository(UserCompany) private userCompanyRepo: Repository<UserCompany>,
    @InjectRepository(CompanyLocation) private companylocationsRepo: Repository<CompanyLocation>,
    @InjectRepository(Company) private companyRepo: Repository<Company>,
    @InjectRepository(CompanyAddresss) private CompanyAddressRepository: Repository<CompanyAddresss>,
    @InjectRepository(CompanyAddressType) private companyAddressTypeRepository: Repository<CompanyAddressType>,
    @InjectRepository(UserRegister) private userRepo: Repository<UserRegister>,
    @InjectRepository(CompanyDepartment) private CompanyDepartmentRepo: Repository<CompanyDepartment>,
    @InjectRepository(User) private userRepository: Repository<User>,

  ) {
  }

  public async getCompanyById(companyId: number) {

    let companyDetails = await this.companyDetailsRepo.findOne(companyId);
    if (null == companyDetails) {
      return -1;
    }
    console.log(companyDetails);
    return companyDetails;
  }

  public async modifyAccountPreference(accountPreference: AccountPreference, userId: number): Promise<number> {
    let methodName: string = 'modifyAccountPreference ->';
    console.log(this.className + methodName + 'accountPreference', accountPreference);

    let companyDetails = await this.companyDetailsRepo.findOne(accountPreference.CompanyId);
    // let companyDetails = await this.companyDetailsRepo.findOne({ where: [{ "Id": accountPreference.CompanyId }] }).then(result => {
    //   console.log('accountPreference results..', result);
    //   return result;
    // });

    // let companyDetails = await this.companyDetailsRepo.findOneOrFail(accountPreference.CompanyId).then(result => {
    //   console.log('accountPreference results..', result);
    //   return result;
    // });
    console.log(this.className + methodName + 'companyDetails', companyDetails);
    if (null != companyDetails) {
      let themeDetails = await this.themeRepo.findOne(accountPreference.ThemeId);
      console.log(this.className + methodName + 'themeDetails', themeDetails);
      if (null != themeDetails) {
        let langDetails = await this.langRepo.findOne(accountPreference.LanguageId);
        console.log(this.className + methodName + 'langDetails', langDetails);
        if (langDetails == null) {
          console.log(this.className + methodName + 'language does not exist  !!');
          return -3;
        } else {
          let timezoneDetails = await this.timezoneRepo.findOne(accountPreference.TimezoneId);
          console.log(this.className + methodName + 'timezoneDetails', timezoneDetails);
          if (timezoneDetails == null) {
            console.log(this.className + methodName + 'timezone does not exist  !!');
            return -4;
          }
          companyDetails.ThemeId = accountPreference.ThemeId;
          companyDetails.TimezoneId = accountPreference.TimezoneId;
          companyDetails.LanguageId = accountPreference.LanguageId;
          companyDetails.ModifiedDate = new Date();
          companyDetails.ModifiedBy = userId;
          console.log(this.className + methodName + 'Company details save to DB:', companyDetails);
          let status = await this.companyDetailsRepo.save(companyDetails);
          // let status = this.companyDetailsRepo.update({ Id: companyDetails.Id }, companyDetails);
          console.log(this.className + methodName + 'DB SAVE STATUS', status);
          return companyDetails.Id;
        }
      } else {
        console.log(this.className + methodName + 'theme does not exist  !!');
        return -2;
      }
    } else {
      console.log(this.className + methodName + 'company does not exist  !!');
      return -1;
    }
  }

  public async updateCompanyBillingAddress(insertCompanyBillingAddress: InsertCompanyBillingAddress, userId: number): Promise<number> {
    let methodName: string = 'updateCompanyBillingAddress ->';
    console.log(this.className + methodName + 'insertCompanyBillingAddress', insertCompanyBillingAddress);
    let companyDetails = await this.companyDetailsRepo.findOne(insertCompanyBillingAddress.CompanyId);
    if (null == companyDetails) {
      console.log(this.className + methodName + 'company does not exist  !!');
      return -1;
    }
    console.log(this.className + methodName + 'companyDetails', companyDetails);
    // let companyAddress = await this.companyAddressRepo.findOne({ where: [{ "CompanyId": insertCompanyBillingAddress.CompanyId }, { "AddressTypeId": 1 }] });
    let companyAddress = await this.companyAddressRepo.findOne({ CompanyId: insertCompanyBillingAddress.CompanyId, AddressTypeId: 1 });
    console.log(this.className + methodName + 'companyAddress', companyAddress);
    if (null == companyAddress) {
      console.log(this.className + methodName + 'billing address for given company does not exist');
      return -2;
    }

    companyAddress.AddressLine1 = insertCompanyBillingAddress.AddressLine1;
    companyAddress.AddressLine2 = insertCompanyBillingAddress.AddressLine2;
    companyAddress.City = insertCompanyBillingAddress.City;
    companyAddress.CorrespondanceEmail = insertCompanyBillingAddress.CorrespondanceEmail;
    companyAddress.PointOfContactMobileNumber = insertCompanyBillingAddress.PointOfContactMobileNumber;
    companyAddress.PointOfContactName = insertCompanyBillingAddress.PointOfContactName;
    companyAddress.ZipCode = insertCompanyBillingAddress.ZipCode;
    companyAddress.CountryCodeId = insertCompanyBillingAddress.CountryCodeId;
    companyAddress.CountryId = insertCompanyBillingAddress.CountryId;
    companyAddress.StateId = insertCompanyBillingAddress.StateId;
    companyAddress.LocationType = insertCompanyBillingAddress.LocationType;
    companyAddress.ModifiedDate = new Date();
    companyAddress.ModifiedBy = userId;
    console.log(this.className + methodName + 'Company address save to DB:', companyAddress);
    let status = await this.companyAddressRepo.save(companyAddress);
    console.log(this.className + methodName + 'DB SAVE STATUS', status);
    return insertCompanyBillingAddress.CompanyId;
  }

  public async insertCompanyDetails(insertCompanyDetailsViewModel: InsertCompanyDetailsViewModel, userId: number): Promise<number> {
    let methodName: string = 'insertCompanyDetails ->';
    console.log(this.className + methodName + 'insertCompanyDetailsViewModel', insertCompanyDetailsViewModel);
    let companyDetails = await this.companyDetailsRepo.findOne({ CompanyName: insertCompanyDetailsViewModel.CompanyName });

    console.log(this.className + methodName + 'companyDetails', companyDetails);
    if (null != companyDetails && undefined !== companyDetails) {
      console.log(this.className + methodName + 'company already exist');
      return -1;
    } else {
      let companyDetailsEntity: CompanyDetails = new CompanyDetails();
      companyDetailsEntity.CompanyName = insertCompanyDetailsViewModel.CompanyName;
      companyDetailsEntity.CountryId = insertCompanyDetailsViewModel.CountryId;
      companyDetailsEntity.IndustryTypeId = insertCompanyDetailsViewModel.IndustryTypeId;
      companyDetailsEntity.StateId = insertCompanyDetailsViewModel.StateId;
      companyDetailsEntity.ThemeId = 1;
      companyDetailsEntity.LanguageId = 1;
      companyDetailsEntity.TimezoneId = 1;
      companyDetailsEntity.ImageId = null;
      companyDetailsEntity.BackgroundImageId = null;
      companyDetailsEntity.CreatedBy = userId;
      let localDate: Date = new Date();
      console.log('UTC DATE >>>> ', new Date(Date.UTC(localDate.getFullYear(), localDate.getMonth(), localDate.getDay(), localDate.getHours(), localDate.getMinutes(), localDate.getSeconds())));
      console.log('DATE NOW >>>', new Date());
      companyDetailsEntity.CreatedDate = new Date(Date.UTC(localDate.getFullYear(), localDate.getMonth(), localDate.getDay(), localDate.getHours(), localDate.getMinutes(), localDate.getSeconds()));
      companyDetailsEntity.ModifiedBy = null;
      companyDetailsEntity.ModifiedDate = null;
      // Exsting code insert into DB
      // let companyResult = await this.companyDetailsRepo.save(companyDetails);
      let companyResult = await this.companyDetailsRepo.insert(companyDetailsEntity);

      console.log(this.className + methodName + 'companyResult::', companyResult);
      if (null != companyResult && companyResult.raw.insertId > 0 && userId > 0) {
        let companyBilingAddressEntity: CompanyAddress = new CompanyAddress();
        companyBilingAddressEntity.AddressLine1 = insertCompanyDetailsViewModel.billingAddress.AddressLine1;
        companyBilingAddressEntity.AddressLine2 = insertCompanyDetailsViewModel.billingAddress.AddressLine2;
        companyBilingAddressEntity.AddressTypeId = 1;
        companyBilingAddressEntity.City = insertCompanyDetailsViewModel.billingAddress.City;
        companyBilingAddressEntity.CompanyId = companyResult.raw.insertId;
        companyBilingAddressEntity.CorrespondanceEmail = insertCompanyDetailsViewModel.billingAddress.CorrespondanceEmail;
        companyBilingAddressEntity.PointOfContactMobileNumber = insertCompanyDetailsViewModel.billingAddress.PointOfContactMobileNumber;
        companyBilingAddressEntity.PointOfContactName = insertCompanyDetailsViewModel.billingAddress.PointOfContactName;
        companyBilingAddressEntity.ZipCode = insertCompanyDetailsViewModel.billingAddress.ZipCode;
        companyBilingAddressEntity.CountryCodeId = insertCompanyDetailsViewModel.billingAddress.CountryCodeId;
        companyBilingAddressEntity.LocationType = insertCompanyDetailsViewModel.billingAddress.LocationType;
        companyBilingAddressEntity.CountryId = insertCompanyDetailsViewModel.billingAddress.CountryId;
        companyBilingAddressEntity.StateId = insertCompanyDetailsViewModel.billingAddress.StateId;
        companyBilingAddressEntity.TimezoneId = 0;
        companyBilingAddressEntity.AlternativeEmail = null;
        companyBilingAddressEntity.CreatedBy = userId;
        companyBilingAddressEntity.CreatedDate = new Date();
        companyBilingAddressEntity.ModifiedBy = null;
        companyBilingAddressEntity.ModifiedDate = null;

        let companyBillingAddressInsertResult = await this.companyAddressRepo.insert(companyBilingAddressEntity);
        console.log(this.className + methodName + 'companyBillingAddressInsertResult::', companyBillingAddressInsertResult);

        let userCompanyEntity: UserCompany = new UserCompany();
        userCompanyEntity.CompanyId = companyResult.raw.insertId;
        userCompanyEntity.UserId = userId;
        userCompanyEntity.CreatedBy = userId;
        // let localDate: Date = new Date();
        userCompanyEntity.CreatedDate = new Date(Date.UTC(localDate.getFullYear(), localDate.getMonth(), localDate.getDay(), localDate.getHours(), localDate.getMinutes(), localDate.getSeconds()));
        userCompanyEntity.ModifiedBy = null;
        userCompanyEntity.ModifiedDate = null;
        let userCompanyInsertResult = await this.userCompanyRepo.insert(userCompanyEntity);
        console.log(this.className + methodName + 'userCompanyInsertResult::', userCompanyInsertResult);
      }
      return companyResult.raw.Id;
    }

  }

  public async updateCompanyDetails(updateCompanyDetailsViewModel: UpdateCompanyDetailsViewModel, userId: number): Promise<number> {

    let methodName: string = 'updateCompanyDetails ->';
    console.log(this.className + methodName + 'updateCompanyDetailsViewModel', updateCompanyDetailsViewModel);
    let companyDetails = await this.companyDetailsRepo.findOne({ Id: updateCompanyDetailsViewModel.Id });
    if (null != companyDetails && undefined !== companyDetails) {
      console.log(this.className + methodName + 'company exist');

      companyDetails.CompanyName = updateCompanyDetailsViewModel.CompanyName
      companyDetails.IndustryTypeId = updateCompanyDetailsViewModel.IndustryTypeId;
      companyDetails.StateId = updateCompanyDetailsViewModel.StateId;
      companyDetails.ModifiedBy = userId;
      companyDetails.ModifiedDate = new Date();
      await this.companyDetailsRepo.save(companyDetails)

      if (updateCompanyDetailsViewModel.billingAddress != null && updateCompanyDetailsViewModel.billingAddress != undefined) {

        let companyBillingAddress: CompanyAddress = await this.companyAddressRepo.findOne({ where: { CompanyId: companyDetails.CountryId, AddressTypeId: 1 } });
        if (null != companyBillingAddress) {
          companyBillingAddress.AddressLine1 = updateCompanyDetailsViewModel.billingAddress.AddressLine1;
          companyBillingAddress.AddressLine2 = updateCompanyDetailsViewModel.billingAddress.AddressLine2;
          companyBillingAddress.City = updateCompanyDetailsViewModel.billingAddress.City;
          companyBillingAddress.CorrespondanceEmail = updateCompanyDetailsViewModel.billingAddress.CorrespondanceEmail;
          companyBillingAddress.PointOfContactMobileNumber = updateCompanyDetailsViewModel.billingAddress.PointOfContactMobileNumber;
          companyBillingAddress.PointOfContactName = updateCompanyDetailsViewModel.billingAddress.PointOfContactName;
          companyBillingAddress.ZipCode = updateCompanyDetailsViewModel.billingAddress.ZipCode;
          companyBillingAddress.CountryCodeId = updateCompanyDetailsViewModel.billingAddress.CountryCodeId;
          companyBillingAddress.CountryId = updateCompanyDetailsViewModel.billingAddress.CountryId;
          companyBillingAddress.StateId = updateCompanyDetailsViewModel.billingAddress.StateId;
          companyBillingAddress.LocationType = updateCompanyDetailsViewModel.billingAddress.LocationType;
          companyBillingAddress.ModifiedDate = new Date();
          companyBillingAddress.ModifiedBy = userId;
          this.companyAddressRepo.save(companyBillingAddress);
        } else {

          companyBillingAddress = new CompanyAddress()
          companyBillingAddress.AddressLine1 = updateCompanyDetailsViewModel.billingAddress.AddressLine1;
          companyBillingAddress.AddressLine2 = updateCompanyDetailsViewModel.billingAddress.AddressLine2;
          companyBillingAddress.AddressTypeId = 1;
          companyBillingAddress.City = updateCompanyDetailsViewModel.billingAddress.City;
          companyBillingAddress.CompanyId = updateCompanyDetailsViewModel.Id;
          companyBillingAddress.CorrespondanceEmail = updateCompanyDetailsViewModel.billingAddress.CorrespondanceEmail;
          companyBillingAddress.PointOfContactMobileNumber = updateCompanyDetailsViewModel.billingAddress.PointOfContactMobileNumber;
          companyBillingAddress.PointOfContactName = updateCompanyDetailsViewModel.billingAddress.PointOfContactName;
          companyBillingAddress.ZipCode = updateCompanyDetailsViewModel.billingAddress.ZipCode;
          companyBillingAddress.CountryCodeId = updateCompanyDetailsViewModel.billingAddress.CountryCodeId;
          companyBillingAddress.LocationType = updateCompanyDetailsViewModel.billingAddress.LocationType;
          companyBillingAddress.CountryId = updateCompanyDetailsViewModel.billingAddress.CountryId;
          companyBillingAddress.StateId = updateCompanyDetailsViewModel.billingAddress.StateId;
          companyBillingAddress.CreatedBy = userId;
          companyBillingAddress.CreatedDate = new Date();
          companyBillingAddress.ModifiedBy = null;
          companyBillingAddress.ModifiedDate = null;
          this.companyAddressRepo.insert(companyBillingAddress);
        }

      }
      return updateCompanyDetailsViewModel.Id;

    } else {
      return -1;
    }
  }

  public async insertCompanyOtherAddress(address: InsertCompanyOtherAddress, userId: number): Promise<number> {

    let methodName: string = 'insertCompanyOtherAddress ->';
    console.log(this.className + methodName + 'insertCompanyOtherAddress', address);

    // Get Company record based on id
    let companyDetails = await this.companyDetailsRepo.findOne(address.CompanyId);
    if (null == companyDetails) {
      console.log(this.className + methodName + 'company does not exist  !!');
      return -1;
    }
    console.log(this.className + methodName + 'companyResult::', companyDetails);
    let companyOtherAddress = new CompanyAddress()
    companyOtherAddress.AddressLine1 = address.otherAddress.AddressLine1;
    companyOtherAddress.AddressLine2 = address.otherAddress.AddressLine2;
    companyOtherAddress.AddressTypeId = 2;
    companyOtherAddress.City = address.otherAddress.City;
    companyOtherAddress.CompanyId = address.CompanyId;
    companyOtherAddress.CorrespondanceEmail = address.otherAddress.CorrespondanceEmail;
    companyOtherAddress.PointOfContactMobileNumber = address.otherAddress.PointOfContactMobileNumber;
    companyOtherAddress.PointOfContactName = address.otherAddress.PointOfContactName;
    companyOtherAddress.ZipCode = address.otherAddress.ZipCode;
    companyOtherAddress.CountryCodeId = address.otherAddress.CountryCodeId;
    companyOtherAddress.LocationType = address.otherAddress.LocationType;
    companyOtherAddress.CountryId = address.otherAddress.CountryId;
    companyOtherAddress.StateId = address.otherAddress.StateId;
    companyOtherAddress.TimezoneId = address.otherAddress.TimezoneId;
    companyOtherAddress.AlternativeEmail = address.otherAddress.AlternativeEmail;
    companyOtherAddress.CreatedBy = userId;
    companyOtherAddress.CreatedDate = new Date();
    companyOtherAddress.ModifiedBy = null;
    companyOtherAddress.ModifiedDate = null;
    let insertResult = await this.companyAddressRepo.insert(companyOtherAddress);
    return insertResult.raw.Id;
  }

  public async getCompanyOtherAddressesByCompanyId(companyId: number, page: number, limit: number) {

    let companyAddresses: CompanyAddress[] = await this.companyAddressRepo.find({ where: { CompanyId: companyId, AddressTypeId: 2 } });
    //TODO: JOIN query required
    if (null == companyAddresses && companyAddresses.length == 0) {
      return -1;
    }

    console.log(companyAddresses);
    return companyAddresses;

  }

  public async updateCompanyOtherAddress(companyId: number, address: InsertCompanyOtherAddress, userId: number): Promise<number> {

    let methodName: string = 'updateCompanyOtherAddress ->';
    console.log(this.className + methodName + 'updateCompanyOtherAddress:', address);

    // Get Company record based on id
    let companyDetails = await this.companyDetailsRepo.findOne(address.CompanyId);
    if (null == companyDetails) {
      console.log(this.className + methodName + 'company does not exist  !!');
      return -1;
    }

    let companyOtherAddress: CompanyAddress = await this.companyAddressRepo.findOne({ where: { CompanyId: companyId, AddressTypeId: 2, Id: address.otherAddress.Id } });
    if (null == companyOtherAddress) {
      return -1;
    }

    console.log(this.className + methodName + 'companyResult::', companyDetails);
    companyOtherAddress.AddressLine1 = address.otherAddress.AddressLine1;
    companyOtherAddress.AddressLine2 = address.otherAddress.AddressLine2;
    companyOtherAddress.AddressTypeId = 2;
    companyOtherAddress.City = address.otherAddress.City;
    companyOtherAddress.CompanyId = address.CompanyId;
    companyOtherAddress.CorrespondanceEmail = address.otherAddress.CorrespondanceEmail;
    companyOtherAddress.PointOfContactMobileNumber = address.otherAddress.PointOfContactMobileNumber;
    companyOtherAddress.PointOfContactName = address.otherAddress.PointOfContactName;
    companyOtherAddress.ZipCode = address.otherAddress.ZipCode;
    companyOtherAddress.CountryCodeId = address.otherAddress.CountryCodeId;
    companyOtherAddress.LocationType = address.otherAddress.LocationType;
    companyOtherAddress.CountryId = address.otherAddress.CountryId;
    companyOtherAddress.StateId = address.otherAddress.StateId;
    companyOtherAddress.TimezoneId = address.otherAddress.TimezoneId;
    companyOtherAddress.AlternativeEmail = address.otherAddress.AlternativeEmail;
    companyOtherAddress.CreatedBy = userId;
    companyOtherAddress.CreatedDate = new Date();
    companyOtherAddress.ModifiedBy = null;
    companyOtherAddress.ModifiedDate = null;
    let insertResult = await this.companyAddressRepo.save(companyOtherAddress);
    return insertResult.Id;
  }

  public async insertCompanyProfile(companyProfileViewModel: CompanyProfileViewModel, userId: number): Promise<number> {
    let methodName: string = 'insertCompanyProfile ->';
    console.log(this.className + methodName + 'insertCompanyProfile:', companyProfileViewModel);

    // Get Company record based on id
    let companyDetails = await this.companyDetailsRepo.findOne(companyProfileViewModel.CompanyId);
    if (null == companyDetails) {
      console.log(this.className + methodName + 'company does not exist  !!');
      return -1;
    }

    companyDetails.ImageId = companyProfileViewModel.ImageId;
    companyDetails.BackgroundImageId = companyProfileViewModel.BackgroundImageId
    companyDetails.ModifiedBy = userId;
    companyDetails.ModifiedDate = new Date();
    let insertResult = await this.companyDetailsRepo.save(companyDetails);
    return insertResult.Id;

  }
  public async getLocationTypeByCompanyId(companyId: number) {
    let methodName: string = 'getLocationTypeByCompanyId ->';
    console.log(this.className + methodName + 'companyId', companyId);
    let companyLocations = await this.companyAddressRepo.find({ where: [{ "CompanyId": companyId }] });
    let locationList: Array<LocationTypeViewModel> = [];
    console.log(this.className + methodName + 'companyLocations', companyLocations);
    companyLocations.forEach(element => {
      let locationModel: LocationTypeViewModel = new LocationTypeViewModel();
      locationModel.LocationTypeId = element.Id
      locationModel.LocationTypeName = element.LocationType;
      locationList.push(locationModel);
    });
    return locationList
  }

  public async getCompaniesTheme(companyId: number) {
    let methodName: string = 'getCompaniesTheme ->';
    console.log(this.className + methodName + 'companyId', companyId);
    let companyThemeDetails = await getRepository(CompanyDetails).createQueryBuilder("compDetails")
      .leftJoinAndSelect("compDetails.theme", "theme")
      .where("compDetails.Id = :companyId", { companyId: companyId })
      .getOne();
    let themeDetails: ThemeViewModel = new ThemeViewModel();
    if (null != companyThemeDetails) {
      themeDetails.CompanyId = companyThemeDetails.Id;
      themeDetails.ThemeId = companyThemeDetails.ThemeId;
      themeDetails.Theme = companyThemeDetails.theme.name;
    }
    return themeDetails;
  }

  public async updateTheme(updateThemeViewModel: UpdateThemeViewModel, userId: number) {
    let methodName: string = 'updateTheme ->';
    console.log(this.className + methodName + 'userId', userId);
    let companyDetails = await this.companyDetailsRepo.findOne(updateThemeViewModel.CompanyId);
    if (null != companyDetails) {
      let themeDetails = this.themeRepo.findOne(updateThemeViewModel.ThemeId);
      if (null != themeDetails) {
        companyDetails.ThemeId = updateThemeViewModel.ThemeId;
        companyDetails.ModifiedBy = userId;
        // let localDate: Date = new Date();
        // companyDetails.ModifiedDate = new Date(Date.UTC(localDate.getFullYear(), localDate.getMonth(), localDate.getDay(), localDate.getHours(), localDate.getMinutes(), localDate.getSeconds()));
        companyDetails.ModifiedDate = new Date();
        console.log('Update companyDetails ::', companyDetails)
        let status = await this.companyDetailsRepo.update({ Id: companyDetails.Id }, companyDetails);
        console.log(this.className + methodName + 'update status', status);
        return companyDetails.Id;
      }
      return -2; // theme does not exist
    }
    return -1; // company does not exist
  }

  public async getlocations(companyId): Promise<CompanyLocation[]> {
    // console.log("get input:", Id);
    let companylocation: CompanyLocation[] = await this.companylocationsRepo.query('SELECT * FROM company_location WHERE company_id =' + companyId);
    console.log(companylocation);

    if (companylocation.length == 0) {
      return null;
    }
    let companyData = []
    for (let item of companylocation) {

      let obj: any = {
        "id": item['id'],
        "companyName": item['company_name'],
        "locationName": item['location_name'],
        "locationDescription": item['location_description'],
        "addressTypeId": item['address_type_id'],
        "timezoneId": item['timezone_id'],
        "addressLine1": item['address_line_1'],
        "addressLine2": item['address_line_2'],
        "countryId": item['country_id'],
        "stateId": item['state_id'],
        "cityId": item['city_id'],
        "zipcode": item['zipcode'],
        "status": item['status'],
        "companyId": item['company_id'],
        "createdBy": item['created_by'],
        "createdOn": item['created_on'],
        "updatedOn": item['updated_on'],
      }
      companyData.push(obj)
    }
    console.log(companyData);
    return companyData;
  }


  /**
   * 
   * @param companyid 
   * @returns
   */
  async getCompanyId(companyid: any) {
    try {
      var data = await getConnection()
        .getRepository(Company)
        .createQueryBuilder("company")
        .where('company.id= :id', { id: companyid })
        .innerJoinAndSelect('company.industry_type_id', 'industry_type_id')
        .innerJoinAndSelect('company.country_id', 'country_id')
        .innerJoinAndSelect('company.state_id', 'state_id')
        .getOne();
      var companyDepartmentData = await getConnection()
        .getRepository(CompanyDepartment)
        .createQueryBuilder("company")
        .where('company.company_id= :company_id', { company_id: companyid })
        .getOne();

      var companyLocationData = await getConnection()
        .getRepository(CompanyAddresss)
        .createQueryBuilder("company")
        .where('company.company_id= :company_id', { company_id: companyid })
        .getMany();

      console.log("companyLocationData", companyLocationData);

      var obj = {
        "companyName": data['name'],
        "industryTypeId": data['industry_type_id']['id'],
        "companyCountryId": data['country_id']['id'],
        "companyStateId": data['state_id']['id'],
        "addressLine1": companyLocationData[0]['address_line_1'],
        "addressLine2": companyLocationData[0]['address_line_2'],
        "billingCountryId": companyLocationData[0]['country_id'],
        "billingStateId": companyLocationData[0]['state_id'],
        "billingCityId": companyLocationData[0]['city_id'],
        "zipCode": companyLocationData[0]['zipcode'],
        "primaryContactName": companyLocationData[0]['primary_contact_name'],
        "primaryContactCCCodeId": companyLocationData[0]['primary_contact_mob_country_code'],
        "primaryContactNumber": companyLocationData[0]['primary_contact_mob_number'],
        "contactMail": companyLocationData[0]['correspondence_email'],
        "departments": companyDepartmentData
      }
    } catch (error) {
      throw new HttpException(`comapany id =${companyid} not found`, HttpStatus.NOT_FOUND)
    }
    return obj
  }


  async createCompanyDetails(company_Id: any, data: any) {
    try {
      let value = data;
      let comapanydata = {
        name: value.companyName,
        industry_type_id: value.industryTypeId,
        country_id: value.companyCountryId,
        state_id: value.companyStateId,
        created_by: value.createdBy
      }
      if (company_Id == 0) {
        let createDetails = await this.companyRepo.create(comapanydata)
        var saveCompany = await this.companyRepo.save(createDetails)
        console.log(saveCompany);
      } else {
        const update = await this.companyRepo.update({ id: company_Id }, comapanydata)
        console.log(update)
      }

      if (saveCompany) {
        try {
          var companyId = saveCompany.id;
          let billingAddress = {}
          billingAddress['company_id'] = companyId;
          billingAddress['type'] = 'BILLING';
          billingAddress['address_line_1'] = value['addressLine1'];
          billingAddress['address_line_1'] = value['addressLine2'];
          billingAddress['country_id'] = value['billingCountryId'];
          billingAddress['state_id'] = value['billingStateId'];
          billingAddress['city_id'] = value['billingCityId'];
          billingAddress['zipcode'] = value['zipCode'];
          billingAddress['primary_contact_name'] = value['primaryContactName'];
          billingAddress['primary_contact_mob_country_code'] = value['primaryContactCCCodeId'];
          billingAddress['primary_contact_mob_number'] = value['primaryContactNumber']
          billingAddress['correspondence_email'] = value['contactMail'];
          if (company_Id == 0) {
            var createCompanyAddressDetails = await this.CompanyAddressRepository.create(billingAddress)
            await this.CompanyAddressRepository.save(createCompanyAddressDetails)
          } else {

          }
        } catch (error) {
          throw new HttpException(`Address ${error}`, HttpStatus.BAD_REQUEST)
        }
        if (createCompanyAddressDetails) {
          try {
            // CompanyDepartmentRepo
            var companyDepart = []
            for (let companyDprt of data['departments']) {
              let companyDepartment = {}
              companyDepartment['name'] = companyDprt['name']
              companyDepartment['company_id'] = companyId
              companyDepartment['created_by'] = value.createdBy
              companyDepart.push(companyDepartment)
            }
            if (company_Id == 0) {
              var createComapnyDepartmentDetails = await this.CompanyDepartmentRepo.create(companyDepart)
              await this.CompanyDepartmentRepo.save(createComapnyDepartmentDetails)
            } else {

            }
          } catch (error) {
            throw new HttpException(`Address type ${error}`, HttpStatus.BAD_REQUEST)

          }
        }

      }
    } catch (error) {
      throw new HttpException(`comapany ${error}`, HttpStatus.BAD_REQUEST)
    }
    return companyId
  }

  /**
   * 
   * @param data 
   * @returns 
   */
  async createCompanyLocation(data: any) {
    try {
      console.log(data)
      let createLoaction = await this.companylocationsRepo.create(data)
      var saveLocation = await this.companylocationsRepo.save(createLoaction)
    } catch (error) {
      throw new HttpException(`comapany location ${error}`, HttpStatus.BAD_REQUEST)

    }
    return saveLocation
  }

  async getaddresstype() {
    console.log("addresstype")
    try {
      var data = await getConnection()
        .getRepository(CompanyAddressType)
        .createQueryBuilder("company")
        .getMany();
    } catch (error) {
      throw new HttpException(`address type not found`, HttpStatus.NOT_FOUND)
    }
    return data
  }

  public async getCompanyIduser(companyid: any): Promise<UserRegister[]> {
    // console.log("get input:", Id);

    let response = await getConnection()
      .getRepository(User)
      .createQueryBuilder("user")
      .where("user.company_id = :company_id", { company_id: companyid })
      .getMany();
    if (response.length == 0) {
      return null;
    }
    var data: any = []
    for (let item of response) {
      let role = await getConnection()
        .getRepository(Role)
        .createQueryBuilder("role")
        .where("role.id = :id", { id: item['role_id'] })
        .getOne();
      let location = await getConnection()
        .getRepository(CompanyLocation)
        .createQueryBuilder("location")
        .where("location.id = :id", { id: item['location_id'] })
        .getOne();

      let object = {
        "userId": item['id'],
        "email": item['email'],
        // "encPassword": item['enc_password'],
        "firstName": item['first_name'],
        "lastName": item['last_name'],
        "phoneNumber": item['phone_number'],
        "userStatus": item['is_active'],
        "createdOn": item['created_on'],
        "updatedOn": item['updated_on'],
        "location": location['location_name'],
        "userRole": role['name'],
        "designation": item['designation']
      }
      data.push(object)
    }

    console.log(data);
    return data;
  }

  public async getCompanyIduserid(companyid: any, userid: any): Promise<any> {
    // console.log("get input:", Id);

    // let response: UserRegister[] = await this.userRepo.query('SELECT * FROM user JOIN company ON user.company_id = company.id WHERE user.company_id = ' + companyid + ' AND user.id = ' + userid);
    let response = await getConnection()
      .getRepository(User)
      .createQueryBuilder("user")
      .where("user.company_id = :company_id", { company_id: companyid })
      .where("user.id = :id", { id: userid })
      .getOne();
    if (!response) {
      return null;
    }
    let role = await getConnection()
      .getRepository(Role)
      .createQueryBuilder("role")
      .where("role.id = :id", { id: response['role_id'] })
      .getOne();
    let location = await getConnection()
      .getRepository(CompanyLocation)
      .createQueryBuilder("location")
      .where("location.id = :id", { id: response['location_id'] })
      .getOne();



    let users: any = {
      "userId": response['id'],
      "email": response['email'],
      // "encPassword": response['enc_password'],
      "firstName": response['first_name'],
      "lastName": response['last_name'],
      "phoneNumber": response['phone_number'],
      "userStatus": response['is_active'],
      "createdOn": response['created_on'],
      "updatedOn": response['updated_on'],
      "location": location['location_name'],
      "userRole": role['name'],
      "designation": response['designation']
    }
    // var data: any = []

    // for (let item of response) {
    //   const object = {
    //     id: item['id'],
    //     email: item['email'],
    //     encPassword: item['enc_password'],
    //     companyId: item['company_id'],
    //     firstName: item['first_name'],
    //     lastName: item['last_name'],
    //     phoneNumber: item['phone_number'],
    //     isActive: item['is_active'],
    //     createdOn: item['created_on'],
    //     updatedOn: item['updated_on'],
    //     roleId: item['role_id'],
    //     locationId: item['location_id'],
    //     name: item['name'],
    //     industryTypeId: item['industry_type_id'],
    //     countryId: item['country_id'],
    //     stateId: item['state_id'],
    //     logoImageUploadId: item['logo_image_upload_id'],
    //     bgImageUploadId: item['bg_image_upload_id'],
    //     createdBy: item['created_by'],
    //     themeId: item['theme_id'],
    //     languageId: item['language_id'],
    //     timezoneId: item['timezone_id']
    //   }
    //   data.push(object)

    // }
    // console.log(data);
    return users;
  }
  public async getCompanyIddepartment(companyid: any): Promise<CompanyDepartment[]> {
    // console.log("get input:", Id);
    const response = await getConnection()
      .getRepository(CompanyDepartment)
      .createQueryBuilder("companyDepartment")
      .where("companyDepartment.company_id = :company_id", { company_id: companyid })
      .getMany();
    // let response: CompanyDepartment[] = await this.CompanyDepartmentRepo.query('SELECT * FROM company_department JOIN company ON company_department.company_id = company.id WHERE company_department.company_id = ' + companyid);
    if (response.length == 0) {
      return null;
    }

    console.log(response);
    return response;
  }

  /**
   * 
   * @param company_Id 
   * @param data 
   */
  async createCompanyLocationDetails(company_Id: number, location_id: number, data: any): Promise<any> {
    try {
      let addressObj = {};
      addressObj['company_name'] = data['companyName'];
      addressObj['location_name'] = data['locationName'];
      addressObj['location_description'] = data['locationDescription'];
      addressObj['address_type_id'] = data['addressTypeId'];
      addressObj['timezone_id'] = data['timeZoneID'];
      addressObj['address_line_1'] = data['addressLine1'];
      addressObj['address_line_2'] = data['addressLine2'];
      addressObj['country_id'] = data['countryId'];
      addressObj['state_id'] = data['stateId'];
      addressObj['city_id'] = data['cityId'];
      addressObj['zipcode'] = data['zipCode'];
      addressObj['status'] = data['status'];
      addressObj['company_id'] = company_Id;
      addressObj['created_by'] = data['createdBy'];
      if (location_id == 0) {
        const saveCompanyAddress = await this.companylocationsRepo.create(addressObj);
        const createCompanyAddress = await this.companylocationsRepo.save(saveCompanyAddress);
        console.log(createCompanyAddress)
        var locationId = createCompanyAddress.id;

      } else {
        const update = await this.companylocationsRepo.update({ id: location_id }, addressObj)
        console.log(update)
        var locationId = location_id;
      }

    } catch (error) {
      throw new HttpException(`Location ${error}`, HttpStatus.BAD_REQUEST)

    }

    return locationId

  }

  async createCompanyUser(company_Id: number, user_id: number, data: any) {
    try {

      let userObj: any = {
        email: data.email,
        company_id: company_Id,
        first_name: data.firstName,
        last_name: data.lastName,
        phone_number: data.phoneNumber,
        is_active: data.userStatus,
        role_id: data.userRole,
        location_id: data.location,
        designation: data.designation,
        enc_password: data.password
      }
      if (user_id == 0) {
        const userSave = await this.userRepository.create(userObj);
        const createUser = await this.userRepository.save(userSave);
        return createUser['id']
      }
      const updateUser = await this.userRepository.update({
        id: user_id
      },
        userObj
      );
      return updateUser

    } catch (error) {
      throw new HttpException(`User ${error}`, HttpStatus.BAD_REQUEST)

    }
  }
}
