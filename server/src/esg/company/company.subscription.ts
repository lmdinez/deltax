import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'CompanySubscription' })
export class CompanySubscription {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ name: 'CompanyId' })
    companyId: number;
    @Column({ name: 'PlanId' })
    planId: number;
    @Column({ name: 'NoOfUsers' })
    noOfUsers: number;
    @Column({ name: 'ValidFrom' })
    validFrom: string;
    @Column({ name: 'ValidTo' })
    validTo: string;
    @Column({ name: 'ModifiedDate' })
    modifiedDate: string | null;
    @Column({ name: 'ModifiedBy' })
    modifiedBy: number | null;
    @Column({ name: 'CreatedDate' })
    createdDate: string | null;
    @Column({ name: 'CreatedBy' })
    createdBy: number | null;
}
