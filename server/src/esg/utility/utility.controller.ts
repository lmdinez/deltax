import { Body, Controller, Get, Param, Post, Res } from '@nestjs/common';
import { response } from 'express';
import {
  DeleteCountry,
  DeleteCountryCode,
  DeleteIndustryType,
  DeleteLanguage,
  InsertCountry,
  InsertCountryCode,
  InsertIndustryType,
  InsertLanguage,
  UpdateCountry,
  UpdateCountryCode,
  UpdateIndustryType,
  UpdateLanguage,
} from '../model/utility.model';
import { GlobalResponse } from '../model/response.model';
import { Utility } from '../utils/util';
import { UtilityService } from './utility.service';

@Controller('/Utility')
export class UtilityController {
  constructor(private utilityService: UtilityService) { }
  @Get('country')
  async allcountry(@Res() response) {
    this.utilityService.getallcountry().then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', countries: result };

      return response.status(200).json(final_result);
    });
  }

  @Get('country/:Id')
  async country(@Res() response, @Param('Id') Id: number) {
    this.utilityService.getcountry(Id).then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      return Utility.handleSuccessList(response, result);
    });
  }
  @Post('insertcountry')
  async insertcountry(@Res() response, @Body() insertcountry: InsertCountry) {
    this.utilityService.insertcountry(insertcountry).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'Country already present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'Country inserted successfully.',
        )
        : Utility.handleFailureWithResult(response, 'Country insert failed.');
    });
  }
  @Post('updatecountry')
  async updatecountry(@Res() response, @Body() updatecountry: UpdateCountry) {
    this.utilityService.updatecountry(updatecountry).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'Country already present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'Country updated successfully.',
        )
        : Utility.handleFailureWithResult(response, 'Country update failed.');
    });
  }
  @Post('deletecountry')
  async deletecountry(@Res() response, @Body() deletecountry: DeleteCountry) {
    this.utilityService.deletecountry(deletecountry).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'Country is not present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'Country deleted successfully.',
        )
        : Utility.handleFailureWithResult(response, 'Country delete failed.');
    });
  }
  @Get('countrycode')
  async allcountrycode(@Res() response) {
    this.utilityService.getallcountrycode().then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      return Utility.handleSuccessList(response, result);
    });
  }

  @Get('countrycode/:Id')
  async countrycode(@Res() response, @Param('Id') Id: number) {
    this.utilityService.getcountrycode(Id).then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      return Utility.handleSuccessList(response, result);
    });
  }
  @Get('countrycode/countryid/:CountryId')
  async countrycodecountryid(@Res() response, @Param('CountryId') Id: number) {
    this.utilityService.getcountrycodecountrycode(Id).then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      return Utility.handleSuccessList(response, result);
    });
  }
  @Post('insertcountrycode')
  async insertcountrycode(
    @Res() response,
    @Body() insertcountrycode: InsertCountryCode,
  ) {
    this.utilityService.insertcountrycode(insertcountrycode).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'Country Code already present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'Country Code inserted successfully.',
        )
        : Utility.handleFailureWithResult(
          response,
          'Country Code insert failed.',
        );
    });
  }
  @Post('updatecountrycode')
  async updatecountrycode(
    @Res() response,
    @Body() updatecountrycode: UpdateCountryCode,
  ) {
    // this.utilityService.deletecountry(updatecountrycode).then(result => {
    //   if (-1 == result) {
    //     return Utility.handleFailure(response, "Country Code already present in database");
    //   }
    //   return result != 0 ? Utility.handleSuccessWithResult(response, "Country Code updated successfully.") : Utility.handleFailureWithResult(response, "Country Code update failed.");
    // })
  }
  @Post('deletecountrycode')
  async deletecountrycode(
    @Res() response,
    @Body() deletecountrycode: DeleteCountryCode,
  ) {
    this.utilityService.deletecountrycode(deletecountrycode).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'Country Code is not present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'Country Code deleted successfully.',
        )
        : Utility.handleFailureWithResult(
          response,
          'Country Code delete failed.',
        );
    });
  }

  @Get('industrytype')
  async allindustrytype(@Res() response) {
    this.utilityService.getallindustrytype().then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', industryTypes: result };

      return response.status(200).json(final_result);
    });
  }

  @Get('industrytype/:Id')
  async industrytype(@Res() response, @Param('Id') Id: number) {
    this.utilityService.getindustrytype(Id).then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', industryTypes: result };

      return response.status(200).json(final_result);
    });
  }
  @Post('insertindustrytype')
  async insertindustrytype(
    @Res() response,
    @Body() insertindustrytype: InsertIndustryType,
  ) {
    this.utilityService
      .insertindustrytype(insertindustrytype)
      .then((result) => {
        if (-1 == result) {
          return Utility.handleFailure(
            response,
            'industrytype already present in database',
          );
        }
        return result != 0
          ? Utility.handleSuccessWithResult(
            response,
            'industrytype inserted successfully.',
          )
          : Utility.handleFailureWithResult(
            response,
            'industrytype insert failed.',
          );
      });
  }
  @Post('updateindustrytype')
  async updateindustrytype(
    @Res() response,
    @Body() updateindustrytype: UpdateIndustryType,
  ) {
    this.utilityService
      .updateindustrytype(updateindustrytype)
      .then((result) => {
        if (-1 == result) {
          return Utility.handleFailure(
            response,
            'industrytype already present in database',
          );
        }
        return result != 0
          ? Utility.handleSuccessWithResult(
            response,
            'industrytype updated successfully.',
          )
          : Utility.handleFailureWithResult(
            response,
            'industrytype update failed.',
          );
      });
  }
  @Post('deleteindustrytype')
  async deleteindustrytype(
    @Res() response,
    @Body() deleteindustrytype: DeleteIndustryType,
  ) {
    this.utilityService
      .deleteindustrytype(deleteindustrytype)
      .then((result) => {
        if (-1 == result) {
          return Utility.handleFailure(
            response,
            'industrytype is not present in database',
          );
        }
        return result != 0
          ? Utility.handleSuccessWithResult(
            response,
            'industrytype deleted successfully.',
          )
          : Utility.handleFailureWithResult(
            response,
            'industrytype delete failed.',
          );
      });
  }

  @Get('language')
  async alllanguage(@Res() response) {
    this.utilityService.getalllanguage().then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', languages: result };

      return response.status(200).json(final_result);
    });
  }

  @Get('language/:Id')
  async language(@Res() response, @Param('Id') Id: number) {
    this.utilityService.getlanguage(Id).then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      return Utility.handleSuccessList(response, result);
    });
  }
  @Post('insertlanguage')
  async insertlanguage(
    @Res() response,
    @Body() insertlanguage: InsertLanguage,
  ) {
    this.utilityService.insertlanguage(insertlanguage).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'language already present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'language inserted successfully.',
        )
        : Utility.handleFailureWithResult(response, 'language insert failed.');
    });
  }
  @Post('updatelanguage')
  async updatelanguage(
    @Res() response,
    @Body() updatelanguage: UpdateLanguage,
  ) {
    this.utilityService.updatelanguage(updatelanguage).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'language already present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'language updated successfully.',
        )
        : Utility.handleFailureWithResult(response, 'language update failed.');
    });
  }
  @Post('deletelanguage')
  async deletelanguage(
    @Res() response,
    @Body() deletelanguage: DeleteLanguage,
  ) {
    this.utilityService.deletelanguage(deletelanguage).then((result) => {
      if (-1 == result) {
        return Utility.handleFailure(
          response,
          'language is not present in database',
        );
      }
      return result != 0
        ? Utility.handleSuccessWithResult(
          response,
          'language deleted successfully.',
        )
        : Utility.handleFailureWithResult(response, 'language delete failed.');
    });
  }
  @Get('city/:stateid')
  async city(@Res() response, @Param('stateid') Id: number) {
    this.utilityService.getcity(Id).then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', cities: result };

      return response.status(200).json(final_result);
    });
  }
  @Get('state/:country_id')
  async state(@Res() response, @Param('country_id') Id: number) {
    this.utilityService.getstate(Id).then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', states: result };

      return response.status(200).json(final_result);
    });
  }
  @Get('theme')
  async theme(@Res() response) {
    this.utilityService.gettheme().then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', themes: result };

      return response.status(200).json(final_result);
    });
  }
  @Get('timezone')
  async timezone(@Res() response) {
    this.utilityService.gettimezone().then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', timezones: result };

      return response.status(200).json(final_result);
    });
  }

  @Get('userroles')
  async roles(@Res() response) {
    this.utilityService.getroles().then((result) => {
      if (null == result) {
        return Utility.handleFailure(
          response,
          'No Data is present in database',
        );
      }
      //return Utility.handleSuccessList(response, result);
      let final_result = { status: 'SUCCESS', roles: result };

      return response.status(200).json(final_result);
    });
  }
}
