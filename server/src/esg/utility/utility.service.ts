import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { City } from '../common/entity/city.entity';
import { Country } from '../common/entity/country.entity';
import { CountryCode } from '../common/entity/countryCode.entity';
import { IndustryType } from '../common/entity/industryType.entity';
import { Language } from '../common/entity/language.entity';
import { Role } from '../common/entity/role.entity';
import { State } from '../common/entity/state.entity';
import { Theme } from '../common/entity/theme.entity';
import { Timezone } from '../common/entity/timezone.entity';
import { DeleteCountry, DeleteCountryCode, DeleteIndustryType, DeleteLanguage, InsertCountry, InsertCountryCode, InsertIndustryType, InsertLanguage, UpdateCountry, UpdateCountryCode, UpdateIndustryType, UpdateLanguage } from '../model/utility.model';

@Injectable()
export class UtilityService {
  constructor(
    @InjectRepository(Country) private countryRepo: Repository<Country>,
    @InjectRepository(CountryCode) private countrycodeRepo: Repository<CountryCode>,
    @InjectRepository(IndustryType) private industrytypeRepo: Repository<IndustryType>,
    @InjectRepository(Language) private languageRepo: Repository<Language>,
    @InjectRepository(City) private cityRepo: Repository<City>,
    @InjectRepository(State) private stateRepo: Repository<State>,
    @InjectRepository(Theme) private themeRepo: Repository<Theme>,
    @InjectRepository(Timezone) private timezoneRepo: Repository<Timezone>,
    @InjectRepository(Role) private roleRepo: Repository<Role>,
  ) { }
  public async getallcountry(): Promise<Country[]> {
    let country: Country[] = await this.countryRepo.find();
    if (null == country && country.length == 0) {
      return null;
    }

    console.log(country);
    return country;

  }
  public async getcountry(Id: number): Promise<Country[]> {
    console.log("get input:", Id);
    let country: Country[] = await this.countryRepo.find({ where: { Id: Id } });
    if (country.length == 0) {
      return null;
    }

    console.log(country);
    return country;
  }
  public async insertcountry(insertcountry: InsertCountry): Promise<number> {
    let country = await this.countryRepo.findOne({ where: { name: insertcountry.name } })
    if (country) {
      console.log("Country already exists.");
      return -1;
    }
    let countryEntity: Country = new Country();
    countryEntity.name = insertcountry.name;
    countryEntity.code = insertcountry.code;
    countryEntity.active = 1;
    countryEntity.phonecode = insertcountry.phonecode;
    let countryInsertResult = await this.countryRepo.insert(countryEntity);
    return countryInsertResult.raw.Id;
  }
  public async updatecountry(updatecountry: UpdateCountry): Promise<number> {

    let countryEntity: Country = new Country();
    countryEntity.name = updatecountry.name;
    countryEntity.code = updatecountry.code;
    countryEntity.active = updatecountry.active;
    countryEntity.phonecode = updatecountry.phonecode;
    let countryUpdateResult = await this.countryRepo.update({ id: updatecountry.id }, countryEntity);
    return updatecountry.id;
  }
  public async deletecountry(deletecountry: DeleteCountry): Promise<number> {

    let country: Country[] = await this.countryRepo.find({ where: { id: deletecountry.id } });
    console.log
    if (country.length == 0) {
      return -1;
    } else {

      let countryUpdateResult = await this.countryRepo.delete({ id: deletecountry.id });
      return 1;
    }
  }

  public async getallcountrycode(): Promise<CountryCode[]> {
    let countrycode: CountryCode[] = await this.countrycodeRepo.find();
    if (null == countrycode && countrycode.length == 0) {
      return null;
    }

    console.log(countrycode);
    return countrycode;

  }
  public async getcountrycode(Id: number): Promise<CountryCode[]> {
    console.log("get input:", Id);
    let countrycode: CountryCode[] = await this.countrycodeRepo.find({ where: { Id: Id } });
    if (countrycode.length == 0) {
      return null;
    }

    console.log(countrycode);
    return countrycode;
  }
  public async getcountrycodecountrycode(Id: number): Promise<CountryCode[]> {
    console.log("get input:", Id);
    let countrycode: CountryCode[] = await this.countrycodeRepo.find({ where: { CountryId: Id } });
    if (countrycode.length == 0) {
      return null;
    }

    console.log(countrycode);
    return countrycode;
  }
  public async insertcountrycode(insertcountrycode: InsertCountryCode): Promise<number> {
    let countrycode = await this.countryRepo.findOne({ where: { Code: insertcountrycode.Code } })
    if (countrycode) {
      console.log("Country Code already exists.");
      return -1;
    }
    let countrycodeEntity: CountryCode = new CountryCode();
    countrycodeEntity.Code = insertcountrycode.Code;
    countrycodeEntity.CountryId = insertcountrycode.CountryId;
    let countrycodeInsertResult = await this.countrycodeRepo.insert(countrycodeEntity);
    return countrycodeInsertResult.raw.Id;
  }
  // public async updatecountrycode(updatecountrycode: UpdateCountryCode): Promise<number> {

  //   let countrycodeEntity: CountryCode = new CountryCode();
  //   countrycodeEntity.Code = updatecountrycode.Code;
  //   countrycodeEntity.CountryId = updatecountrycode.CountryId;
  //   let countryUpdateResult = await this.countryRepo.update({ id: updatecountrycode.id }, countrycodeEntity);
  //   return updatecountrycode.Id;
  // }
  public async deletecountrycode(deletecountrycode: DeleteCountryCode): Promise<number> {

    let countrycode: CountryCode[] = await this.countrycodeRepo.find({ where: { Id: deletecountrycode.Id } });
    console.log
    if (countrycode.length == 0) {
      return -1;
    } else {

      let countrycodeUpdateResult = await this.countrycodeRepo.delete({ Id: deletecountrycode.Id });
      return 1;
    }
  }

  public async getallindustrytype(): Promise<IndustryType[]> {
    let industrytype: IndustryType[] = await this.industrytypeRepo.query('SELECT * FROM industry_type');
    if (null == industrytype && industrytype.length == 0) {
      return null;
    }

    console.log(industrytype);
    return industrytype;

  }
  public async getindustrytype(Id: number): Promise<IndustryType[]> {
    console.log("get input:", Id);
    let industrytype: IndustryType[] = await this.industrytypeRepo.find({ where: { Id: Id } });
    if (industrytype.length == 0) {
      return null;
    }

    console.log(industrytype);
    return industrytype;
  }
  public async insertindustrytype(insertindustrytype: InsertIndustryType): Promise<number> {
    let industrytype = await this.industrytypeRepo.findOne({ where: { type: insertindustrytype.type } })
    if (industrytype) {
      console.log("industrytype already exists.");
      return -1;
    }
    let industrytypeEntity: IndustryType = new IndustryType();
    industrytypeEntity.type = insertindustrytype.type;
    industrytypeEntity.is_active = 1;
    industrytypeEntity.created_on = new Date();
    let industrytypeInsertResult = await this.industrytypeRepo.insert(industrytypeEntity);
    return industrytypeInsertResult.raw.Id;
  }
  public async updateindustrytype(updateindustrytype: UpdateIndustryType): Promise<number> {

    let industrytypeEntity: IndustryType = new IndustryType();
    industrytypeEntity.type = updateindustrytype.type;
    let industrytypeUpdateResult = await this.industrytypeRepo.update({ id: updateindustrytype.Id }, industrytypeEntity);
    return updateindustrytype.Id;
  }
  public async deleteindustrytype(deleteindustrytype: DeleteIndustryType): Promise<number> {

    let industrytype: IndustryType[] = await this.industrytypeRepo.find({ where: { Id: deleteindustrytype.Id } });
    console.log
    if (industrytype.length == 0) {
      return -1;
    } else {

      let industrytypeUpdateResult = await this.industrytypeRepo.delete({ id: deleteindustrytype.Id });
      return 1;
    }
  }

  public async getalllanguage(): Promise<Language[]> {
    let language: Language[] = await this.languageRepo.query('SELECT * FROM language');
    if (null == language && language.length == 0) {
      return null;
    }

    console.log(language);
    return language;

  }
  public async getlanguage(Id: number): Promise<Language[]> {
    console.log("get input:", Id);
    let language: Language[] = await this.languageRepo.find({ where: { Id: Id } });
    if (language.length == 0) {
      return null;
    }

    console.log(language);
    return language;
  }
  public async insertlanguage(insertlanguage: InsertLanguage): Promise<number> {
    let language = await this.languageRepo.findOne({ where: { name: insertlanguage.name } })
    if (language) {
      console.log("language already exists.");
      return -1;
    }
    let languageEntity: Language = new Language();
    languageEntity.name = insertlanguage.name;
    languageEntity.code = insertlanguage.code;
    let languageInsertResult = await this.languageRepo.insert(languageEntity);
    return languageInsertResult.raw.Id;
  }
  public async updatelanguage(updatelanguage: UpdateLanguage): Promise<number> {

    let languageEntity: Language = new Language();
    languageEntity.name = updatelanguage.name;
    languageEntity.code = updatelanguage.code;

    let languageUpdateResult = await this.languageRepo.update({ id: updatelanguage.id }, languageEntity);
    return updatelanguage.id;
  }
  public async deletelanguage(deletelanguage: DeleteLanguage): Promise<number> {

    let language: Language[] = await this.languageRepo.find({ where: { id: deletelanguage.id } });
    console.log
    if (language.length == 0) {
      return -1;
    } else {

      let languageUpdateResult = await this.languageRepo.delete({ id: deletelanguage.id });
      return 1;
    }
  }
  public async getcity(Id: number): Promise<City[]> {
    console.log("get input:", Id);
    let city: City[] = await this.cityRepo.query('SELECT * FROM city WHERE state_id = ' + Id);
    if (city.length == 0) {
      return null;
    }

    console.log(city);
    return city;
  }
  public async getstate(Id: number): Promise<State[]> {
    console.log("get input:", Id);

    let state: State[] = await this.stateRepo.query('SELECT * FROM state WHERE country_id = ' + Id);
    if (state.length == 0) {
      return null;
    }

    console.log(state);
    return state;
  }

  public async gettheme(): Promise<Theme[]> {
    // console.log("get input:", Id);

    let theme: Theme[] = await this.themeRepo.query('SELECT * FROM theme');
    if (theme.length == 0) {
      return null;
    }

    console.log(theme);
    return theme;
  }

  public async gettimezone(): Promise<Timezone[]> {
    // console.log("get input:", Id);

    let timezone: Timezone[] = await this.timezoneRepo.query('SELECT * FROM timezone');
    if (timezone.length == 0) {
      return null;
    }

    console.log(timezone);
    return timezone;
  }


  public async getroles(): Promise<Role[]> {
    // console.log("get input:", Id);

    let role: Role[] = await this.roleRepo.query('SELECT * FROM role');
    if (role.length == 0) {
      return null;
    }

    console.log(role);
    return role;
  }
}
