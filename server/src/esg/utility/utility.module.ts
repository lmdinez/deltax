import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { City } from '../common/entity/city.entity';
import { Country } from "../common/entity/country.entity";
import { CountryCode } from '../common/entity/countryCode.entity';
import { IndustryType } from '../common/entity/industryType.entity';
import { Language } from '../common/entity/language.entity';
import { Role } from '../common/entity/role.entity';
import { State } from '../common/entity/state.entity';
import { Theme } from '../common/entity/theme.entity';
import { Timezone } from '../common/entity/timezone.entity';
import { UtilityController } from './utility.controller';
import { UtilityService } from './utility.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Country, CountryCode, IndustryType, Language, City, State, Theme, Timezone, Role])
  ],
  controllers: [UtilityController],
  providers: [UtilityService]
})
export class UtilityModule { }
