import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import {
  TwoFactorAuthenticationModel,
  UserCredentialsViewModel,
  UserDto,
} from '../users/dto/user.dto';
import { RegistrationStatus } from './interface/registration.status';
import { UsersService } from '../users/users.service';
import { LoginStatus } from './interface/login.status';
import { LoginUserDto } from '../users/dto/user-login.dto';
import { JwtPayload } from './interface/jwt.payload';
import { JwtService } from '@nestjs/jwt';
import { toUserDto } from '../shared/mapper';
import { TwoFactorAuthenticationService } from './2f.authentication.service';
import { CreateUserDto } from '../users/dto/user.create.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly twoFactAuthService: TwoFactorAuthenticationService,
  ) { }

  async register(userDto: CreateUserDto): Promise<RegistrationStatus> {
    let status: RegistrationStatus = {
      status: 'SUCCESS',
      message: 'User Registered',
    };

    try {
      await this.usersService.create(userDto);
    } catch (err) {
      status = {
        status: err.status,
        message: err.message,
      };
    }
    return status;
  }

  async userLogin(loginUserDto: LoginUserDto): Promise<LoginStatus> {
    // find user in db
    const user = await this.usersService.authenticateUser(loginUserDto);
    console.log("USER", user);

    // generate and sign token
    const jwtResult = this._createToken(user);
    console.log('jwtResult==>', jwtResult);
    if (null != jwtResult || undefined != jwtResult) {
      user.accessToken = jwtResult.accessToken;
      user.expiresIn = jwtResult.expiryIn;
    } else {
      throw new HttpException(
        'TOKEN GENERATION FAILED',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
    // return {
    //   username: user.UserName,

    //   ...token,
    // };
    return user;
  }

  async loginWithOtp(
    twoFactorAuthModel: TwoFactorAuthenticationModel,
  ): Promise<LoginStatus> {
    let mfaStatus = await this.twoFactAuthService.verifyTwoFactorAuthentication(
      twoFactorAuthModel,
    );
    if (!mfaStatus) {
      //Handle error code.
      throw new HttpException('INVALID OTP', HttpStatus.UNAUTHORIZED);
    }

    // find user in db
    const user = await this.usersService.findByLoginUser(
      twoFactorAuthModel.userName,
    );

    // generate and sign token
    const jwtResult = this._createToken(user);
    if (null != jwtResult || undefined != jwtResult) {
      user.accessToken = jwtResult.accessToken;
      user.expiresIn = jwtResult.expiryIn;
    } else {
      throw new HttpException(
        'TOKEN GENERATION FAILED',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
    return user;
  }

  async validateUser(payload: JwtPayload): Promise<UserDto> {
    console.log('validateUser - JwtPayload>>', payload);
    const user = await this.usersService.findByPayload(payload);
    let userDto: UserDto = toUserDto(user);

    console.log('validateUser - USER >>', userDto);

    if (!user) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
    return userDto;
  }

  private _createToken(loginStatus: LoginStatus): any {
    let userName: string = loginStatus.userName;
    const expiryIn = process.env.JWT_EXPIRES_IN;
    // console.log('expiryIn=>', expiryIn);
    // console.log('Secret=>', process.env.JWT_SECRET);
    // const secretOrKey = process.env.JWT_SECRET;

    const user: JwtPayload = { UserName: userName };
    const accessToken = this.jwtService.sign(user);
    return {
      expiryIn,
      accessToken,
    };
  }

  private _createToken_bkup({ UserName }: UserDto): any {
    const expiryIn = process.env.JWT_EXPIRES_IN;
    // console.log('expiryIn=>', expiryIn);
    // console.log('Secret=>', process.env.JWT_SECRET);
    // const secretOrKey = process.env.JWT_SECRET;

    const user: JwtPayload = { UserName: UserName };
    const accessToken = this.jwtService.sign(user);
    return {
      expiryIn,
      accessToken,
    };
  }
}
