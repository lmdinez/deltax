import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../usermgmt/entity/user.entity';

@Entity({ name: 'mfa_details' })
export class MfaDetails {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'user_id' })
  user_id: number;
  @Column({ name: 'generation_time' })
  generation_time: Date;
  @Column({ name: 'token' })
  token: string;
  @Column({ name: 'secret' })
  secret: string;
  @Column({ name: 'verify_time' })
  verify_time: Date;
  @Column({ name: 'verify_status' })
  verify_status: number;
  @Column({ name: 'attempt' })
  attempt: number;
}
