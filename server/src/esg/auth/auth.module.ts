import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { User } from '../usermgmt/entity/user.entity';
import { TwoFactorAuthenticationService } from './2f.authentication.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MfaDetails } from './mfa.details.entity';
import { EmailService } from '../common/email/email.service';
import { Designations } from '../usermgmt/entity/designations.entity';
import { UserRegister } from '../usermgmt/entity/registerUser.entity';
import { ZeptoMailClientService } from '../common/zeptomail/zeptomail.service';

@Module({
  imports: [
    UsersModule,
    User,
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {
        expiresIn: process.env.JWT_EXPIRES_IN,
      },
    }),
    TypeOrmModule.forFeature([MfaDetails, UserRegister]),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    JwtStrategy,
    TwoFactorAuthenticationService,
    EmailService,
    // ZeptoMailClientService,
  ],
  exports: [PassportModule, JwtModule],
})
export class AuthModule {}
