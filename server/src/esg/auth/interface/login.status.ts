import { UserDto } from '../../users/dto/user.dto';

export class LoginStatus {
  userName: string;
  accessToken: any;
  expiresIn: any;
  email: string;
  firstName: string;
  lastName: string;
  image: string;
  userRole: string;
  designation: number;
  //  companyId:number;
  //   theme:string;
  //   userStatus:number;
  //   accountModule:number
  // "companyId": 12, // Send 0 for company details are not added
  // "theme": "green-theme",
  // "userStatus": 1, // 0 - Inactive, 1 - Active, 2 - License expired
  // "accountModule": 0, // 0 - Not Completed, 1- Completed
}
