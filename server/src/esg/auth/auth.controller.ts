import {
  Controller,
  Body,
  Post,
  HttpException,
  HttpStatus,
  UsePipes,
  Get,
  Req,
  UseGuards,
  Param,
  Res,
} from '@nestjs/common';
import {
  TwoFactorAuthenticationModel,
  UserCredentialsViewModel,
  UserDto,
} from '../users/dto/user.dto';
import { RegistrationStatus } from './interface/registration.status';
import { AuthService } from './auth.service';
import { LoginStatus } from './interface/login.status';
import { LoginUserDto } from '../users/dto/user-login.dto';
import { JwtPayload } from './interface/jwt.payload';
import { AuthGuard } from '@nestjs/passport';
import { TwoFactorAuthenticationService } from './2f.authentication.service';
import { Utility } from '../utils/util';
import { EmailService } from '../common/email/email.service';
import { CreateUserDto } from '../users/dto/user.create.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly twoFactAuthService: TwoFactorAuthenticationService,
    private emailService: EmailService,
  ) {}

  @Post('/register')
  public async register(
    @Body() createUserDto: CreateUserDto,
  ): Promise<RegistrationStatus> {
    const result: RegistrationStatus = await this.authService.register(
      createUserDto,
    );
    if (!result.status) {
      console.log('Error, Something went wrong. Try again later');
      throw new HttpException(result.message, HttpStatus.BAD_REQUEST);
    }
    return result;
  }

  @Post('/login')
  public async userLogin(@Res() response, @Body() loginUserDto: LoginUserDto) {
    let result = null;
    try {
      result = await this.authService.userLogin(loginUserDto);
      if (result) {
        return Utility.handleSuccessWithResult(response, result);
      }
    } catch (e) {
      return Utility.handleFailure(response, e.message, e.status);
    }
    return Utility.handleFailure(
      response,
      'Error, Something went wrong. Try again later',
      401,
    );
  }

  @Post('/generate-otp')
  public async generateSecret(
    @Res() response,
    @Body() twoFactorAuthModel: TwoFactorAuthenticationModel,
  ) {
    // this.emailService.sendEmail();
    let result = await this.twoFactAuthService.generateTwoFactorAuthentication(
      twoFactorAuthModel,
    );
    if (result) {
      return Utility.handleSuccess(response, 'OTP geenration successful.');
    }
    return Utility.handleFailure(response, 'OTP generation failed.');
  }

  @Post('/validate-otp')
  public async verfiySecret(
    @Res() response,
    @Body() twoFactorAuthModel: TwoFactorAuthenticationModel,
  ) {
    // return await this.twoFactAuthService.verifyTwoFactorAuthentication(twoFactorAuthModel);
    let result = null;
    try {
      result = await this.authService.loginWithOtp(twoFactorAuthModel);
      if (result) {
        return Utility.handleSuccessWithResult(response, result);
      }
    } catch (e) {
      console.log(e);
      return Utility.handleFailure(response, e.message, e.status);
    }
    return Utility.handleFailure(response, 'MFA failed', 401);
  }

  /*
    @Get('whoami')
    @UseGuards(AuthGuard())
    public async testAuth(@Req() req: any): Promise<JwtPayload> {
      return req.user;
    }
    */
}
