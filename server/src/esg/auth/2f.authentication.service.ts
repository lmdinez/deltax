import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { authenticator, totp, hotp } from 'otplib';
import { Repository } from 'typeorm';
import { ZeptoMailClientService } from '../common/zeptomail/zeptomail.service';
import { UserRegister } from '../usermgmt/entity/registerUser.entity';
import { TwoFactorAuthenticationModel } from '../users/dto/user.dto';
import { MfaDetails } from './mfa.details.entity';

// import User from '../../users/user.entity';
// import { UsersService } from '../../users/users.service';

@Injectable()
export class TwoFactorAuthenticationService {
  constructor(
    @InjectRepository(MfaDetails)
    private mfaDetailsRepo: Repository<MfaDetails>,
    @InjectRepository(UserRegister)
    private userRepo: Repository<UserRegister>, // private zeptoMailServ: ZeptoMailClientService, // @InjectRepository(ZeptoMailClientService) // private zeptoMailServ: Repository<ZeptoMailClientService>, //   private readonly configService: ConfigService
  ) {}

  public async generateTwoFactorAuthentication(
    twoFactorAuthModel: TwoFactorAuthenticationModel,
  ): Promise<boolean> {
    //const secret = "FJRQUCDJEZSCKPLQ" + twoFactorAuthModel.UserName;
    let userName: string = twoFactorAuthModel.userName;
    console.log(twoFactorAuthModel);
    const secret = authenticator.generateSecret();
    console.log('secret::', secret);
    let maxDigit: number = +process.env.MFA_OTP_DIGITS;
    let expiry: number = +process.env.MFA_OTP_EXPIRY;

    totp.options = {
      digits: maxDigit,
      epoch: Date.now(),
      step: expiry,
      window: 0,
    };
    console.log('OTP OPTIONS::', totp.options);
    let otp: string = null;
    try {
      otp = totp.generate(secret);
      console.log(otp);
      let findUserId: UserRegister = await this.userRepo.findOne({
        where: { email: twoFactorAuthModel.userName },
      });
      console.log(findUserId);
      if (findUserId) {
        let mfaDetails: MfaDetails = await this.mfaDetailsRepo.findOne({
          where: { user_id: findUserId.id },
        });
        if (null == mfaDetails || undefined == mfaDetails) {
          mfaDetails = new MfaDetails();
        }
        console.log(twoFactorAuthModel);
        mfaDetails.generation_time = new Date();
        mfaDetails.secret = secret;
        mfaDetails.token = otp;
        mfaDetails.user_id = findUserId.id;
        mfaDetails.attempt = 0;
        mfaDetails.verify_status = 0;
        let result = await this.mfaDetailsRepo.save(mfaDetails);
        console.log(result);
      }
      // console.log('DB INSERTED::', result);
      // console.log(authenticator.generate(secret));
      // const otpauthUrl = authenticator.keyuri(user.email, this.configService.get('TWO_FACTOR_AUTHENTICATION_APP_NAME'), secret);
      // await this.usersService.setTwoFactorAuthenticationSecret(secret, user.id);
      // return {
      //   secret,
      //   otpauthUrl
      // }

      /*
       * TODO
       * SEND EMAIL WITH OTP
       */
      // this.zeptoMailServ.sendMail();
    } catch (e) {
      console.log('OTP generation failure::' + e.message);
      return false;
    }
    return otp == null || otp == undefined || otp == '' ? false : true;
  }

  //Return verifyStatus, message

  public async verifyTwoFactorAuthentication(
    twoFactorAuthModel: TwoFactorAuthenticationModel,
  ): Promise<boolean> {
    let findUserId: UserRegister = await this.userRepo.findOne({
      where: { email: twoFactorAuthModel.userName },
    });
    let mfaDetails: MfaDetails = await this.mfaDetailsRepo.findOne({
      where: { user_id: findUserId.id },
    });
    console.log('MfaDetails::', mfaDetails);
    let retry: number = mfaDetails.attempt;
    if (
      null != mfaDetails &&
      undefined != mfaDetails &&
      mfaDetails.verify_status == 0 &&
      retry < 3
    ) {
      const isValid: boolean =
        totp.check(twoFactorAuthModel.otpCode, mfaDetails.secret) &&
        mfaDetails.token == twoFactorAuthModel.otpCode;
      //update Attempt count;
      mfaDetails.attempt = retry + 1;
      if (isValid) {
        mfaDetails.verify_status = 1;
      }
      mfaDetails.verify_time = new Date();
      this.mfaDetailsRepo.save(mfaDetails);
      return isValid;
    } else {
      console.log(
        'MFA failure::UserName>' +
          twoFactorAuthModel.userName +
          '::VerifyStatus>' +
          mfaDetails.verify_status +
          '::retry>' +
          retry,
      );
      return false;
    }
  }
}
