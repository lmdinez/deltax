import { UserDto } from '../users/dto/user.dto';
import { User } from '../usermgmt/entity/user.entity';

export const toUserDto = (data: User): UserDto => {
  const { Id, UserName, Email } = data;

  let userDto: UserDto = {
    Id,
    UserName,
    Email,
  };

  return userDto;
};
