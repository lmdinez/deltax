import { Body, Controller, Get, Post, Res } from "@nestjs/common";
import { UserCompleteInformationViewModel } from "../model/user.view.model";
import { Utility } from "../utils/util";
import { UserManagementService } from "./user.management.service";

@Controller('/UserManagement')
export class UserManagementController {
  constructor(private userManagementService: UserManagementService) {
  }

  @Get('/CompleteInfo')
  public getUserCompleteInformation(@Res() response) {
    let userId: number = Utility.getUserId()
    this.userManagementService.getUserCompleteInformation(userId).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    });

  }

}
