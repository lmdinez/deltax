import { ConsoleLogger, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { CountryCode } from '../common/entity/countryCode.entity';
import { CompanyAddress } from '../company/entity/company.address.entity';
import { CompanyDetails } from '../company/entity/company.details.entity';
import { UserCompany } from '../company/entity/user.company.entity';
import {
  CompanyBillingAddress,
  CompanyOtherAddress,
} from '../model/company.model';
import { UserCompleteInformationViewModel } from '../model/user.view.model';
import { Designations } from './entity/designations.entity';
import { Roles } from './entity/roles.entity';
import { Status } from './entity/status.entity';
import { User } from './entity/user.entity';
import { UserRoles } from './entity/user.roles.entity';

@Injectable()
export class UserManagementService {
  private className: string = 'UserManagementService -->';

  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    @InjectRepository(UserRoles) private userRolesRepo: Repository<UserRoles>,
    @InjectRepository(Designations)
    private designationsRepo: Repository<Designations>,
    @InjectRepository(Roles) private rolesRepo: Repository<Roles>,
    @InjectRepository(CountryCode)
    private countryCodeRepo: Repository<CountryCode>,
    @InjectRepository(Status) private statusRepo: Repository<Status>,
    @InjectRepository(UserCompany)
    private userCompanyRepo: Repository<UserCompany>,
    @InjectRepository(CompanyAddress)
    private companyAddressRepo: Repository<CompanyAddress>,
  ) { }

  public async getUserCompleteInformation(userId: number) {
    let methodName: string = 'getUserCompleteInformation ->';
    console.log(this.className + methodName);
    var userInfo = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.designation', 'designation')
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('user.status', 'status')
      .leftJoinAndSelect('user.country', 'country')
      .leftJoinAndSelect('user.userCompany', 'userCompany')
      .leftJoinAndSelect('user.companyAddress', 'companyAddress')
      .where('user.Id = :userId', { userId: userId })
      .getOne();
    console.log(this.className + methodName + 'userInfo::', userInfo);
    let userCompleteInfo: UserCompleteInformationViewModel =
      new UserCompleteInformationViewModel();
    (userCompleteInfo.Id = userInfo.Id),
      (userCompleteInfo.UserName = userInfo.UserName),
      (userCompleteInfo.FirstName = userInfo.FirstName),
      (userCompleteInfo.LastName = userInfo.LastName),
      (userCompleteInfo.DateOfBirth = userInfo.DateOfBirth),
      (userCompleteInfo.PhoneNumber = userInfo.PhoneNumber),
      (userCompleteInfo.Status = userInfo.status.Name),
      (userCompleteInfo.StatusId = userInfo.status.Id),
      (userCompleteInfo.Email = userInfo.Email),
      (userCompleteInfo.Image = userInfo.Image),
      (userCompleteInfo.DesignationId = userInfo.designation.Id),
      (userCompleteInfo.Designation = userInfo.designation.Name),
      (userCompleteInfo.RoleId = userInfo.roles[0].Id),
      (userCompleteInfo.Role = userInfo.roles[0].Name),
      (userCompleteInfo.CountryCodeId = userInfo.country.Id),
      (userCompleteInfo.CountryCode = userInfo.country.Code),
      (userCompleteInfo.IsPrimaryContact = userInfo.IsPrimaryContact),
      (userCompleteInfo.LocationType = userInfo.companyAddress.LocationType),
      (userCompleteInfo.LocationTypeId = userInfo.LocationTypeId);
    console.log(
      this.className + methodName + 'UserCompleteInformationViewModel::',
      userCompleteInfo,
    );

    let finalList: Array<any> = [];
    const companyList = [];
    finalList.push(userCompleteInfo);
    // The below query with User company details fetched from above query
    let companyDataList = await this.userCompanyRepo.find({
      where: [{ UserId: userId }],
    });

    for await (const userCompany of companyDataList) {
      let compDetails = await getRepository(CompanyDetails)
        .createQueryBuilder('compDetails')
        .leftJoinAndSelect('compDetails.state', 'state')
        .leftJoinAndSelect('compDetails.industryType', 'industryType')
        .leftJoinAndSelect('compDetails.country', 'country')
        .leftJoinAndSelect('compDetails.theme', 'theme')
        .where('compDetails.Id = :companyId', {
          companyId: userCompany.CompanyId,
        })
        .getOne();
      console.log(
        this.className + methodName + 'For companyData::',
        compDetails,
      );
      if (undefined != compDetails) {
        var data: { [k: string]: any } = {};
        data.CompanyName = compDetails.CompanyName;
        data.Country = compDetails.country;
        data.CountryId = compDetails.CountryId;
        data.IndustryType = compDetails.industryType;
        data.IndustryTypeId = compDetails.IndustryTypeId;
        data.CompanyId = compDetails.Id;
        data.State = compDetails.state;
        data.StateId = compDetails.StateId;
        data.Theme = compDetails.theme;
        data.ThemeId = compDetails.ThemeId;
        data.ImageId = compDetails.ImageId;
        data.BackgroundimageId = compDetails.BackgroundImageId;

        //To get billing address
        let billingAddress: any = await this.getAddress(compDetails.Id, 1);
        if (null != billingAddress || undefined != billingAddress) {
          let companyBillingAddress: CompanyBillingAddress =
            new CompanyBillingAddress();
          companyBillingAddress.AddressLine1 = billingAddress.AddressLine1;
          companyBillingAddress.AddressLine2 = billingAddress.AddressLine2;
          companyBillingAddress.City = billingAddress.City;
          companyBillingAddress.CorrespondanceEmail =
            billingAddress.CorrespondanceEmail;
          companyBillingAddress.PointOfContactMobileNumber =
            billingAddress.PointOfContactMobileNumber;
          companyBillingAddress.PointOfContactName =
            billingAddress.PointOfContactName;
          companyBillingAddress.ZipCode = billingAddress.ZipCode;
          companyBillingAddress.AlternativeEmail =
            billingAddress.AlternativeEmail;
          companyBillingAddress.CountryId = billingAddress.CountryId;
          companyBillingAddress.Country = compDetails.country.name;
          companyBillingAddress.CountryCode = billingAddress.countryCode.Code;
          companyBillingAddress.CountryCodeId = billingAddress.CountryCodeId;
          companyBillingAddress.LocationType = billingAddress.LocationType;
          companyBillingAddress.State = billingAddress.state.Name;
          companyBillingAddress.StateId = billingAddress.StateId;
          data.billingAddress = companyBillingAddress;
          console.log(
            this.className + methodName + 'COMPANY BILLING ADDRESS ::',
            data.billingAddress,
          );
        }

        let otherAddress = await this.getAddress(compDetails.Id, 2);
        if (null != otherAddress || undefined != otherAddress) {
          console.log(
            this.className +
            methodName +
            '********    getAddress **************',
            otherAddress,
          );
          let otherAddrList: Array<CompanyOtherAddress> = [];
          otherAddress.forEach((element) => {
            let cmpOthAddress: CompanyOtherAddress = new CompanyOtherAddress();
            cmpOthAddress.AddressLine1 = element.AddressLine1;
            cmpOthAddress.AddressLine2 = element.AddressLine2;
            cmpOthAddress.City = element.City;
            cmpOthAddress.CorrespondanceEmail = element.CorrespondanceEmail;
            cmpOthAddress.PointOfContactMobileNumber =
              element.PointOfContactMobileNumber;
            cmpOthAddress.PointOfContactName = element.PointOfContactName;
            cmpOthAddress.ZipCode = element.ZipCode;
            otherAddrList.push(cmpOthAddress);
          });
          data.otherAddress = otherAddrList;
          console.log(
            this.className + methodName + 'COMPANY OTHER ADDRESS ::',
            data.otherAddress,
          );
        }
        companyList.push(data);
        // companyList.push(compDetails);
        console.log(
          this.className + methodName + 'Insinde function companyList===>>::',
          companyList,
        );
      }
    }
    console.log('Final companyList.length::', companyList.length);
    finalList.push(companyList);
    console.log('finalList::', finalList);
    return finalList;
  }

  public async getUserCompleteInformation_bkup(userId: number) {
    // The below query with User company details fetched from above query
    let companyDataList = await this.userCompanyRepo.find({
      where: [{ UserId: userId }],
    });

    // var companyList: CompanyDetails[] = [];
    const companyList = [];
    let i: number = 0;
    /*
        companyDataList.forEach(userCompany => {
          //userCompany.CompanyId
             var compDetails = getRepository(CompanyDetails).createQueryBuilder('compDetails')
            .leftJoinAndSelect("compDetails.state", "state")
            .leftJoinAndSelect("compDetails.industryType", "industryType")
            .leftJoinAndSelect("compDetails.country", "country")
            .leftJoinAndSelect("compDetails.theme", "theme")
            .where("compDetails.Id = :companyId", { companyId: userCompany.CompanyId })
            // .printSql()
            .getOne()
            .then(companyData => {
              console.log('companyData::', companyData);
              companyList.push(companyData)
            })
    
        });
    
        */

    (async () => {
      for await (const userCompany of companyDataList) {
        let compDetails = await getRepository(CompanyDetails)
          .createQueryBuilder('compDetails')
          .leftJoinAndSelect('compDetails.state', 'state')
          .leftJoinAndSelect('compDetails.industryType', 'industryType')
          .leftJoinAndSelect('compDetails.country', 'country')
          .leftJoinAndSelect('compDetails.theme', 'theme')
          .where('compDetails.Id = :companyId', {
            companyId: userCompany.CompanyId,
          })
          // .printSql()
          .getOne()
          .then((companyData) => {
            console.log('companyData::', companyData);
            if (undefined != companyData) {
              var data: { [k: string]: any } = {};
              data.CompanyName = companyData.CompanyName;
              data.Country = companyData.country;
              data.CountryId = companyData.CountryId;
              data.IndustryType = companyData.industryType;
              data.IndustryTypeId = companyData.IndustryTypeId;
              data.CompanyId = companyData.Id;
              data.State = companyData.state;
              data.StateId = companyData.StateId;
              data.Theme = companyData.theme;
              data.ThemeId = companyData.ThemeId;
              data.ImageId = companyData.ImageId;
              data.BackgroundimageId = companyData.BackgroundImageId;

              //To get billing address
              (async () => {
                let billingAddress = await this.getAddress(
                  companyData.Id,
                  1,
                ).then((result) => {
                  if (null != result || undefined != result) {
                    let companyBillingAddress: CompanyBillingAddress =
                      new CompanyBillingAddress();
                    companyBillingAddress.AddressLine1 = result.AddressLine1;
                    (companyBillingAddress.AddressLine2 = result.AddressLine2),
                      (companyBillingAddress.City = result.City),
                      (companyBillingAddress.CorrespondanceEmail =
                        result.CorrespondanceEmail),
                      (companyBillingAddress.PointOfContactMobileNumber =
                        result.PointOfContactMobileNumber),
                      (companyBillingAddress.PointOfContactName =
                        result.PointOfContactName),
                      (companyBillingAddress.ZipCode = result.ZipCode),
                      (companyBillingAddress.AlternativeEmail =
                        result.AlternativeEmail),
                      (companyBillingAddress.CountryId = result.CountryId),
                      (companyBillingAddress.Country =
                        companyData.country.name),
                      (companyBillingAddress.CountryCode =
                        result.countryCode.Code),
                      (companyBillingAddress.CountryCodeId =
                        result.CountryCodeId),
                      (companyBillingAddress.LocationType =
                        result.LocationType),
                      (companyBillingAddress.State = result.state.Name),
                      (companyBillingAddress.StateId = result.StateId);
                    data.billingAddress = companyBillingAddress;
                    console.log(
                      'COMPANY BILLING ADDRESS ::',
                      data.billingAddress,
                    );
                  }
                });
              })();
              //To get other Address
              (async () => {
                let billingAddress = await this.getAddress(companyData.Id, 2)
                  .then((result) => {
                    if (null != result || undefined != result) {
                      console.log(
                        '********    getAddress **************',
                        result,
                      );
                      let otherAddrList: Array<CompanyOtherAddress> = [];
                      result.forEach((element) => {
                        let cmpOthAddress: CompanyOtherAddress =
                          new CompanyOtherAddress();
                        cmpOthAddress.AddressLine1 = element.AddressLine1;
                        cmpOthAddress.AddressLine2 = element.AddressLine2;
                        cmpOthAddress.City = element.City;
                        cmpOthAddress.CorrespondanceEmail =
                          element.CorrespondanceEmail;
                        cmpOthAddress.PointOfContactMobileNumber =
                          element.PointOfContactMobileNumber;
                        cmpOthAddress.PointOfContactName =
                          element.PointOfContactName;
                        cmpOthAddress.ZipCode = element.ZipCode;
                        otherAddrList.push(cmpOthAddress);
                      });
                      data.otherAddress = otherAddrList;
                      console.log(
                        'COMPANY OTHER ADDRESS ::',
                        data.otherAddress,
                      );
                    }
                  })
                  .finally(() => {
                    companyList.push(data);
                    console.log(
                      'Insinde function companyList===>>::',
                      companyList,
                    );
                  });
                // console.log("COMPANY OTHER ADDRESS ::", billingAddress);
              })();
            }
          });
      }
    })().finally(() => {
      console.log('Final companyList.length::', companyList.length);
      return companyList;
    });

    // console.log('companyList===>>::', companyList);
    return null;
  }

  private async getAddress(
    companyDataId: number,
    addressType: number,
  ): Promise<any> {
    if (addressType == 1) {
      console.log('getBillingAddress :: companyDataId :::', companyDataId);
      return await getRepository(CompanyAddress)
        .createQueryBuilder('compBillAddr')
        .leftJoinAndSelect('compBillAddr.countryCode', 'countryCode')
        .leftJoinAndSelect('compBillAddr.state', 'state')
        .leftJoinAndSelect('compBillAddr.country', 'country')
        .where(
          'compBillAddr.CompanyId = :companyId AND compBillAddr.AddressTypeId = :addressTypeId',
          { companyId: companyDataId, addressTypeId: 1 },
        )
        .getOne()
        .then((result) => {
          // console.log("COMPANY BILL ADDRESS ::", result);
          return result;
        });
    } else if (addressType == 2) {
      console.log('getOtherAddress :: companyDataId :::', companyDataId);
      return await getRepository(CompanyAddress)
        .createQueryBuilder('compBillAddr')
        .leftJoinAndSelect('compBillAddr.countryCode', 'countryCode')
        .leftJoinAndSelect('compBillAddr.state', 'state')
        .leftJoinAndSelect('compBillAddr.country', 'country')
        .leftJoinAndSelect('compBillAddr.timeZone', 'timeZone')
        .where(
          'compBillAddr.CompanyId = :companyId AND compBillAddr.AddressTypeId = :addressTypeId',
          { companyId: companyDataId, addressTypeId: 2 },
        )
        .getMany()
        .then((result) => {
          // console.log("COMPANY BILL ADDRESS ::", result);
          return result;
        });
    }
  }

  private sampleJoin() {
    /*
    var userInfo = await getRepository(User).createQueryBuilder('user')
      // .select(["user.Id", "user.LastName", "user.FirstName", "user.StatusId", "designation.Name", "roles.Id", "roles.Name"])
      .leftJoinAndSelect("user.designation", "designation")
      .leftJoinAndSelect("user.roles", "roles")
      .leftJoinAndSelect("user.status", "status")
      .leftJoinAndSelect("user.country", "country")
      .leftJoinAndSelect("user.userCompany", "userCompany")
      .leftJoinAndSelect("user.companyAddress", "companyAddress")
      // .where("user.FirstName = :FirstName", { FirstName: "admin" })
      .where("user.Id = :userId", { userId: userId })
      // .andWhere("user.FirstName = :FirstName", { FirstName: "admin" })
      .printSql()
      .getOne()
    // .getRawAndEntities();
    console.log('userInfo::', userInfo);
    let userCompleteInfo: UserCompleteInformationViewModel = new UserCompleteInformationViewModel();
    userCompleteInfo.Id = userInfo.Id,
      userCompleteInfo.UserName = userInfo.UserName,
      userCompleteInfo.FirstName = userInfo.FirstName,
      userCompleteInfo.LastName = userInfo.LastName,
      userCompleteInfo.DateOfBirth = userInfo.DateOfBirth,
      userCompleteInfo.PhoneNumber = userInfo.PhoneNumber,
      userCompleteInfo.Status = userInfo.status.Name,
      userCompleteInfo.StatusId = userInfo.StatusId,
      userCompleteInfo.Email = userInfo.Email,
      userCompleteInfo.Image = userInfo.Image,
      userCompleteInfo.DesignationId = userInfo.DesignationId,
      userCompleteInfo.Designation = userInfo.designation.Name,
      userCompleteInfo.RoleId = userInfo.roles.Id,
      userCompleteInfo.Role = userInfo.roles.Name,
      userCompleteInfo.CountryCodeId = userInfo.country.Id,
      userCompleteInfo.CountryCode = userInfo.country.Code,
      userCompleteInfo.IsPrimaryContact = userInfo.IsPrimaryContact,
      userCompleteInfo.LocationType = userInfo.companyAddress.LocationType,
      userCompleteInfo.LocationTypeId = userInfo.LocationTypeId
    console.log('UserCompleteInformationViewModel::', userCompleteInfo);
    */
    // console.log('Designation::', userInfo.entities[0].designation);
  }
}
