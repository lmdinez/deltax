import { CountryCode } from 'src/esg/common/entity/countryCode.entity';
import { CompanyAddress } from 'src/esg/company/entity/company.address.entity';
import { UserCompany } from 'src/esg/company/entity/user.company.entity';
import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Designations } from './designations.entity';
import { Roles } from './roles.entity';
import { Status } from './status.entity';
import * as bcrypt from 'bcrypt';

@Entity({ name: 'user' })
export class UserRegister {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'email' })
  email: string;
  @Column({ name: 'enc_password' })
  enc_password: string;
  @Column({ name: 'company_id' })
  company_id: number;
  @Column({ name: 'first_name' })
  first_name: string;
  @Column({ name: 'last_name' })
  last_name: string;
  @Column({ name: 'phone_number' })
  phone_number: string;
  @Column({ name: 'is_active' })
  is_active: boolean;
  @Column({ name: 'created_on' })
  created_on: Date | null;
  @Column({ name: 'updated_on' })
  updated_on: Date | null;
  @Column({ name: 'role_id' })
  role_id: number;
  @Column({ name: 'location_id' })
  location_id: number;
}
