import { CountryCode } from 'src/esg/common/entity/countryCode.entity';
import { CompanyAddress } from 'src/esg/company/entity/company.address.entity';
import { UserCompany } from 'src/esg/company/entity/user.company.entity';
import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Designations } from './designations.entity';
import { Roles } from './roles.entity';
import { Status } from './status.entity';
import * as bcrypt from 'bcrypt';

@Entity({ name: 'AspNetUsers' })
export class User {
  @PrimaryGeneratedColumn()
  Id: number;

  @Column({ name: 'UserName' })
  UserName: string;

  @Column({ name: 'FirstName' })
  FirstName: string;

  @Column({ name: 'LastName' })
  LastName: string;
  @Column({ name: 'DateOfBirth' })
  DateOfBirth: string | null;

  @Column({ name: 'PhoneNumber' })
  PhoneNumber: string;

  @OneToOne(() => CountryCode)
  @JoinColumn({ name: 'CountryCodeId' })
  country: CountryCode;

  // @Column({ name: 'CountryCodeId' })
  // CountryCodeId: number;

  @Column({ name: 'Image' })
  Image: string;
  @Column({ name: 'LastLoginTime' })
  LastLoginTime: Date;
  @Column({ name: 'IsActiveLogin' })
  IsActiveLogin: boolean | null;

  @OneToOne(() => Designations)
  @JoinColumn()
  designation: Designations;

  @Column({ name: 'Email' })
  Email: string;

  // @Column({ name: 'StatusId' })
  // StatusId: number;

  @Column({ name: 'EmailConfirmed' })
  EmailConfirmed: boolean;

  @Column({ name: 'PhoneNumberConfirmed' })
  PhoneNumberConfirmed: boolean;

  @Column({ name: 'TwoFactorEnabled' })
  TwoFactorEnabled: boolean;

  @Column({ name: 'LockoutEnabled' })
  LockoutEnabled: boolean;

  @Column({ name: 'AccessFailedCount' })
  AccessFailedCount: number;

  @OneToOne(() => Status)
  @JoinColumn()
  status: Status;

  @Column({ name: 'IsDeleted' })
  IsDeleted: boolean;
  @Column({ name: 'CreatedBy' })
  CreatedBy: number;
  @Column({ name: 'CreatedDate' })
  CreatedDate: Date;
  @Column({ name: 'UpdatedBy' })
  UpdatedBy: number | null;
  @Column({ name: 'UpdatedDate' })
  UpdatedDate: Date | null;
  @Column({ name: 'LocationTypeId' })
  LocationTypeId: number;
  @Column({ name: 'IsPrimaryContact' })
  IsPrimaryContact: boolean;

  @Column({ name: 'PasswordHash' })
  password: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 15);
    console.log(`Password : ${this.password}`);
  }

  //Many user to Many roles
  @ManyToMany(() => Roles, (role) => role.users)
  //intermediate table to join the column
  @JoinTable({
    name: 'AspNetUserRoles',
    joinColumns: [{ name: 'UserId' }],
    inverseJoinColumns: [{ name: 'RoleId' }],
  })
  roles: Roles[];

  // @OneToOne(() => UserCompany)
  // @JoinColumn({ name: 'Id', referencedColumnName: "UserId" })
  // userCompany: UserCompany;

  @OneToMany(() => UserCompany, (userCompany) => userCompany.user)
  @JoinColumn({ name: 'Id', referencedColumnName: 'UserId' })
  userCompany: UserCompany[];

  @OneToOne(() => CompanyAddress)
  @JoinColumn({ name: 'LocationTypeId', referencedColumnName: 'Id' })
  companyAddress: CompanyAddress;
}
