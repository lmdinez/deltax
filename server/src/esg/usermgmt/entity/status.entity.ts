import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity({ name: 'Status' })
export class Status {
  @PrimaryColumn()
  Id: number;
  @Column({ name: 'Name' })
  Name: string;
}
