import { Entity, ManyToOne, OneToMany, PrimaryColumn } from "typeorm"
import { User } from "./user.entity";

@Entity({ name: 'AspNetUserRoles' })
export class UserRoles {
  @PrimaryColumn({ name: 'UserId' })
  UserId: number;
  @PrimaryColumn({ name: 'RoleId' })
  RoleId: number;

}
