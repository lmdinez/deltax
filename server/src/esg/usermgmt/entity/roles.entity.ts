import { Column, Entity, JoinTable, ManyToMany, PrimaryColumn } from "typeorm"
import { User } from "./user.entity";

@Entity({ name: 'AspNetRoles' })
export class Roles {
  @PrimaryColumn({ name: 'Id' })
  Id: number;
  @Column({ name: 'Name' })
  Name: string;
  @Column({ name: 'NormalizedName' })
  NormalizedName: string;
  @Column({ name: 'ConcurrencyStamp' })
  ConcurrencyStamp: string;
  @Column({ name: 'DesignationId' })
  DesignationId: number;

  //Many roles to Many users
  @ManyToMany(() => User, user => user.roles)
  //intermediate table to join the column
  // @JoinTable({
  //   name: 'AspNetUserRoles',
  //   joinColumns: [{ name: 'RoleId' }],
  //   inverseJoinColumns: [{ name: 'UserId' }]
  // })
  @JoinTable({
    name: 'AspNetUserRoles',
    joinColumns: [{ name: 'UserId' }],
    inverseJoinColumns: [{ name: 'RoleId' }]
  })
  users: User;

}
