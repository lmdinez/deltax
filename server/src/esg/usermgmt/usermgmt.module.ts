import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompanyAddress } from '../company/entity/company.address.entity';
import { UserCompany } from '../company/entity/user.company.entity';
import { Designations } from './entity/designations.entity';
import { Roles } from './entity/roles.entity';
import { Status } from './entity/status.entity';
import { User } from './entity/user.entity';
import { UserRoles } from './entity/user.roles.entity';
import { UserManagementController } from './user.management.controller';
import { UserManagementService } from './user.management.service';

import { CountryCode } from '../common/entity/countryCode.entity';
import { State } from '../common/entity/state.entity';
import { Country } from '../common/entity/country.entity';
import { IndustryType } from '../common/entity/industryType.entity';
import { UserRegister } from './entity/registerUser.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      UserRegister,
      UserRoles,
      Roles,
      CountryCode,
      Status,
      Designations,
      CompanyAddress,
      UserCompany,
      IndustryType,
      State,
      Country,
    ]),
  ],
  providers: [UserManagementService],
  controllers: [UserManagementController],
})
export class UserMgmtModule {}
