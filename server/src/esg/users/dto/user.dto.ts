import { IsNotEmpty, IsEmail } from 'class-validator';

export class UserDto {
  @IsNotEmpty()
  Id: number;

  @IsNotEmpty()
  UserName: string;

  @IsNotEmpty()
  @IsEmail()
  Email: string;

  CreatedDate?: Date;

  @IsNotEmpty()
  password?: string;
}

export class UserCredentialsViewModel {
  UserName: string;
  Password: string;
  Designation: string;
  DesignationId: number;
}

export class TwoFactorAuthenticationModel {
  userName: string;
  otpCode: string;
}

export class UserChangePasswordViewModel {
  OldPassword: string;
  NewPassword: string;
  ConfirmPassword: string;
}

export class UserCodeViewModel {
  Code: string;
  UserId: string;
}

export class UserForgotPasswordViewModel {
  UserId: string;
  Code: string;
  NewPassword: string;
}
