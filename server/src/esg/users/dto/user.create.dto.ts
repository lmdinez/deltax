import { IsNotEmpty, IsEmail, IsMobilePhone } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password?: string;

  @IsNotEmpty()
  firstName: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  @IsMobilePhone()
  phoneNumber?: string;
  @IsNotEmpty()
  role_id: number;
}
// @PrimaryGeneratedColumn()
//   id: number;
//   email: string;
//   enc_password;
//   company_id;
//   first_name;
//   last_name;
//   phone_number;
//   is_active;
//   created_on;
//   updated_on;
//   role_id;
//   location_id;
