import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';
import { UserCredentialsViewModel, UserDto } from './dto/user.dto';
import { CreateUserDto } from './dto/user.create.dto';
import { LoginUserDto } from './dto/user-login.dto';
import { toUserDto } from '../shared/mapper';
import { comparePasswords, encryptPassowrd } from '../shared/utils';
import { User } from '../usermgmt/entity/user.entity';
import { RegistrationStatus } from '../auth/interface/registration.status';
import { Designations } from './entity/designation.entity';
import { CountryCode } from '../common/entity/countryCode.entity';
import { Status } from '../usermgmt/entity/status.entity';
import { UserViewModel } from '../model/user.view.model';
import { TwoFactorAuthenticationService } from '../auth/2f.authentication.service';
import { LoginStatus } from '../auth/interface/login.status';
import { UserRegister } from '../usermgmt/entity/registerUser.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRegister)
    private userRepoRegister: Repository<UserRegister>,
    @InjectRepository(User)
    private readonly userRepo: Repository<User>,
    @InjectRepository(Designations)
    private readonly designationRepo: Repository<Designations>,
  ) { }

  async findOne(options?: object): Promise<UserDto> {
    const user = await this.userRepo.findOne(options);
    return toUserDto(user);
  }

  async authenticateUser({
    userName,
    password,
  }: LoginUserDto): Promise<LoginStatus> {
    console.log("authenticateUser", userName, password);

    const user = await this.userRepoRegister.findOne({
      where: { email: userName },
    });

    console.log("user by id", user);

    // const user = await getRepository(UserRegister)
    //   .createQueryBuilder('user')
    //   .leftJoinAndSelect('user.roles', 'roles')
    //   .leftJoinAndSelect('user.designation', 'designation')
    //   .where('user.UserName = : ', { userName: UserName })
    //   .getOne();

    if (!user) {
      throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
    }
    // compare passwords
    const areEqual = await comparePasswords(user.enc_password, password);
    console.log('areEqual::', areEqual);
    if (!areEqual) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
    // return toUserDto(user);
    return await this.converLoginStatus(user);
  }

  async findByLoginUser(email: string): Promise<LoginStatus> {
    const user = await this.userRepoRegister.findOne({
      where: { email: email },
    });
    // const user = await getRepository(UserRegister)
    //   .createQueryBuilder('user')
    //   .leftJoinAndSelect('user.roles', 'roles')
    //   .leftJoinAndSelect('user.designation', 'designation')
    //   .where('user.UserName = :userName', { userName: userName })
    //   .getOne();
    if (!user) {
      throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
    }
    return this.converLoginStatus(user);
  }

  private async converLoginStatus(user: UserRegister): Promise<LoginStatus> {
    console.log('User::', user);
    let loginStatus: any = {
      userName: user.first_name,
      accessToken: null,
      expiresIn: null,
      email: user.email,
      firstName: user.first_name,
      lastName: user.last_name,
      image: '',
      userRole: user.role_id,
      designation: null,
      companyId: user.company_id,
      userStatus: user.is_active,
      accountModule: 0,
      theme: 'green-theme',
    };
    return loginStatus;
  }

  async findByLoginReg({
    UserName,
    Password,
    Designation,
    DesignationId,
  }: UserCredentialsViewModel): Promise<string> {
    const esgDesignation = await this.designationRepo.findOne({
      where: { 'LOWER(Name)': UserName.toLowerCase() },
    });
    let verifiedUser = new UserViewModel();

    // var error = new DataValidationException();
    if (null != esgDesignation) {
      const userToVerify = await this.userRepo.findOne({
        where: { UserName: UserName },
      });
      if (userToVerify.status.Id == 2) {
        // error.Errors.Add("InActive user", "User Not found");
        // return verifiedUser;
      }
      if (userToVerify == null) {
        // error.Errors.Add("UserNotAuthenticate", "User Not found");
        // return verifiedUser;
        //throw error;
      }
      if (esgDesignation.Id == userToVerify.designation.Id) {
        // var identityResult = await _userManager.CheckPasswordAsync(userToVerify, model.Password);
        let identityResult = await comparePasswords(
          userToVerify.password,
          Password,
        );
        var userRoles = userToVerify.roles;

        if (identityResult) {
          userToVerify.LastLoginTime = new Date();
          // userToVerify.TwoFactorEnabled = true;
          await this.userRepo.save(userToVerify);

          let userModel: UserViewModel = new UserViewModel();
          userModel.Id = userToVerify.Id;
          userModel.UserName = userToVerify.UserName;
          userModel.FirstName = userToVerify.FirstName;
          userModel.LastName = userToVerify.LastName;
          userModel.DateOfBirth = userToVerify.DateOfBirth;
          userModel.Email = userToVerify.Email;
          userModel.Image = userToVerify.Image;
          userModel.Roles = userRoles.map((role) => role.Name).join(',');
          if (userToVerify.TwoFactorEnabled) {
            // AuthToken = await _userManager.GenerateTwoFactorTokenAsync(userToVerify, "Email")
          }
          // return userToVerify;
        } else {
          // throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
          // error.Errors.Add("UserPassword", "User Password not correct.");
          // return verifiedUser;
        }
      }
    } else {
      // error.Errors.Add("UnAuthorized", "Invalid User.");
      // return verifiedUser;
      //throw error;
    }

    return '';

    /*
    
        const user = await this.userRepo.findOne({ where: { UserName: username } });
    
        if (!user) {
          throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
        }
    
        // compare passwords
        const areEqual = await comparePasswords(user.password, password);
        console.log('areEqual::', areEqual);
        if (!areEqual) {
          throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
        }
    
        return toUserDto(user);
    
        */
  }

  async findByPayload(payload: any): Promise<User> {
    console.log('payload.UserName::', payload.UserName);
    // console.log('UserName::', payload.);
    return await this.userRepo.findOne({
      where: { UserName: payload.UserName },
    });
  }

  async create(userDto: CreateUserDto): Promise<RegistrationStatus> {
    const _userDto: CreateUserDto = userDto;
    var encryptedPassword: any;

    // check if the user exists in the db

    const userInDb = await this.userRepoRegister.findOne({
      where: { email: _userDto.email },
    });
    if (userInDb) {
      throw {
        status: 'ALREADYREGISTERED',
        message: `User entry available`,
      };
      // throw new HttpException('ALREADYREGISTERED', HttpStatus.BAD_REQUEST);
    }
    // if (userDto.password) {

    // Encryption of password
    const saltRounds = 10;
    let salt = await bcrypt.genSalt(saltRounds);
    encryptedPassword = await bcrypt.hash(_userDto.password, salt);

    // encryptedPassword = await encryptPassowrd(_userDto.password);
    // }
    console.log('User not exist', encryptedPassword, _userDto);
    const newUser: UserRegister = new UserRegister();
    newUser.email = _userDto.email;
    // newUser.Email = _userDto.email;
    newUser.first_name = _userDto.firstName;
    newUser.last_name = _userDto.lastName;
    newUser.phone_number = _userDto.phoneNumber;
    newUser.enc_password = encryptedPassword;
    newUser.created_on = new Date();
    newUser.updated_on = new Date();
    newUser.role_id = _userDto.role_id;
    // newUser.LastLoginTime = new Date();
    // newUser.IsDeleted = false;
    // newUser.CreatedBy = 24;
    // newUser.UpdatedBy = 24;

    // let countryCodeObj = new CountryCode();
    // countryCodeObj.Id = 2;
    // countryCodeObj.Code = '+1-907';
    // countryCodeObj.CountryId = 2;

    // let designation = new Designations();
    // designation.Id = 1;
    // designation.Name = 'consultant';

    // let status = new Status();
    // status.Id = 1;
    // status.Name = 'Active';

    // newUser.country = countryCodeObj;
    // newUser.designation = designation;
    // newUser.EmailConfirmed = false;
    // newUser.PhoneNumberConfirmed = false;
    // newUser.TwoFactorEnabled = false;
    // newUser.LockoutEnabled = false;
    // newUser.IsActiveLogin = false;
    // newUser.IsPrimaryContact = true;
    // newUser.status = status;
    // newUser.AccessFailedCount = 0;

    const result = await this.userRepoRegister.insert(newUser);
    console.log(`result ${result}`);

    return {
      status: 'SUCCESS',
      message: `Uses Created ${result.raw.id}`,
    };
  }

  private _sanitizeUser(user: User) {
    delete user.password;
    return user;
  }
}
