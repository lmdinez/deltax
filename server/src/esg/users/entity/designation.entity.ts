import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity({ name: 'Designations' })
export class Designations {
  @PrimaryColumn()
  Id: number;
  @Column({ name: 'UserName' })
  Name: string;
}
