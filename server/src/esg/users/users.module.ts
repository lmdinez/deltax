import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from '../usermgmt/entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Designations } from './entity/designation.entity';
import { TwoFactorAuthenticationService } from '../auth/2f.authentication.service';
import { MfaDetails } from '../auth/mfa.details.entity';
import { Roles } from '../usermgmt/entity/roles.entity';
import { UserRoles } from '../usermgmt/entity/user.roles.entity';
import { Status } from '../usermgmt/entity/status.entity';
import { UserRegister } from '../usermgmt/entity/registerUser.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      UserRegister,
      Designations,
      MfaDetails,
      UserRoles,
      Roles,
    ]),
  ],
  controllers: [],
  providers: [UsersService, TwoFactorAuthenticationService],
  exports: [UsersService],
})
export class UsersModule {}
