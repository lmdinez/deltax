import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'industry_type' })
export class IndustryType {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ name: 'type' })
    type: string;
    @Column({ name: 'is_active' })
    is_active: number;
    @Column({ name: 'created_on' })
    created_on: Date;
}
