import { CompanyLocation } from "src/esg/company/entity/company.locations.entity";
import { CompanyAddresss } from "src/esg/company/entity/company_address.entity";
import { Column, Entity, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'city' })
export class City {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'name' })
  name: string;
  @Column({ name: 'active' })
  active: number;
  @Column({ name: 'state_id' })
  state_id: number;

  @OneToOne(() => CompanyLocation, companylocation => companylocation.city_id)
  companyLocation: CompanyLocation;

  @OneToOne(() => CompanyAddresss, companyAddress => companyAddress.city_id)
  company_address: CompanyAddresss;
}
