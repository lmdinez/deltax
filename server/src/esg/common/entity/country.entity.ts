import { Company } from "src/esg/company/entity/company.entity";
import { CompanyLocation } from "src/esg/company/entity/company.locations.entity";
import { CompanyAddresss } from "src/esg/company/entity/company_address.entity";
import { Column, Entity, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'country' })
export class Country {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ name: 'name' })
    name: string;
    @Column({ name: 'code' })
    code: string;
    @Column({ name: 'active' })
    active: number;
    @Column({ name: 'phonecode' })
    phonecode: number;

    @OneToOne(() => CompanyLocation, companylocation => companylocation.country_id)
    companyLocation: CompanyLocation;

    @OneToOne(() => Company, company => company.country_id)
    company: Company;

    @OneToOne(() => CompanyAddresss, companyAddress => companyAddress.country_id)
    company_address: CompanyAddresss;
}
