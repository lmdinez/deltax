import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
//import { Country } from './country.entity';

@Entity({ name: 'CountryCode' })
export class CountryCode {
    @PrimaryGeneratedColumn()
    Id: number;

    @Column({ name: 'CountryId' })
    CountryId: number;

    @Column({ name: 'Code' })
    Code: string;

    // @OneToOne(() => Country)
    // @JoinColumn({ name: 'CountryId', referencedColumnName: 'Id' })
    // country: Country;
}
