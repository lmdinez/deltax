import { Company } from "src/esg/company/entity/company.entity";
import { CompanyLocation } from "src/esg/company/entity/company.locations.entity";
import { CompanyAddresss } from "src/esg/company/entity/company_address.entity";
import { Column, Entity, OneToOne, PrimaryColumn } from "typeorm";

@Entity({ name: 'state' })
export class State {
    @PrimaryColumn()
    id: number;
    @Column({ name: 'name' })
    name: string;
    @Column({ name: 'active' })
    active: string;
    @Column({ name: 'country_id' })
    country_id: number;

    @OneToOne(() => CompanyLocation, companylocation => companylocation.state_id)
    companyLocation: CompanyLocation;

    @OneToOne(() => Company, company => company.state_id)
    company: Company;

    @OneToOne(() => CompanyAddresss, companyAddress => companyAddress.state_id)
    company_address: CompanyAddresss;

}
