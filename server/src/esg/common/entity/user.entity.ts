import { BaseEntity, Column, CreateDateColumn, Entity, Generated, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Company } from "src/esg/company/entity/company.entity";
import { CompanyLocation } from "src/esg/company/entity/company.locations.entity";
import { Role } from "./role.entity";


@Entity('user')
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 255,
    nullable: false
  })
  email: string

  @Column({
    type: 'varchar',
    length: 100,
    nullable: false
  })
  enc_password: string


  @OneToOne(() => Company, company => company.user)
  @JoinColumn({ name: 'company_id' })
  company_id: Company;


  @Column({
    type: 'varchar',
    length: 100,
    nullable: false
  })
  first_name: string


  @Column({
    type: 'varchar',
    length: 100,
    nullable: false
  })
  last_name: string



  @Column({
    type: 'varchar',
    length: 20,
    nullable: true
  })
  phone_number: string

  @Column({
    type: 'varchar',
    length: 255,
    nullable: true
  })
  designation: string

  @Column({
    type: 'tinyint',
    nullable: false
  })
  is_active: number

  @CreateDateColumn()
  created_on: string


  @UpdateDateColumn()
  updated_on: string

  @Column()
  location_id: number

  @Column()
  role_id: number

  // @OneToOne(() => CompanyLocation, companyLocation => companyLocation.user)
  // @JoinColumn({ name: 'location_id' })
  // location_id: CompanyLocation;


  // @OneToOne(() => Role, role => role.user)
  // @JoinColumn({ name: 'role_id' })
  // role_id: Role;


}
