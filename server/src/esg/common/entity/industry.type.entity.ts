import { Company } from "src/esg/company/entity/company.entity";
import { Column, Entity, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'industry_type' })
export class IndustryType {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'type' })
  type: string;
  @Column({ name: 'is_active' })
  is_active: number;
  @Column({ name: 'created_on' })
  created_on: Date;

  @OneToOne(() => Company, company => company.industry_type_id)
  company: Company;
}
