
import { CompanyLocation } from "src/esg/company/entity/company.locations.entity";
import { BaseEntity, Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity('timezone')
export class Timezone extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 100,
    nullable: false
  })
  name: string

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false
  })
  code: string

  @OneToOne(() => CompanyLocation, companylocation => companylocation.timezone_id)
  companyLocation: CompanyLocation;
}
