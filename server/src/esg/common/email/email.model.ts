export class EmailModel {
  Id: number;
  ToEmail: string;
  Subject: string;
  Body: string;
  EmailStatus: number | null;
  Seperator: string = ",";
}
