import { Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity({ name: 'EmailQueues' })
export class EmailQueue {
  @PrimaryGeneratedColumn()
  Id: number;
  UserId: number;
  ToEmail: string;
  IsActive: boolean;
  CCEmail: string;
  BCCEmail: string;
  Subject: string;
  Body: string;
  QueueTime: string | null;
  EmailStatus: number | null;
  SentTime: string | null;
  SmtpServer: string;
  SmtpPort: number | null;
  FromAddress: string;
  UserName: string;
  Retries: number;
  Password: string;
  DisplayName: string;
  Seperator: string;
  ErrorDescription: string;
  CreateDate: string;
  ModifiedDate: string | null;

}
