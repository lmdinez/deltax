import { HttpService } from '@nestjs/axios';
import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { catchError, map, Observable, throwError } from 'rxjs';
import { ZeptoMailQuery, ZeptoMailRequest, ZeptoMailResponseSuccess, ZeptoMailConfig, ZEPTO_MAIL_CONFIG, ZeptoMailValidationError } from './zeptomail.type';

@Injectable()
export class ZeptoMailClientService {
  private debug: boolean;
  private token: string;
  private commonURL: string;
  private isUrl: boolean;
  private options: ZeptoMailConfig;

  constructor(
    @Inject(ZEPTO_MAIL_CONFIG) options: ZeptoMailConfig,
    private httpService: HttpService
  ) {
    const token = options.token;
    const url = options.url ?? '';
    const domain = options.domain ?? '';

    this.debug = !!options.debug;
    this.commonURL = `api.zeptomail.${domain || 'com'}/`;

    if (!token) {
      throw new Error('Send Mail token cannot be empty.');
    } else if (!url && !domain) {
      throw new Error('Provide a valid URL or a domain.');
    }

    this.token = token;
    this.isUrl = url && url.indexOf('/v1.1') > -1;
    const defaultOption: Partial<ZeptoMailConfig> = {
      host: url,
      isUrlAPI: url && url.indexOf('/v1.1') === -1,
      useHttps: url.includes('https://'),
      authHeader: 'Zoho-enczapikey'
    };
    this.options = { ...defaultOption, ...options };
  }

  private getUrl(url: string): string {
    const scheme = this.options.useHttps ? '' : 'https://';
    var host = this.isUrl ? this.options.host : `${this.options.isUrlAPI ? this.options.host : this.commonURL}${url}`;
    return `${scheme}${host}`;
  }

  private getHeader(): {
    Authorization: string;
  } {
    return {
      Authorization: `${this.options.authHeader} ${this.token}`
    };
  }

  private validate(query: ZeptoMailQuery): ZeptoMailValidationError {
    const options = query.options;
    const isTemplate = query.isTemplate;

    let valid = true;
    let errorText = '';

    if (!options.to) {
      errorText = 'To address cannot be empty.';
      valid = false;
    } else if (!options.from) {
      errorText = 'From address cannot be empty.';
      valid = false;
    } else if (!options.bounce_address) {
      errorText = 'Bounce address cannot be empty.';
      valid = false;
    } else if (!isTemplate && !options.subject) {
      errorText = 'Subject cannot be empty.';
      valid = false;
    } else if (isTemplate && !options.mail_template_key) {
      errorText = 'Provide a valid template key.';
      valid = false;
    }

    return {
      valid,
      errorText
    };
  }

  private resultWithBody(method: 'POST', query: ZeptoMailQuery, url: string): Observable<ZeptoMailResponseSuccess> {
    const v = this.validate(query);
    if (v.valid) {
      return this.httpService.request<ZeptoMailResponseSuccess>({
        url: this.getUrl(url),
        method,
        headers: this.getHeader(),
        responseType: 'json',
        data: query.options
      }).pipe(
        map(resp => {
          if (resp.status === HttpStatus.OK) {
            return resp.data;
          }
          throw new Error('Invalid response');
        }),
        catchError(error => {
          if (this.debug) {
            console.log(error);
          }

          return throwError(() => error);
        })
      );
    }

    return throwError(() => v.errorText);
  }

  public sendMail(options: ZeptoMailRequest): Observable<ZeptoMailResponseSuccess> {
    if (this.debug) {
      console.log('send mail', this.token);
    }

    return this.resultWithBody('POST', {
      options
    }, 'v1.1/email');
  }

  public sendMailWithTemplate(options: ZeptoMailRequest): Observable<ZeptoMailResponseSuccess> {
    return this.resultWithBody('POST', {
      options,
      isTemplate: true
    }, 'v1.1/email/template');
  }

  public sendMailBatchWithTemplate(options: ZeptoMailRequest): Observable<ZeptoMailResponseSuccess> {
    return this.resultWithBody('POST', {
      options,
      isTemplate: true
    }, 'v1.1/email/template/batch');
  }
}
