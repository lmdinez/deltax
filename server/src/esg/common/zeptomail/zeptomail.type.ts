export const ZEPTO_MAIL_CONFIG = 'ZEPTO_MAIL_CONFIG';

export interface ZeptoMailConfig {
  token: string;
  url?: string;
  debug?: boolean;
  domain?: string;
  host?: string;
  isUrlAPI?: boolean;
  useHttps?: boolean;
  authHeader?: string;
}

export interface ZeptoMailAddress {
  address: string;
  name: string;
}

export interface IKeyValuePair<T> {
  [k: string]: T;
}

export interface ZeptoMailAttachment {
  content?: string;
  mime_type?: string;
  file_cache_key?: string;
  name: string;
}

export interface ZeptoMailInlineImage {
  mime_type: string;
  content: string;
  file_cache_key: string;
  cid: string;
}

export interface ZeptoMailTo {
  email_address: ZeptoMailAddress;
}

export interface ZeptoMailCC {
  email_address: ZeptoMailAddress;
}

export interface ZeptoMailBCC {
  email_address: ZeptoMailAddress;
}

export interface ZeptoMailRequest {
  bounce_address: string;
  from: ZeptoMailAddress;
  to: ZeptoMailTo[];
  reply_to?: ZeptoMailAddress[];
  subject: string;
  textbody?: string;
  htmlbody?: string;
  cc?: ZeptoMailCC[];
  bcc?: ZeptoMailBCC[];
  track_clicks?: boolean;
  track_opens?: boolean;
  client_reference?: string;
  mime_headers?: IKeyValuePair<string>;
  attachments?: ZeptoMailAttachment[];
  inline_images?: ZeptoMailInlineImage[];
  mail_template_key?: string;
}

export interface ZeptoMailResponseSuccessData {
  code: string;
  additional_info: any[];
  message: string;
}

export interface ZeptoMailResponseSuccess {
  data: ZeptoMailResponseSuccessData[];
  message: string;
  request_id: string;
  object: 'email';
}

export interface ZeptoMailResponseErrorErrorDetails {
  code: string;
  message: string;
  target: string;
}

export interface ZeptoMailResponseErrorInfo {
  code: string;
  details: ZeptoMailResponseErrorErrorDetails[];
  message: string;
  request_id: string;
}

export interface ZeptoMailResponseError {
  error: ZeptoMailResponseErrorInfo;
}

export interface ZeptoMailQuery {
  options: ZeptoMailRequest;
  isTemplate?: boolean;
}

export interface ZeptoMailValidationError {
  valid: boolean;
  errorText: string;
}
