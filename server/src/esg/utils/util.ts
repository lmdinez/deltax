import { Response, NextFunction } from 'express';
import { Request } from '@nestjs/common';

import { GlobalResponse } from '../model/response.model';
export class Utility {
  /*
  public static failure(message: string, responseCode: number): any {
    var res =
    {
      Messege: message,
      ResponseCode: responseCode,
      // Result = errors
    };
   return res;
  }
*/
  public static getUserById(@Request() request): number {
    return request.user == null ? 'INVALID USER' : request.user.Id;
    // return 24;
  }

  public static getUserId(): number {
    return 24;
  }

  public static getUserCompanyId(): number {
    return 19;
  }
  public static handleSuccess<T>(
    response: Response,
    message: string = 'Success',
    responseCode: number = 200,
  ): Response {
    let globalResponse: GlobalResponse = new GlobalResponse();
    globalResponse.status = message;
    globalResponse.message = responseCode;
    return response.status(responseCode).json(globalResponse);
  }

  public static handleSuccessWithResult<T>(
    response: Response,
    result: T,
    message: string = 'SUCCESS',
    responseCode: number = 200,
  ): Response {
    let globalResponse: GlobalResponse = new GlobalResponse();
    globalResponse.status = message;
    globalResponse.message = responseCode;
    globalResponse.result = result;
    return response.status(responseCode).json(globalResponse);
  }

  public static handleFailure<T>(
    response: Response,
    message: string = 'Failure',
    responseCode: number = 500,
  ): Response {
    let globalResponse: GlobalResponse = new GlobalResponse();
    globalResponse.status = message;
    globalResponse.message = responseCode;
    return response.status(responseCode).json(globalResponse);
  }

  public static handleFailureWithResult<T>(
    response: Response,
    result: T,
    message: string = 'Failure',
    responseCode: number = 400,
  ): Response {
    let globalResponse: GlobalResponse = new GlobalResponse();
    globalResponse.result = result;
    globalResponse.status = message;
    globalResponse.message = responseCode;
    return response.status(responseCode).json(globalResponse);
  }

  public static handleSuccessList<T>(
    response: Response,
    result: T,
    message: string = 'SUCCESS',
    responseCode: number = 200,
  ): Response {
    let globalResponse: GlobalResponse = new GlobalResponse();
    globalResponse.status = message;
    globalResponse.message = responseCode;
    globalResponse.result = result;
    return response.status(responseCode).json(globalResponse);
  }
}
