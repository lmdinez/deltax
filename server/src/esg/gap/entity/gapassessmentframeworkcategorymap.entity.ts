import { Column, Entity, Index, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
// import { Gap_Assessment_Framework } from './gapassessmentframework.entity';
// import { Gap_Assessment_Framework_Category } from './gapassessmentframeworkcategory.entity';


@Entity({ name: 'gap_assessment_framework_category_map' })
export class Gap_Assessment_Framework_Category_Map {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'category_id' })
  category_id: number;
  @Column({ name: 'framework_id' })
  framework_id: number;

  // @OneToOne(() => Gap_Assessment_Framework_Category)
  // @JoinColumn({ name: 'category_id', referencedColumnName: 'id' })
  // gapassessmentframeworkcategory: Gap_Assessment_Framework_Category;
  // @OneToOne(() => Gap_Assessment_Framework)
  // @JoinColumn({ name: 'category_id', referencedColumnName: 'id' })
  // gapassessmentframework: Gap_Assessment_Framework;
}
