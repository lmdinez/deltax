import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
// import { Gap_Assessment_Framework } from './gapassessmentframework.entity';


@Entity({ name: 'gap_assessment_share' })
export class Gap_Assessment_Share {

  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'assessment_id' })
  assessment_id: number;
  @Column({ name: 'shared_by' })
  shared_by: number;
  @Column({ name: 'shared_with' })
  shared_with: number;
  @Column({ name: 'share_date' })
  share_date: Date;
  @Column({ name: 'status' })
  status: string;


  // @OneToOne(() => User)
  // @JoinColumn({ name: 'shared_by', referencedColumnName: 'id' })
  // sharebyuser: User;
  // @OneToOne(() => User)
  // @JoinColumn({ name: 'shared_with', referencedColumnName: 'id' })
  // sharewithuser: User;
  // @OneToOne(() => Gap_Assessment_Framework)
  // @JoinColumn({ name: 'assessment_id', referencedColumnName: 'id' })
  // gapassessmentframework: Gap_Assessment_Framework;
}
