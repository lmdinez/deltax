
import { CompanyDetails } from 'src/esg/company/entity/company.details.entity';
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, OneToOne, PrimaryColumn } from "typeorm";


@Entity({ name: 'User' })
export class User {
  @PrimaryColumn()
  id: number;
  @Column({ name: 'username' })
  username: string;
  @Column({ name: 'enc_password' })
  enc_password: string;
  @Column({ name: 'company_id' })
  company_id: string;

  @OneToOne(() => CompanyDetails)
  @JoinColumn({ name: 'company_id', referencedColumnName: "Id" })
  companyDetails: CompanyDetails;


}
