import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";


@Entity({ name: 'gap_assessment_question' })
export class Gap_Assessment_Question {

  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'category_id' })
  category_id: number;
  @Column({ name: 'sub_category_id' })
  sub_category_id: number;
  @Column({ name: 'question_type_id' })
  question_type_id: number;
  @Column({ name: 'question' })
  question: string;
  @Column({ name: 'desired_response' })
  desired_response: number;
  @Column({ name: 'comment' })
  comment: string;
  @Column({ name: 'created_by' })
  created_by: number;
  @Column({ name: 'created_on' })
  created_on: Date;
  @Column({ name: 'updated_on' })
  updated_on: Date;

  // @OneToOne(() => User)
  // @JoinColumn({ name: 'created_by', referencedColumnName: 'id' })
  // user: User;
}
