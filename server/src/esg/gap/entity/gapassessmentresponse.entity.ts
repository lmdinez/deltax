import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
// import { Gap_Assessment_Question } from './gapassessmentquestion.entity';


@Entity({ name: 'gap_assessment_response' })
export class Gap_Assessment_Response {

  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'question_id' })
  question_id: number;
  @Column({ name: 'response' })
  response: number;
  @Column({ name: 'point' })
  point: number;
  @Column({ name: 'respose_by' })
  respose_by: number;



  // @OneToOne(() => User)
  // @JoinColumn({ name: 'respose_by', referencedColumnName: 'id' })
  // user: User;
  // @OneToOne(() => Gap_Assessment_Question)
  // @JoinColumn({ name: 'question_id', referencedColumnName: 'id' })
  // gapassessmentquestion: Gap_Assessment_Question;
}
