import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
// import { Gap_Assessment_Share } from './gapassessmentshare.entity';
// import { User } from './user.entity';


@Entity({ name: 'gap_assessment_framework' })
export class Gap_Assessment_Framework {

  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'framework_name' })
  framework_name: string;
  @Column({ name: 'description' })
  description: string;
  @Column({ name: 'created_by' })
  created_by: number;
  @Column({ name: 'is_master' })
  is_master: number;
  @Column({ name: 'created_on' })
  created_on: Date;
  @Column({ name: 'updated_on' })
  updated_on: Date;
  @Column({ name: 'due_date' })
  due_date: Date;

  // @OneToOne(() => User)
  // @JoinColumn({ name: 'created_by', referencedColumnName: 'id' })
  // user: User;
}
