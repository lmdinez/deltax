import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";


@Entity({ name: 'gap_assessment_framework_category' })
export class Gap_Assessment_Framework_Category {

  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'category_name' })
  category_name: string;
  @Column({ name: 'parent_id' })
  parent_id: number;
  @Column({ name: 'created_by' })
  created_by: number;
  @Column({ name: 'created_on' })
  created_on: Date;
  @Column({ name: 'updated_on' })
  updated_on: Date;

  // @OneToOne(() => User)
  // @JoinColumn({ name: 'created_by', referencedColumnName: 'id' })
  // user: User;
}
