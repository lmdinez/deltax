import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
// import { Gap_Assessment_Framework } from './gapassessmentframework.entity';
// import { Gap_Assessment_Question } from './gapassessmentquestion.entity';


@Entity({ name: 'gap_assessment_to_question' })
export class Gap_Assessment_To_Question {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'assessment_id' })
  assessment_id: number;
  @Column({ name: 'question_id' })
  question_id: number;

  // @OneToOne(() => Gap_Assessment_Framework)
  // @JoinColumn({ name: 'assessment_id', referencedColumnName: 'id' })
  // gapassessmentframework: Gap_Assessment_Framework;
  // @OneToOne(() => Gap_Assessment_Question)
  // @JoinColumn({ name: 'question_id', referencedColumnName: 'id' })
  // gapassessmentquestion: Gap_Assessment_Question;
}
