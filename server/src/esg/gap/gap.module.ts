import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Gap_Assessment_Framework } from './entity/gapassessmentframework.entity';
import { Gap_Assessment_Framework_Category } from './entity/gapassessmentframeworkcategory.entity';
import { Gap_Assessment_Framework_Category_Map } from './entity/gapassessmentframeworkcategorymap.entity';
import { Gap_Assessment_Question } from './entity/gapassessmentquestion.entity';
import { Gap_Assessment_Response } from './entity/gapassessmentresponse.entity';
import { Gap_Assessment_Share } from './entity/gapassessmentshare.entity';
import { Gap_Assessment_To_Question } from './entity/gapassessmenttoquestion.entity';
import { GapController } from './gap.controller';
import { GapService } from './gap.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Gap_Assessment_Framework, Gap_Assessment_Framework_Category, Gap_Assessment_Framework_Category_Map, Gap_Assessment_Question, Gap_Assessment_Response, Gap_Assessment_Share, Gap_Assessment_To_Question])
  ],
  controllers: [GapController],
  providers: [GapService]
})
export class GapModule { }
