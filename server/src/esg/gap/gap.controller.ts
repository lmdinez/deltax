import { Body, Controller, Get, Param, Post, Req, Res } from '@nestjs/common';
import { AssessmentAddFramework, AssessmentAddQuestion, AssessmentSaveAs, AssignAssessment, DeleteAssessment, SaveAsNew } from '../model/gap.model';
import { Utility } from '../utils/util';
import { GapService } from './gap.service';

@Controller('/Gap')
export class GapController {
  constructor(private gapService: GapService) {

  }
  @Get('assessment/:userid&:search?')
  async getassessment(@Res() response, @Param('userid') userid?: number, @Param('search') search?: string) {
    console.log(search);
    let srch = search == null ? "" : search;

    this.gapService.getassessment(srch, userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Get('assessment/view/:assessmentid')
  async getassessmentview(@Res() response, @Param('assessmentid') assessmentid?: number) {
    this.gapService.getassessmentview(assessmentid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }

  @Get('assigned/assessment/:userid&:search?')
  async getassignedassessment(@Res() response, @Param('userid') userid?: number, @Param('search') search?: string) {
    console.log(search);
    let srch = search == null ? "" : search;

    this.gapService.getassignedassessment(srch, userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }

  @Get('dashboardcounts/:userid')
  async dashboardcounts(@Res() response, @Param('userid') userid?) {
    this.gapService.dashboardcounts(userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Get('assigned/allassessment/:userid&:search?')
  async getassignedallassessment(@Res() response, @Param('search') search?: string, @Param('userid') userid?) {
    let srch = search == null ? "" : search;
    this.gapService.getassignedallassessment(srch, userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Get('assigned/allassessment/view/:userid')
  async getassignedallassessmentview(@Res() response, @Param('userid') userid?: number,) {
    this.gapService.getassignedallassessmentview(userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Get('dashboard/myscore/:userid')
  async getmyscore(@Res() response, @Param('userid') userid?: number,) {
    this.gapService.getmyscore(userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Get('dashboard/allscore/:userid')
  async getallscore(@Res() response, @Param('userid') userid?: number,) {
    this.gapService.getallscore(userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Post('assessment/assign')
  async assignassessment(@Res() response, @Body() assignassessment: AssignAssessment) {
    this.gapService.assignassessment(assignassessment).then(result => {
      if (-1 == result) {
        return Utility.handleFailure(response, "Country already present in database");
      }
      return result != 0 ? Utility.handleSuccessWithResult(response, "Assessment assigned successfully.") : Utility.handleFailureWithResult(response, "Country insert failed.");
    })
  }
  @Get('assessment/taketest/:userid&:assessmentid')
  async taketest(@Res() response, @Param('userid') userid?: number, @Param('assessmentid') assessmentid?: number) {
    this.gapService.taketest(userid, assessmentid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Post('assessment/saveasdraft')
  async assessmentsaveas(@Res() response, @Body() assessmentsaveas: AssessmentSaveAs) {
    this.gapService.assessmentsaveas(assessmentsaveas).then(result => {
      if (-1 == result) {
        return Utility.handleFailure(response, "Country already present in database");
      }
      return result != 0 ? Utility.handleSuccessWithResult(response, "Assessment saved successfully.") : Utility.handleFailureWithResult(response, "Country insert failed.");
    })
  }
  @Post('assessment/submit')
  async assessmentsubmit(@Res() response, @Body() assessmentsaveas: AssessmentSaveAs) {
    this.gapService.assessmentsubmit(assessmentsaveas).then(result => {

      if (-1 == result) {
        return Utility.handleFailure(response, "Country already present in database");
      }
      return result != 0 ? Utility.handleSuccessWithResult(response, "Assessment saved successfully.") : Utility.handleFailureWithResult(response, "Country insert failed.");
    })
  }
  @Post('assessment/addframework')
  async assessmentaddframework(@Res() response, @Body() assessmentaddframework: AssessmentAddFramework) {
    this.gapService.assessmentaddframework(assessmentaddframework).then(result => {
      if (-1 == result) {
        return Utility.handleFailure(response, "Country already present in database");
      }
      let out_response = {
        "message": "Assessment saved successfully.",
        "assessment_id": result
      }
      return result != 0 ? Utility.handleSuccessWithResult(response, out_response) : Utility.handleFailureWithResult(response, "Country insert failed.");
    })
  }

  @Get('assessment/addframework/:frameworkid')
  async getaddframework(@Res() response, @Param('frameworkid') frameworkid?: number) {
    this.gapService.getaddframework(frameworkid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }

  @Get('assessment/category/:frameworkid')
  async categorylist(@Res() response, @Param('frameworkid') frameworkid?: number) {
    this.gapService.categorylist(frameworkid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }

  @Get('assessment/subcategory/:categoryid')
  async subcategorylist(@Res() response, @Param('categoryid') categoryid?: number) {
    this.gapService.subcategorylist(categoryid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }

  @Post('assessment/addquestion')
  async assessmentaddquestion(@Res() response, @Body() assessmentaddquestion: AssessmentAddQuestion) {

    this.gapService.assessmentaddquestion(assessmentaddquestion).then(result => {

      if (-1 == result) {
        return Utility.handleFailure(response, "Country already present in database");
      }
      let out_response = {
        "message": "Question saved successfully.",
        "question_id": result
      }
      return result != 0 ? Utility.handleSuccessWithResult(response, out_response) : Utility.handleFailureWithResult(response, "Country insert failed.");
    })
  }
  @Get('assessment/review/:userid')
  async review(@Res() response, @Param('userid') userid?: number) {
    this.gapService.review(userid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Get('assessment/questiontype')
  async questiontype(@Res() response) {
    this.gapService.questiontype().then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Post('assessment/saveasnew')
  async saveasnew(@Res() response, @Body() saveasnew: SaveAsNew) {
    this.gapService.saveasnew(saveasnew).then(result => {
      if (-1 == result) {
        return Utility.handleFailure(response, "Country already present in database");
      }
      return result != 0 ? Utility.handleSuccessWithResult(response, result, "Assessment saved as new successfully.",) : Utility.handleFailureWithResult(response, "Country insert failed.");
    })
  }
  @Get('assessment/allassessment/location/:frameworkid')
  async getassignedallassessmentlocation(@Res() response, @Param('frameworkid') frameworkid?: number) {
    this.gapService.getassignedallassessmentlocation(frameworkid).then(result => {
      if (null == result) {
        return Utility.handleFailure(response, "No Data is present in database");
      }
      return Utility.handleSuccessList(response, result);
    })
  }
  @Post('assessment/delete')
  async deleteassessment(@Res() response, @Body() deleteframework: DeleteAssessment) {
    this.gapService.deleteassessment(deleteframework).then(result => {
      if (-1 == result) {
        return Utility.handleFailure(response, "Assessment is assigned to an user. Cannot be deleted");
      }
      return result != 0 ? Utility.handleSuccessWithResult(response, result, "Assessment deleted successfully.",) : Utility.handleFailureWithResult(response, "Country insert failed.");
    })
  }
}
