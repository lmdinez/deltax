import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { AssessmentAddFramework, AssessmentAddQuestion, AssessmentSaveAs, AssignAssessment, DeleteAssessment, SaveAsNew } from '../model/gap.model';
import { Gap_Assessment_Framework } from './entity/gapassessmentframework.entity';
import { Gap_Assessment_Framework_Category } from './entity/gapassessmentframeworkcategory.entity';
import { Gap_Assessment_Framework_Category_Map } from './entity/gapassessmentframeworkcategorymap.entity';
import { Gap_Assessment_Question } from './entity/gapassessmentquestion.entity';
import { Gap_Assessment_Response } from './entity/gapassessmentresponse.entity';
import { Gap_Assessment_Share } from './entity/gapassessmentshare.entity';
import { Gap_Assessment_To_Question } from './entity/gapassessmenttoquestion.entity';

@Injectable()
export class GapService {

  private className: string = 'GapService -->';
  constructor(@InjectRepository(Gap_Assessment_Framework) private GapAssessmentFrameworkRepo: Repository<Gap_Assessment_Framework>,
    @InjectRepository(Gap_Assessment_Framework_Category) private GapAssessmentFrameworkCategoryRepo: Repository<Gap_Assessment_Framework_Category>,
    @InjectRepository(Gap_Assessment_Framework_Category_Map) private GapAssessmentFrameworkCategoryMapRepo: Repository<Gap_Assessment_Framework_Category_Map>,
    @InjectRepository(Gap_Assessment_Question) private GapAssessmentQuestionRepo: Repository<Gap_Assessment_Question>,
    @InjectRepository(Gap_Assessment_Response) private GapAssessmentResponseRepo: Repository<Gap_Assessment_Response>,
    @InjectRepository(Gap_Assessment_Share) private GapAssessmentShareRepo: Repository<Gap_Assessment_Share>,
    @InjectRepository(Gap_Assessment_To_Question) private GapAssessmentToQuestionRepo: Repository<Gap_Assessment_To_Question>,
  ) {
  }


  public async getassessment(srch: string, userid: number) {

    // let assessmentDetails = await this.GapAssessmentFrameworkRepo.find();
    let assessmentDetails = await getRepository(Gap_Assessment_Framework).createQueryBuilder("gapframworkDetails")
      .where("(gapframworkDetails.framework_name LIKE :srch OR gapframworkDetails.description LIKE :srch) AND gapframworkDetails.created_by =:userid AND gapframworkDetails.is_master = 0", { srch: `%${srch}%`, userid: userid })
      .getMany();
    let resultarray = [];

    for (let i = 0; i < assessmentDetails.length; i++) {
      var objcount = {};
      let count = await this.GapAssessmentToQuestionRepo.count({ where: { assessment_id: assessmentDetails[i].id } })
      objcount["questioncount"] = count || 0;
      objcount["assessment"] = assessmentDetails[i];
      resultarray.push(objcount);
    }
    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }

  public async getassignedassessment(srch: string, userid: number) {

    // let assessmentDetails = await this.GapAssessmentFrameworkRepo.find();
    let assignedassessmentDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_share JOIN gap_assessment_framework ON gap_assessment_share.assessment_id = gap_assessment_framework.id WHERE gap_assessment_share.shared_with = ' + userid + ' AND (gap_assessment_framework.framework_name LIKE "%' + srch + '%" OR gap_assessment_framework.description LIKE "%' + srch + '%")');
    let resultarray = [];

    for (let i = 0; i < assignedassessmentDetails.length; i++) {
      var objcount = {};
      let count = await this.GapAssessmentToQuestionRepo.count({ where: { assessment_id: assignedassessmentDetails[i].id } })
      objcount["questioncount"] = count || 0;
      objcount["assessment"] = assignedassessmentDetails[i];
      resultarray.push(objcount);
    }
    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }

  public async dashboardcounts(userid: number) {
    var objcount = {};
    let resultarray = [];
    let count = await this.GapAssessmentShareRepo.findAndCount({ where: { shared_by: userid } });
    let as_ques, as_res, response_submited, ques = {};
    let completion_count = [];
    let final = [];
    let completed = 0, not_completed = 0, yet_to_start = 0;
    console.log(count);
    for (let i = 0; i < count[0].length; i++) {
      let ins = {};
      as_ques = await this.GapAssessmentToQuestionRepo.findAndCount({ where: { assessment_id: count[0][i].assessment_id } });
      as_res = await this.GapAssessmentResponseRepo.query('SELECT * FROM gap_assessment_response WHERE question_id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + count[0][i].assessment_id + ')');
      if (as_res.length == as_ques.length) {
        completed++;
      } else if (as_res.length == 0) {
        yet_to_start++;
      }
      else {
        not_completed++;
      }
    }

    objcount["shared_assessment_count"] = count[1] || 0;
    objcount["completed_assessment_count"] = completed;
    objcount["incompleted_assessment_count"] = not_completed;
    objcount["yet_to_start"] = yet_to_start;
    resultarray.push(objcount);
    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }
  public async getassignedallassessment(srch: String, userid: number) {

    let count = await this.GapAssessmentShareRepo.query('SELECT gap_assessment_share.assessment_id,gap_assessment_share.shared_by,gap_assessment_share.shared_with,gap_assessment_share.status,user.email as username_with,user_by.email as username_by,user.company_id,gap_assessment_framework.due_date,gap_assessment_framework.framework_name FROM gap_assessment_share JOIN gap_assessment_framework ON gap_assessment_share.assessment_id = gap_assessment_framework.id JOIN user ON user.id = gap_assessment_share.shared_with  JOIN user as user_by ON user_by.id = gap_assessment_share.shared_by WHERE (gap_assessment_framework.framework_name LIKE "%' + srch + '%" OR gap_assessment_framework.description LIKE "%' + srch + '%" OR user.email LIKE "%' + srch + '%" OR user_by.email LIKE "%' + srch + '%" OR gap_assessment_share.status LIKE "%' + srch + '%") AND gap_assessment_share.shared_by = ' + userid + '');

    if (null == count || count.length == 0) {
      return null;
    }
    console.log(count);
    return count;
  }
  public async getassignedallassessmentview(userid: number) {

    let assignedassessmentDetails = await this.GapAssessmentShareRepo.query('SELECT gap_assessment_share.assessment_id,gap_assessment_share.status,user.email,user.company_id FROM gap_assessment_share JOIN gap_assessment_framework ON gap_assessment_share.assessment_id = gap_assessment_framework.id JOIN user ON user.id = gap_assessment_share.shared_with WHERE user.id = ' + userid);
    let resultarray = [];
    let total_question = 0;
    let sum = 0;
    for (let i = 0; i < assignedassessmentDetails.length; i++) {
      var objcount = {};
      total_question = await this.GapAssessmentToQuestionRepo.count({ where: { assessment_id: assignedassessmentDetails[i].assessment_id } })
      let ans_ques = await this.GapAssessmentResponseRepo.query('SELECT point FROM gap_assessment_response WHERE question_id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + assignedassessmentDetails[i].assessment_id + ')');
      ans_ques.forEach(element => {
        sum += element.point;
      });
      objcount["total_question"] = total_question;
      objcount["questions_answered"] = ans_ques.length;
      objcount["points"] = sum;
      objcount["User"] = assignedassessmentDetails[i].email;
      objcount["Location"] = assignedassessmentDetails[i].company_id;
      resultarray.push(objcount);
    }
    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }

  public async getmyscore(userid: number) {

    let assignedassessmentDetails = await this.GapAssessmentShareRepo.query('SELECT year(gap_assessment_share.share_date) as year,month(gap_assessment_share.share_date) as month,gap_assessment_share.assessment_id,gap_assessment_share.status,user.email,user.company_id,gap_assessment_framework.framework_name FROM gap_assessment_share JOIN gap_assessment_framework ON gap_assessment_share.assessment_id = gap_assessment_framework.id JOIN user ON user.id = gap_assessment_share.shared_with WHERE user.id = ' + userid);
    let resultarray = [];
    let sum = 0;
    console.log(assignedassessmentDetails)
    for (let i = 0; i < assignedassessmentDetails.length; i++) {
      var objcount = {};
      let ans_ques = await this.GapAssessmentResponseRepo.query('SELECT point FROM gap_assessment_response WHERE question_id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + assignedassessmentDetails[i].assessment_id + ')');
      ans_ques.forEach(element => {
        // console.log(element.point)
        sum += element.point;
      });
      objcount["framework_name"] = assignedassessmentDetails[i].framework_name;
      objcount["points"] = sum;
      objcount["year"] = assignedassessmentDetails[i].year;
      objcount["month"] = assignedassessmentDetails[i].month;
      objcount["status"] = assignedassessmentDetails[i].status;
      sum = 0;
      resultarray.push(objcount);
    }
    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }

  public async getallscore(userid: number) {

    let assignedassessmentDetails = await this.GapAssessmentShareRepo.query('SELECT year(gap_assessment_share.share_date) as year,month(gap_assessment_share.share_date) as month,gap_assessment_share.assessment_id,gap_assessment_share.status,user.email,user.company_id,gap_assessment_framework.framework_name FROM gap_assessment_share JOIN gap_assessment_framework ON gap_assessment_share.assessment_id = gap_assessment_framework.id JOIN user ON user.id = gap_assessment_share.shared_by WHERE user.id = ' + userid);
    let resultarray = [];
    let sum = 0;

    for (let i = 0; i < assignedassessmentDetails.length; i++) {
      var objcount = {};
      let ans_ques = await this.GapAssessmentResponseRepo.query('SELECT point FROM gap_assessment_response WHERE question_id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + assignedassessmentDetails[i].assessment_id + ')');
      ans_ques.forEach(element => {
        sum += element.point;
      });
      objcount["framework_name"] = assignedassessmentDetails[i].framework_name;
      objcount["points"] = sum;
      objcount["year"] = assignedassessmentDetails[i].year;
      objcount["month"] = assignedassessmentDetails[i].month;
      objcount["status"] = assignedassessmentDetails[i].status;
      sum = 0;
      resultarray.push(objcount);
    }
    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }

  public async getassessmentview(assessmentid: number) {

    let assignedassessmentDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_question WHERE id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + assessmentid + ')');
    for (let i = 0; i < assignedassessmentDetails.length; i++) {
      let categoryDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_framework_category WHERE id = ' + assignedassessmentDetails[i].category_id);
      assignedassessmentDetails[i].category_name = categoryDetails[0].category_name
      let subcategoryDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_framework_category WHERE id = ' + assignedassessmentDetails[i].sub_category_id);
      assignedassessmentDetails[i].sub_category_name = subcategoryDetails[0].category_name
      let questiontypeDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_question_type WHERE id = ' + assignedassessmentDetails[i].question_type_id);
      assignedassessmentDetails[i].type_of_question_name = questiontypeDetails[0].type
    }

    if (null == assignedassessmentDetails || assignedassessmentDetails.length == 0) {
      return null;
    }
    console.log(assignedassessmentDetails);
    return assignedassessmentDetails;
  }

  public async assignassessment(assignassessment: AssignAssessment): Promise<number> {
    let assessmentDetails = await this.GapAssessmentFrameworkRepo.query('SELECT * FROM gap_assessment_framework WHERE id = ' + assignassessment.AssessmentId);
    let questDetails = await this.GapAssessmentToQuestionRepo.query('SELECT * FROM gap_assessment_to_question WHERE assessment_id = ' + assignassessment.AssessmentId);
    let GapAssessmentShareEntity: Gap_Assessment_Share = new Gap_Assessment_Share();
    let GapAssessmentFrameworkEntity: Gap_Assessment_Framework = new Gap_Assessment_Framework();
    GapAssessmentFrameworkEntity.framework_name = assessmentDetails[0].framework_name + "_" + new Date().getTime();
    GapAssessmentFrameworkEntity.description = assessmentDetails[0].description;
    GapAssessmentFrameworkEntity.created_by = assignassessment.SharedBy;
    GapAssessmentFrameworkEntity.is_master = 0;
    GapAssessmentFrameworkEntity.created_on = new Date();
    GapAssessmentFrameworkEntity.updated_on = new Date();
    GapAssessmentFrameworkEntity.due_date = new Date(assignassessment.DueDate)
    let assessmentFramworkInsertResult = await this.GapAssessmentFrameworkRepo.insert(GapAssessmentFrameworkEntity);
    for (let i = 0; i < questDetails.length; i++) {
      let GapAssessmentToQuestionEntity: Gap_Assessment_To_Question = new Gap_Assessment_To_Question();
      GapAssessmentToQuestionEntity.assessment_id = assessmentFramworkInsertResult.raw.insertId;
      GapAssessmentToQuestionEntity.question_id = questDetails[i].question_id;
      let GapAssessmentToQuestionResult = await this.GapAssessmentToQuestionRepo.insert(GapAssessmentToQuestionEntity);

    }
    GapAssessmentShareEntity.assessment_id = assessmentFramworkInsertResult.raw.insertId;
    GapAssessmentShareEntity.shared_by = assignassessment.SharedBy;
    GapAssessmentShareEntity.shared_with = assignassessment.SharedWith;
    GapAssessmentShareEntity.share_date = new Date();
    GapAssessmentShareEntity.status = "YET_TO_START"
    let assignassessmentResult = await this.GapAssessmentShareRepo.insert(GapAssessmentShareEntity);
    return assignassessmentResult.raw.Id;
  }

  public async taketest(userid: number, assesmentid: number) {
    let objcount = {};
    let resultarray = [];
    console.log("assessment_id", assesmentid);
    let questionDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_question WHERE id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + assesmentid + ')');
    console.log("questionDetails", questionDetails)
    let assignedassesmentDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_share  JOIN gap_assessment_framework ON gap_assessment_share.assessment_id = gap_assessment_framework.id  WHERE gap_assessment_share.assessment_id = ' + assesmentid);
    console.log("assignedassesmentDetails")
    let total_question = await this.GapAssessmentToQuestionRepo.count({ where: { assessment_id: assesmentid } })
    console.log("total_question")
    for (let i = 0; i < questionDetails.length; i++) {
      let categoryDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_framework_category WHERE id = ' + questionDetails[i].category_id);
      questionDetails[i].category_name = categoryDetails[0].category_name
      let subcategoryDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_framework_category WHERE id = ' + questionDetails[i].sub_category_id);
      questionDetails[i].sub_category_name = subcategoryDetails[0].category_name
      let questiontypeDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_question_type WHERE id = ' + questionDetails[i].question_type_id);
      questionDetails[i].type_of_question_name = questiontypeDetails[0].type
      let responseDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_response WHERE question_id = ' + parseInt(questionDetails[i].id));
      if (responseDetails[0]) {
        questionDetails[i].response = responseDetails[0].response
      } else {
        questionDetails[i].response = null
      }


    }

    objcount["total_question"] = total_question;
    objcount["Assessment_details"] = assignedassesmentDetails[0];
    objcount["Question_details"] = questionDetails;
    resultarray.push(objcount);

    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }

  public async assessmentsaveas(assessmentsaveas: AssessmentSaveAs): Promise<number> {
    let point = 0;
    console.log(assessmentsaveas);
    for (let i = 0; i < assessmentsaveas.QuestionId.length; i++) {
      console.log(assessmentsaveas.QuestionId[i])
      let questionDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_to_question WHERE question_id = ' + assessmentsaveas.QuestionId[i]);
      if (questionDetails[0].desired_response == assessmentsaveas.Response[i]) {
        point = 1;
      }
      let GapAssessmentResponseEntity: Gap_Assessment_Response = new Gap_Assessment_Response();
      GapAssessmentResponseEntity.question_id = assessmentsaveas.QuestionId[i];
      GapAssessmentResponseEntity.response = assessmentsaveas.Response[i];
      GapAssessmentResponseEntity.point = point;
      GapAssessmentResponseEntity.respose_by = assessmentsaveas.ResponseBy;

      let GapAssessmentToQuestionResult = await this.GapAssessmentResponseRepo.insert(GapAssessmentResponseEntity);

    }
    let update = await this.GapAssessmentShareRepo.query('UPDATE gap_assessment_share SET status = "IN_PROGRESS" WHERE assessment_id = ' + assessmentsaveas.AssessmentId);
    return update;
  }

  public async assessmentsubmit(assessmentsaveas: AssessmentSaveAs): Promise<number> {
    let point = 0;
    for (let i = 0; i < assessmentsaveas.QuestionId.length; i++) {

      let questionDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_to_question WHERE question_id = ' + assessmentsaveas.QuestionId[i]);
      if (questionDetails[0].desired_response == assessmentsaveas.Response[i]) {
        point = 1;
      }
      let GapAssessmentResponseEntity: Gap_Assessment_Response = new Gap_Assessment_Response();
      GapAssessmentResponseEntity.question_id = assessmentsaveas.QuestionId[i];
      GapAssessmentResponseEntity.response = assessmentsaveas.Response[i];
      GapAssessmentResponseEntity.point = point;
      GapAssessmentResponseEntity.respose_by = assessmentsaveas.ResponseBy;
      let deletequestion = await this.GapAssessmentResponseRepo.query('DELETE FROM gap_assessment_response WHERE question_id=' + assessmentsaveas.QuestionId[i]);
      let GapAssessmentToQuestionResult = await this.GapAssessmentResponseRepo.insert(GapAssessmentResponseEntity);

    }
    let update = await this.GapAssessmentShareRepo.query('UPDATE gap_assessment_share SET status = "COMPLETED" WHERE assessment_id = ' + assessmentsaveas.AssessmentId);
    return update;
  }

  public async assessmentaddframework(assessmentaddframework: AssessmentAddFramework): Promise<number> {

    let GapAssessmentFrameworkEntity: Gap_Assessment_Framework = new Gap_Assessment_Framework();
    GapAssessmentFrameworkEntity.framework_name = assessmentaddframework.FrameworkName;
    GapAssessmentFrameworkEntity.description = assessmentaddframework.Description;
    GapAssessmentFrameworkEntity.created_by = assessmentaddframework.CreatedBy;
    GapAssessmentFrameworkEntity.is_master = 0;
    GapAssessmentFrameworkEntity.created_on = new Date();
    GapAssessmentFrameworkEntity.updated_on = new Date();
    GapAssessmentFrameworkEntity.due_date = new Date()
    let assessmentFramworkInsertResult = await this.GapAssessmentFrameworkRepo.insert(GapAssessmentFrameworkEntity);
    console.log(assessmentFramworkInsertResult)
    for (let i = 0; i < assessmentaddframework.Category.length; i++) {
      console.log("category save:")
      let GapAssessmentFrameworkCategoryEntity: Gap_Assessment_Framework_Category = new Gap_Assessment_Framework_Category();
      GapAssessmentFrameworkCategoryEntity.category_name = assessmentaddframework.Category[i].CategoryName;
      GapAssessmentFrameworkCategoryEntity.parent_id = 0;
      GapAssessmentFrameworkCategoryEntity.created_by = assessmentaddframework.CreatedBy;
      GapAssessmentFrameworkCategoryEntity.created_on = new Date();
      GapAssessmentFrameworkCategoryEntity.updated_on = new Date();
      let assessmentFramworkCategoryInsertResult = await this.GapAssessmentFrameworkCategoryRepo.insert(GapAssessmentFrameworkCategoryEntity);
      let GapAssessmentFrameworkCategoryMapEntity: Gap_Assessment_Framework_Category_Map = new Gap_Assessment_Framework_Category_Map();
      GapAssessmentFrameworkCategoryMapEntity.category_id = assessmentFramworkCategoryInsertResult.raw.insertId;
      GapAssessmentFrameworkCategoryMapEntity.framework_id = assessmentFramworkInsertResult.raw.insertId;
      let assessmentFramworkCategoryMapInsertResult = await this.GapAssessmentFrameworkCategoryMapRepo.insert(GapAssessmentFrameworkCategoryMapEntity);
      console.log("assessmentFramworkCategoryMapInsertResult id:", assessmentFramworkCategoryInsertResult.raw.insertId);
      assessmentaddframework.Category[i].Subcategory.forEach(async sub_cat => {
        console.log("subcat", sub_cat);
        console.log("parent id:", assessmentFramworkCategoryInsertResult.raw.insertId);
        let GapAssessmentFrameworkSubCategoryEntity: Gap_Assessment_Framework_Category = new Gap_Assessment_Framework_Category();
        GapAssessmentFrameworkSubCategoryEntity.category_name = sub_cat;
        GapAssessmentFrameworkSubCategoryEntity.parent_id = assessmentFramworkCategoryInsertResult.raw.insertId;
        GapAssessmentFrameworkSubCategoryEntity.created_by = assessmentaddframework.CreatedBy;
        GapAssessmentFrameworkSubCategoryEntity.created_on = new Date();
        GapAssessmentFrameworkSubCategoryEntity.updated_on = new Date();
        let assessmentFramworkSubCategoryInsertResult = await this.GapAssessmentFrameworkCategoryRepo.insert(GapAssessmentFrameworkSubCategoryEntity);
        let GapAssessmentFrameworkSubCategoryMapEntity: Gap_Assessment_Framework_Category_Map = new Gap_Assessment_Framework_Category_Map();
        GapAssessmentFrameworkSubCategoryMapEntity.category_id = assessmentFramworkSubCategoryInsertResult.raw.insertId;
        GapAssessmentFrameworkSubCategoryMapEntity.framework_id = assessmentFramworkInsertResult.raw.insertId;
        let assessmentFramworkSubCategoryMapInsertResult = await this.GapAssessmentFrameworkCategoryMapRepo.insert(GapAssessmentFrameworkSubCategoryMapEntity);

      })

    }
    //let update = await this.GapAssessmentShareRepo.query('UPDATE gap_assessment_share SET status = "COMPLETED" WHERE assessment_id = ' + assessmentsaveas.AssessmentId);
    return assessmentFramworkInsertResult.raw.insertId;
  }

  public async getaddframework(frameworkid: number) {
    console.log("getaddframework", frameworkid);
    let objcount = {};
    let resultarray = [];
    let frameworkDetails = await this.GapAssessmentFrameworkRepo.query('SELECT * FROM gap_assessment_framework WHERE id = ' + frameworkid);
    console.log(frameworkDetails)
    let categoryDetails = await this.GapAssessmentFrameworkCategoryRepo.query('SELECT * FROM gap_assessment_framework_category WHERE id IN (SELECT category_id FROM gap_assessment_framework_category_map WHERE framework_id = ' + frameworkid + ')');
    console.log(categoryDetails)

    objcount["frameworkDetails"] = frameworkDetails;
    objcount["categoryDetails"] = categoryDetails;
    resultarray.push(objcount);

    if (null == resultarray || resultarray.length == 0) {
      return null;
    }
    console.log(resultarray);
    return resultarray;
  }
  public async categorylist(frameworkid: number) {
    let categoryDetails = await this.GapAssessmentFrameworkCategoryMapRepo.query('SELECT * FROM gap_assessment_framework_category WHERE parent_id = 0 AND id IN (SELECT category_id FROM gap_assessment_framework_category_map WHERE framework_id = ' + frameworkid + ')');

    if (null == categoryDetails || categoryDetails.length == 0) {
      return null;
    }
    console.log(categoryDetails);
    return categoryDetails;
  }

  public async subcategorylist(categoryid: number) {
    let subcategoryDetails = await this.GapAssessmentFrameworkCategoryMapRepo.query('SELECT * FROM gap_assessment_framework_category WHERE parent_id =' + categoryid);

    if (null == subcategoryDetails || subcategoryDetails.length == 0) {
      return null;
    }
    console.log(subcategoryDetails);
    return subcategoryDetails;
  }

  public async assessmentaddquestion(assessmentaddquestion: AssessmentAddQuestion): Promise<number> {
    let GapAssessmentQuestionEntity: Gap_Assessment_Question = new Gap_Assessment_Question();
    GapAssessmentQuestionEntity.category_id = parseInt(assessmentaddquestion.Category);
    GapAssessmentQuestionEntity.sub_category_id = parseInt(assessmentaddquestion.Subcategory);
    GapAssessmentQuestionEntity.question_type_id = parseInt(assessmentaddquestion.QuestionType);
    GapAssessmentQuestionEntity.question = assessmentaddquestion.Question;
    GapAssessmentQuestionEntity.desired_response = assessmentaddquestion.DesiredResponse;
    GapAssessmentQuestionEntity.comment = assessmentaddquestion.Comment;
    GapAssessmentQuestionEntity.created_on = new Date();
    GapAssessmentQuestionEntity.updated_on = new Date();
    GapAssessmentQuestionEntity.created_by = assessmentaddquestion.CreatedBy;
    if (assessmentaddquestion.Id == "insert") {
      let GapAssessmentQuestionResult = await this.GapAssessmentQuestionRepo.insert(GapAssessmentQuestionEntity);
      console.log(GapAssessmentQuestionResult)
      let GapAssessmentToQuestionEntity: Gap_Assessment_To_Question = new Gap_Assessment_To_Question();
      GapAssessmentToQuestionEntity.assessment_id = assessmentaddquestion.AssessmentId;
      GapAssessmentToQuestionEntity.question_id = GapAssessmentQuestionResult.raw.insertId;
      let GapAssessmentToQuestionResult = await this.GapAssessmentToQuestionRepo.insert(GapAssessmentToQuestionEntity);
      console.log(GapAssessmentToQuestionResult)
      //let update = await this.GapAssessmentShareRepo.query('UPDATE gap_assessment_share SET status = "COMPLETED" WHERE assessment_id = ' + assessmentsaveas.AssessmentId);
      return GapAssessmentQuestionResult.raw.insertId;
    } else {

      let GapAssessmentQuestionUpdateResult = await this.GapAssessmentQuestionRepo.update({ id: parseInt(assessmentaddquestion.Id) }, GapAssessmentQuestionEntity);
      return parseInt(assessmentaddquestion.Id);
    }

  }
  public async review(userid): Promise<Gap_Assessment_Response[]> {
    // console.log("get input:", Id);

    let response: Gap_Assessment_Response[] = await this.GapAssessmentResponseRepo.find({ where: { respose_by: userid } });
    if (response.length == 0) {
      return null;
    }

    console.log(response);
    return response;
  }
  public async questiontype(): Promise<Gap_Assessment_Response[]> {
    // console.log("get input:", Id);

    let response: Gap_Assessment_Response[] = await this.GapAssessmentResponseRepo.query('SELECT * FROM gap_assessment_question_type');
    if (response.length == 0) {
      return null;
    }

    console.log(response);
    return response;
  }

  public async saveasnew(saveasnew: SaveAsNew): Promise<number> {
    if (saveasnew.Type == "all") {
      let assessmentDetails = await this.GapAssessmentFrameworkRepo.query('SELECT * FROM gap_assessment_framework WHERE id = ' + saveasnew.AssessmentId);
      let category = await this.GapAssessmentFrameworkCategoryRepo.query('SELECT * FROM gap_assessment_framework_category WHERE parent_id = 0 AND id IN (SELECT category_id FROM gap_assessment_framework_category_map WHERE framework_id = ' + saveasnew.AssessmentId + ')');
      //let subcategory = await this.GapAssessmentFrameworkCategoryRepo.query('SELECT * FROM gap_assessment_framework_category WHERE parent_id IN (SELECT id FROM gap_assessment_framework_category WHERE parent_id = 0 AND id IN (SELECT category_id FROM gap_assessment_framework_category_map WHERE framework_id = ' + saveasnew.AssessmentId + ')) AND id IN (SELECT category_id FROM gap_assessment_framework_category_map WHERE framework_id = ' + saveasnew.AssessmentId + ')');
      // let question = await this.GapAssessmentQuestionRepo.query('SELECT * FROM gap_assessment_question WHERE id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + saveasnew.AssessmentId + ')');

      let GapAssessmentShareEntity: Gap_Assessment_Share = new Gap_Assessment_Share();
      let GapAssessmentFrameworkEntity: Gap_Assessment_Framework = new Gap_Assessment_Framework();
      GapAssessmentFrameworkEntity.framework_name = assessmentDetails[0].framework_name + "_" + new Date().getTime();
      GapAssessmentFrameworkEntity.description = assessmentDetails[0].description;
      GapAssessmentFrameworkEntity.created_by = saveasnew.CreatedBy;
      GapAssessmentFrameworkEntity.is_master = 0;
      GapAssessmentFrameworkEntity.created_on = new Date();
      GapAssessmentFrameworkEntity.updated_on = new Date();
      GapAssessmentFrameworkEntity.due_date = new Date()
      let assessmentFramworkInsertResult = await this.GapAssessmentFrameworkRepo.insert(GapAssessmentFrameworkEntity);
      for (let i = 0; i < category.length; i++) {
        console.log("category save:", category[i])
        let subcategory = await this.GapAssessmentFrameworkCategoryRepo.query('SELECT * FROM gap_assessment_framework_category WHERE parent_id = ' + category[i].id + ' AND id IN (SELECT category_id FROM gap_assessment_framework_category_map WHERE framework_id = ' + saveasnew.AssessmentId + ')');

        let GapAssessmentFrameworkCategoryEntity: Gap_Assessment_Framework_Category = new Gap_Assessment_Framework_Category();
        GapAssessmentFrameworkCategoryEntity.category_name = category[i].category_name;
        GapAssessmentFrameworkCategoryEntity.parent_id = 0;
        GapAssessmentFrameworkCategoryEntity.created_by = saveasnew.CreatedBy;
        GapAssessmentFrameworkCategoryEntity.created_on = new Date();
        GapAssessmentFrameworkCategoryEntity.updated_on = new Date();
        let assessmentFramworkCategoryInsertResult = await this.GapAssessmentFrameworkCategoryRepo.insert(GapAssessmentFrameworkCategoryEntity);
        let GapAssessmentFrameworkCategoryMapEntity: Gap_Assessment_Framework_Category_Map = new Gap_Assessment_Framework_Category_Map();
        GapAssessmentFrameworkCategoryMapEntity.category_id = assessmentFramworkCategoryInsertResult.raw.insertId;
        GapAssessmentFrameworkCategoryMapEntity.framework_id = assessmentFramworkInsertResult.raw.insertId;
        let assessmentFramworkCategoryMapInsertResult = await this.GapAssessmentFrameworkCategoryMapRepo.insert(GapAssessmentFrameworkCategoryMapEntity);
        subcategory.forEach(async sub_cat => {
          console.log("subcat", sub_cat);
          console.log("parent id:", assessmentFramworkCategoryInsertResult.raw.insertId);
          let question = await this.GapAssessmentQuestionRepo.query('SELECT * FROM gap_assessment_question WHERE category_id = ' + category[i].id + ' AND sub_category_id = ' + sub_cat.id + ' AND id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + saveasnew.AssessmentId + ')');

          let GapAssessmentFrameworkSubCategoryEntity: Gap_Assessment_Framework_Category = new Gap_Assessment_Framework_Category();
          GapAssessmentFrameworkSubCategoryEntity.category_name = sub_cat.category_name;
          GapAssessmentFrameworkSubCategoryEntity.parent_id = assessmentFramworkCategoryInsertResult.raw.insertId;
          GapAssessmentFrameworkSubCategoryEntity.created_by = saveasnew.CreatedBy;
          GapAssessmentFrameworkSubCategoryEntity.created_on = new Date();
          GapAssessmentFrameworkSubCategoryEntity.updated_on = new Date();
          let assessmentFramworkSubCategoryInsertResult = await this.GapAssessmentFrameworkCategoryRepo.insert(GapAssessmentFrameworkSubCategoryEntity);
          let GapAssessmentFrameworkSubCategoryMapEntity: Gap_Assessment_Framework_Category_Map = new Gap_Assessment_Framework_Category_Map();
          GapAssessmentFrameworkSubCategoryMapEntity.category_id = assessmentFramworkSubCategoryInsertResult.raw.insertId;
          GapAssessmentFrameworkSubCategoryMapEntity.framework_id = assessmentFramworkInsertResult.raw.insertId;
          let assessmentFramworkSubCategoryMapInsertResult = await this.GapAssessmentFrameworkCategoryMapRepo.insert(GapAssessmentFrameworkSubCategoryMapEntity);
          question.forEach(async ques => {
            console.log("ques", ques)
            let GapAssessmentQuestionEntity: Gap_Assessment_Question = new Gap_Assessment_Question();
            GapAssessmentQuestionEntity.category_id = assessmentFramworkCategoryInsertResult.raw.insertId;
            GapAssessmentQuestionEntity.sub_category_id = assessmentFramworkSubCategoryInsertResult.raw.insertId;
            GapAssessmentQuestionEntity.question_type_id = ques.question_type_id;
            GapAssessmentQuestionEntity.question = ques.question;
            GapAssessmentQuestionEntity.desired_response = ques.desired_response;
            GapAssessmentQuestionEntity.comment = ques.comment;
            GapAssessmentQuestionEntity.created_on = new Date();
            GapAssessmentQuestionEntity.updated_on = new Date();
            GapAssessmentQuestionEntity.created_by = saveasnew.CreatedBy;
            let GapAssessmentQuestionResult = await this.GapAssessmentQuestionRepo.insert(GapAssessmentQuestionEntity);
            //console.log(GapAssessmentQuestionResult)
            let GapAssessmentToQuestionEntity: Gap_Assessment_To_Question = new Gap_Assessment_To_Question();
            GapAssessmentToQuestionEntity.assessment_id = assessmentFramworkInsertResult.raw.insertId;
            GapAssessmentToQuestionEntity.question_id = GapAssessmentQuestionResult.raw.insertId;
            let GapAssessmentToQuestionResult = await this.GapAssessmentToQuestionRepo.insert(GapAssessmentToQuestionEntity);
            // console.log(GapAssessmentToQuestionResult)
            //let update = await this.GapAssessmentShareRepo.query('UPDATE gap_assessment_share SET status = "COMPLETED" WHERE assessment_id = ' + assessmentsaveas.AssessmentId);
            // return GapAssessmentQuestionResult.raw.insertId;

          })


        })
      }
      return assessmentFramworkInsertResult.raw.insertId
    } else {
      let subcategory = await this.GapAssessmentFrameworkCategoryRepo.query('SELECT * FROM gap_assessment_framework_category WHERE id = ' + saveasnew.CategoryId);

      let question = await this.GapAssessmentQuestionRepo.query('SELECT * FROM gap_assessment_question WHERE sub_category_id = ' + saveasnew.CategoryId + ' AND id IN (SELECT question_id FROM gap_assessment_to_question WHERE assessment_id = ' + saveasnew.AssessmentId + ')');

      let GapAssessmentFrameworkSubCategoryEntity: Gap_Assessment_Framework_Category = new Gap_Assessment_Framework_Category();
      GapAssessmentFrameworkSubCategoryEntity.category_name = subcategory[0].category_name;
      GapAssessmentFrameworkSubCategoryEntity.parent_id = subcategory[0].parent_id;
      GapAssessmentFrameworkSubCategoryEntity.created_by = saveasnew.CreatedBy;
      GapAssessmentFrameworkSubCategoryEntity.created_on = new Date();
      GapAssessmentFrameworkSubCategoryEntity.updated_on = new Date();
      let assessmentFramworkSubCategoryInsertResult = await this.GapAssessmentFrameworkCategoryRepo.insert(GapAssessmentFrameworkSubCategoryEntity);
      let GapAssessmentFrameworkSubCategoryMapEntity: Gap_Assessment_Framework_Category_Map = new Gap_Assessment_Framework_Category_Map();
      GapAssessmentFrameworkSubCategoryMapEntity.category_id = assessmentFramworkSubCategoryInsertResult.raw.insertId;
      GapAssessmentFrameworkSubCategoryMapEntity.framework_id = saveasnew.AssessmentId;
      let assessmentFramworkSubCategoryMapInsertResult = await this.GapAssessmentFrameworkCategoryMapRepo.insert(GapAssessmentFrameworkSubCategoryMapEntity);
      question.forEach(async ques => {
        console.log("ques", ques)
        let GapAssessmentQuestionEntity: Gap_Assessment_Question = new Gap_Assessment_Question();
        GapAssessmentQuestionEntity.category_id = subcategory[0].parent_id;
        GapAssessmentQuestionEntity.sub_category_id = assessmentFramworkSubCategoryInsertResult.raw.insertId;
        GapAssessmentQuestionEntity.question_type_id = ques.question_type_id;
        GapAssessmentQuestionEntity.question = ques.question;
        GapAssessmentQuestionEntity.desired_response = ques.desired_response;
        GapAssessmentQuestionEntity.comment = ques.comment;
        GapAssessmentQuestionEntity.created_on = new Date();
        GapAssessmentQuestionEntity.updated_on = new Date();
        GapAssessmentQuestionEntity.created_by = saveasnew.CreatedBy;
        let GapAssessmentQuestionResult = await this.GapAssessmentQuestionRepo.insert(GapAssessmentQuestionEntity);
        //console.log(GapAssessmentQuestionResult)
        let GapAssessmentToQuestionEntity: Gap_Assessment_To_Question = new Gap_Assessment_To_Question();
        GapAssessmentToQuestionEntity.assessment_id = saveasnew.AssessmentId;
        GapAssessmentToQuestionEntity.question_id = GapAssessmentQuestionResult.raw.insertId;
        let GapAssessmentToQuestionResult = await this.GapAssessmentToQuestionRepo.insert(GapAssessmentToQuestionEntity);
        // console.log(GapAssessmentToQuestionResult)
        //let update = await this.GapAssessmentShareRepo.query('UPDATE gap_assessment_share SET status = "COMPLETED" WHERE assessment_id = ' + assessmentsaveas.AssessmentId);
        // return GapAssessmentQuestionResult.raw.insertId;


      })

    }
    return saveasnew.AssessmentId
  }

  public async getassignedallassessmentlocation(frameworkid: number) {

    let count = await this.GapAssessmentShareRepo.query('SELECT state.name,gap_assessment_share.status FROM gap_assessment_share JOIN user ON user.id = gap_assessment_share.shared_with JOIN company ON user.company_id = company.id JOIN state ON company.state_id = state.id  WHERE gap_assessment_share.assessment_id = ' + frameworkid + '');

    if (null == count || count.length == 0) {
      return null;
    }
    console.log(count);
    return count;
  }

  public async deleteassessment(deleteframework: DeleteAssessment): Promise<number> {

    // let assessmentDetails = await this.GapAssessmentFrameworkRepo.find();
    let assignedassessmentDetails = await this.GapAssessmentShareRepo.query('SELECT * FROM gap_assessment_share WHERE assessment_id =' + deleteframework.AssessmentId);
    let resultarray = [];

    if (resultarray.length > 0) {
      return -1;
    }
    let deleteassessment = await this.GapAssessmentShareRepo.query('DELETE FROM gap_assessment_framework WHERE id =' + deleteframework.AssessmentId);
    return 1;
  }

} 
