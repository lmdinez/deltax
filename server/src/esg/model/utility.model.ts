export class InsertCountry {
  name: string;
  code: string;
  phonecode: number;
}
export class UpdateCountry {
  id: number;
  name: string;
  code: string;
  active: number;
  phonecode: number;
}
export class DeleteCountry {
  id: number;
}
export class InsertCountryCode {
  Code: string;
  CountryId: number;
}
export class UpdateCountryCode {
  Id: number;
  Code: string;
  CountryId: number;
}
export class DeleteCountryCode {
  Id: number;
}
export class InsertIndustryType {
  type: string;
}
export class UpdateIndustryType {
  Id: number;
  type: string;
}
export class DeleteIndustryType {
  Id: number;
}
export class InsertLanguage {
  name: string;
  code: string;
}
export class UpdateLanguage {
  id: number;
  name: string;
  code: string;
}
export class DeleteLanguage {
  id: number;
}
