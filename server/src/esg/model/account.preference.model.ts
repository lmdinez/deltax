
export class AccountPreference {

  public CompanyId: number;
  public LanguageId: number;
  public ThemeId: number;
  public TimezoneId: number;
}
