import { CompanyBillingAddress, CompanyOtherAddress } from "./company.model";

export class UserViewModel {
  Id: number;
  UserName: string;
  FirstName: string;
  LastName: string;
  DateOfBirth: string | null;
  DesignationId: number;
  CountryCodeId: number;
  Designation: string;
  CountryCode: string;
  PhoneNumber: string;
  StatusId: number;
  Status: string;
  RoleId: number;
  Role: string;
  CompanyId: number | null;
  Email: string;
  Image: string;
  Roles: string;
  AuthToken: string;
  LocationType: string;
  LocationTypeId: number;
  IsPrimaryContact: boolean;
}

export class UserCompleteInformationViewModel {
  Id: number;
  UserName: string;
  FirstName: string;
  LastName: string;
  DateOfBirth: string | null;
  PhoneNumber: string;
  Status: string;
  StatusId: number;
  Email: string;
  Image: string;
  DesignationId: number;
  CountryCodeId: number;
  Designation: string;
  CountryCode: string;
  RoleId: number;
  Role: string;
  LocationType: string;
  LocationTypeId: number;
  IsPrimaryContact: boolean;
  companies: userCompanies[];
}

export class userCompanies {
  CompanyId: number | null;
  CompanyName: string;
  IndustryTypeId: number;
  CountryId: number;
  StateId: number;
  ThemeId: number;
  Theme: string;
  IndustryType: string;
  Country: string;
  State: string;
  ImageId: string;
  BackgroundimageId: string;
  billingAddress: CompanyBillingAddress;
  otherAddress: CompanyOtherAddress[];
}

export class AllUserInformationViewModel {
  Id: number;
  UserName: string;
  FirstName: string;
  LastName: string;
  DateOfBirth: string | null;
  PhoneNumber: string;
  StatusId: number;
  Status: string;
  Email: string;
  Image: string;
  DesignationId: number;
  CountryCodeId: number;
  CountryCode: string;
  Designation: string;
  LocationTypeId: number;
  LocationType: string;
  IsPrimaryContact: boolean;
  RoleId: number;
  Role: string;
  CompanyId: number | null;
}
