export class CompanyViewModel {
  public Id: number;
  public CompanyName: string;
  public IndustryTypeId: number;
  public CountryId: number;
  public StateId: number;
  public ThemeId: number;
  public TimezoneId: number;
  public LanguageId: number;
  public IndustryType: string;
  public TimezoneName: string;
  public TimezoneDescription: string;
  public TimezoneRelativeInGmt: string;
  public Language: string;
  public Theme: string;
  public Country: string;
  public State: string;
  public ImageId: string;
  public BackgroundImageId: string;
  public billingAddress: CompanyBillingAddress;
  public otherAddress: CompanyOtherAddress[]
}


export class CompanyBillingAddress {
  public Id: number;
  public AddressLine1: string;
  public AddressLine2: string;
  public City: string;
  public ZipCode: string;
  public PointOfContactName: string;
  public CountryCodeId: number;
  public CountryCode: string;
  public PointOfContactMobileNumber: string;
  public CorrespondanceEmail: string;
  public AlternativeEmail: string;

  public StateId: number;
  public CountryId: number;
  public State: string;
  public Country: string;
  public LocationType: string;
}


export class CompanyOtherAddress {
  public Id: number;
  public AddressLine1: string;
  public AddressLine2: string;
  public City: string;
  public ZipCode: string;
  public PointOfContactName: string;

  public CountryCodeId: number;
  public CountryCode: string;
  public PointOfContactMobileNumber: string;
  public CorrespondanceEmail: string;
  public AlternativeEmail: string;

  public StateId: number;
  public CountryId: number;
  public TimezoneId: number;
  public State: string;
  public Country: string;
  public LocationType: string;
  public Timezone: string;
}

export class InsertCompanyBillingAddress {
  CompanyId: number;
  AddressLine1: string;
  AddressLine2: string;
  City: string;
  ZipCode: string;
  PointOfContactName: string;
  CountryCodeId: number;
  PointOfContactMobileNumber: string;
  CountryId: number;
  StateId: number;
  LocationType: string;
  CorrespondanceEmail: string;
}

export class InsertCompleteCompanyDetailsViewModel {
  CompanyName: string;
  IndustryTypeId: number;
  CountryId: number;
  StateId: number;
  billingAddress: InsertCompleteCompanyBillingAddress;
  otherAddress: CompanyInsertCompleteOtherAddress[];
}

export class CompanyInsertCompleteOtherAddress {
  AddressLine1: string;
  AddressLine2: string;
  City: string;
  ZipCode: string;
  PointOfContactName: string;
  CountryCodeId: number;
  PointOfContactMobileNumber: string;
  CorrespondanceEmail: string;
  CountryId: number;
  TimezoneId: number;
  StateId: number;
  LocationType: string;
  AlternativeEmail: string;
}

export class CompanyInsertOtherAddress {
  Id: number;
  AddressLine1: string;
  AddressLine2: string;
  City: string;
  ZipCode: string;
  PointOfContactName: string;
  CountryCodeId: number;
  PointOfContactMobileNumber: string;
  CorrespondanceEmail: string;
  CountryId: number;
  TimezoneId: number;
  StateId: number;
  LocationType: string;
  AlternativeEmail: string;
}

export class InsertCompanyDetailsViewModel {
  CompanyName: string;
  IndustryTypeId: number;
  CountryId: number;
  StateId: number;
  billingAddress: CompanyBillingAddress;
}

export class UpdateCompanyDetailsViewModel {
  Id: number;
  CompanyName: string;
  IndustryTypeId: number;
  CountryId: number;
  StateId: number;
  billingAddress: CompanyBillingAddress;
}

export class InsertCompleteCompanyBillingAddress {
  AddressLine1: string;
  AddressLine2: string;
  City: string;
  ZipCode: string;
  PointOfContactName: string;
  CountryCodeId: number;
  CountryId: number;
  StateId: number;
  LocationType: string;
  PointOfContactMobileNumber: string;
  CorrespondanceEmail: string;
}

export class InsertCompanyOtherAddress {
  CompanyId: number;
  otherAddress: CompanyInsertOtherAddress;
}

export class CompanyProfileViewModel {
  CompanyId: number;
  ImageId: string;
  BackgroundImageId: string;
}
