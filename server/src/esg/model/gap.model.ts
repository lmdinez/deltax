export class AssignAssessment {
  AssessmentId: number;
  SharedBy: number;
  SharedWith: number;
  DueDate: Date;
}

export class AssessmentSaveAs {
  AssessmentId: number;
  QuestionId: number[];
  Response: number[];
  ResponseBy: number;
}
export class AssessmentAddFramework {
  FrameworkName: string;
  Description: string;
  CreatedBy: number;
  Category: Category[];
}
export class Category {
  CategoryName: string;
  Subcategory: string[];
}

export class AssessmentAddQuestion {
  Id: string;
  AssessmentId: number;
  Category: string;
  Subcategory: string;
  QuestionType: string;
  Question: string;
  DesiredResponse: number;
  Comment: string;
  CreatedBy: number;
}

export class SaveAsNew {
  AssessmentId: number;
  CategoryId: number;
  Type: string;
  CreatedBy: number;
}

export class DeleteAssessment {
  AssessmentId: number;
}
