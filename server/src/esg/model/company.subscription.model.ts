
enum User_Status {
    Active = "Active", 
    InActive = "In Active"
}

export class CompanySubscriptionViewModel {
    id: number;
    companyId: number;
    planId: number;
    noOfDaysLeftToExpire: number;
    planName: string;
    validFrom: string;
    validTo: string;
    noOfUsers: number;
    noOfActiveUsers: number;
    subscriptionStatus: User_Status;
}

export class InsertSubscriptionViewModel {
    companyId: number;
    planId: number;
    validFrom: string;
    validTo: string;
    noOfUsers: number;
}

export class UpdateSubscriptionViewModel {
    companyId: number;
    planId: number;
    validFrom: string;
    validTo: string;
    noOfUsers: number;
}
