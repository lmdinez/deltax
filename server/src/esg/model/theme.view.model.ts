export class UpdateThemeViewModel {
  ThemeId: number;
  CompanyId: number;
}

export class ThemeViewModel {
  ThemeId: number;
  CompanyId: number;
  Theme: string;
}

export class GetThemeViewModel {
  ThemeId: number;
  Name: string;
  Theme: string;
  Color: string;
}

export class InsertThemeViewModel {
  Name: string;
  Theme: string;
  Color: string;
}
