import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './esg/auth/auth.module';
import { CompanyModule } from './esg/company/company.module';
import { UserMgmtModule } from './esg/usermgmt/usermgmt.module';
import { UtilityModule } from './esg/utility/utility.module';
import { GapModule } from './esg/gap/gap.module';
import { UsersModule } from './esg/users/users.module';
import { HttpModule } from '@nestjs/axios';
import {
  ZeptoMailConfig,
  ZEPTO_MAIL_CONFIG,
} from './esg/common/zeptomail/zeptomail.type';
import { ZeptoMailClientService } from './esg/common/zeptomail/zeptomail.service';

const zeptoMailConfig: ZeptoMailConfig = {
  token:
    'Zoho-enczapikey PHtE6r1cFu/tjDZ88xJS4fS4QsfxMtks/u5uLgNOsI5CD/8FFk0Ar49/wzS+rxl8BvQUQPadzo1us+mZuuqCLWfoZzwdD2qyqK3sx/VYSPOZsbq6x00euFsedkXVUIbmcNJj3CDXuJI=',
  url: 'https://api.zeptomailcom/',
  debug: true,
};

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    UsersModule,
    CompanyModule,
    UserMgmtModule,
    AuthModule,
    UsersModule,
    PassportModule,
    UtilityModule,
    GapModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: ZEPTO_MAIL_CONFIG,
      useValue: zeptoMailConfig,
    },
    ZeptoMailClientService,
  ],
})
export class AppModule {}
